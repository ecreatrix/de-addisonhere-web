<?php

namespace Tests\Feature\Livewire;

use App\Helpers\Misc;
use App\Livewire\Dashboard\TutorAssessment;
use App\Models\Admin;
use App\Models\Tutee;
use App\Models\Tutor;
use App\Models\User;
use App\Models\UserMeta;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class TutorAssessmentTest extends TestCase
{
    use RefreshDatabase;

    /*public function renders_successfully()
    {
    Livewire::test( TutorAssessment::class )->assertStatus( 200 );
    }*/

    function test_can_update_assessment()
    {
        $tutor = Tutor::all()->random();
        $user  = $tutor->user( true );
        //dump( $user->id );
        $this->actingAs( $user );
        $metas = UserMeta::where( 'user_id', $user->id )->get()->keyBy( 'key' )->all();
        dump( $metas['gender']->value );
        Livewire::test( TutorAssessment::class )
            ->set( 'gender', 'female' )
            ->assertSet( 'gender', $metas['gender']->value );
        //->call( 'store' );
        //$this->assertTrue( true );
        /*

    $this->assertTrue( 'female' === $metas['gender'] );*/
    }
}
