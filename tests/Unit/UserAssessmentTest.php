<?php

namespace Tests\Unit;

use App\Helpers\Misc;
use App\Models\Admin;
use App\Models\Tutee;
use App\Models\Tutor;
use App\Models\User;
use App\Models\UserMeta;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserAssessmentTest extends TestCase
{
    public function test_is_admin(): void
    {
        $user          = User::all()->random();
        $user_is_admin = $user->is_admin();
        $admin_model   = Admin::where( 'user_id', $user->id )->exists();

        $message = 'User ID is ' . $user->id . ' user is a ' . $user->get_type() . '.';
        //dump( $message );

        $this->assertTrue( $user_is_admin == $admin_model );
    }

    //use WithConsoleEvents;

    public function test_is_tutee(): void
    {
        $user          = User::all()->random();
        $user_is_tutee = $user->is_tutee();
        $tutee_model   = Tutee::where( 'user_id', $user->id )->exists();

        $message = 'User ID is ' . $user->id . ' user is a ' . $user->get_type() . '.';
        //dump( $message );

        $this->assertTrue( $user_is_tutee == $tutee_model );
    }

    public function test_is_tutor(): void
    {
        $user          = User::all()->random();
        $user_is_tutor = $user->is_tutor();
        $tutor_model   = Tutor::where( 'user_id', $user->id )->exists();

        $message = 'User ID is ' . $user->id . ' user is a ' . $user->get_type() . '.';
        //dump( $message );

        $this->assertTrue( $user_is_tutor == $tutor_model );
    }
}
