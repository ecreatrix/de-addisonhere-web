<?php

namespace Database\Factories;

use App\Models\Lesson;
use App\Models\LessonRequest;
use App\Models\Tutee;
use App\Models\Tutor;
use App\Models\TutorTutee;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Lesson>
 */
class LessonFactory extends Factory {
    public function configure() {
        return $this->afterMaking( function ( Lesson $lesson ) {
            //
        } )->afterCreating( function ( Lesson $lesson ) {
        } );
    }

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition() {
        $created = $this->faker->dateTimeBetween( '-2 months', '+ 3 months' );

        $status = [
            'payment-pending',
            'booked',
            'no-payment',
            'payment-received',
        ];

        $tutor = Tutor::inRandomOrder()->first()->user->id;
        $tutee = TutorTutee::where( 'tutor_user_id', $tutor )->first()->tutee_user_id;

        return [
            'created_at'    => $created,
            'tutor_user_id' => $tutor,
            'tutee_user_id' => $tutee,
            'timeslot'      => $this->faker->dateTimeBetween( '-3 months', '+ 2 months' ),
            'cost'          => $this->faker->numberBetween( 100, 800 ),
            'status'        => $status[array_rand( $status )],
            'description'   => $this->faker->sentence(),
        ];
    }

    public function testTutee() {
        return $this->state( function ( array $attributes ) {
            return [
                'tutor_user_id' => 4,
            ];
        } );
    }

    public function testTutor() {
        return $this->state( function ( array $attributes ) {
            return [
                'tutor_user_id' => 3,
            ];
        } );
    }
}
