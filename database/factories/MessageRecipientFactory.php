<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Wa\Laramessage\Models\Message;
use Wa\Laramessage\Models\MessageRecipient;
use Wa\Laramessage\Models\MessageUser;

class MessageRecipientFactory extends Factory {
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition() {
        return [
            'message_id'   => Message::inRandomOrder()->first()->id,
            'recipient_id' => \App\Models\User::inRandomOrder()->first()->id,
        ];
    }

    public function newModel( array $attributes = [] ) {
        $model = new MessageRecipient();

        return new $model( $attributes );
    }
}
