<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Wa\Laramessage\Models\Message;
use Wa\Laramessage\Models\MessageTag;

class MessageTagFactory extends Factory {
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition() {
        $tag = [
            'archived',
            'draft',
            'important',
            'reported',
        ];

        return [
            'message_id' => Message::inRandomOrder()->first()->id,
            'user_id'    => \App\Models\User::inRandomOrder()->first()->id,
            'name'       => $tag[array_rand( $tag )],
        ];
    }

    public function newModel( array $attributes = [] ) {
        $model = new MessageTag();

        return new $model( $attributes );
    }
}
