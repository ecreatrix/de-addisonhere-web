<?php

namespace Database\Factories;

use App\Models\Admin;
use App\Models\Lesson;
use App\Models\Tutee;
use App\Models\Tutor;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class LessonRequestFactory extends Factory {
    public function approved() {
        return $this->state( function ( array $attributes ) {
            return [
                'admin_status' => 'approved',
                'tutor_status' => 'approved',
                'tutee_status' => 'approved',
            ];
        } );
    }

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition() {
        $created = $this->faker->dateTimeBetween( '-3 months', '+ 2 months' );

        $status = [
            'approved',
            'declined',
        ];

        return [
            'created_at'   => $created,
            'lesson_id'    => Lesson::inRandomOrder()->first()->id,
            'user_id'      => User::inRandomOrder()->first()->id,
            //'user_id'      => 1,
            'tutor_status' => $status[array_rand( $status )],
            'tutee_status' => $status[array_rand( $status )],
            'admin_status' => $status[array_rand( $status )],
        ];
    }

    public function testTutee() {
        return $this->state( function ( array $attributes ) {
            return [
                'lesson_id' => Lesson::where( 'tutee_user_id', 4 )->inRandomOrder()->first()->id,
                'user_id'   => 4,
            ];
        } );
    }

    public function testTutor() {
        return $this->state( function ( array $attributes ) {
            return [
                'lesson_id' => Lesson::where( 'tutor_user_id', 3 )->inRandomOrder()->first()->id,
                'user_id'   => 3,
            ];
        } );
    }
}
