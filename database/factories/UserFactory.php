<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    public function alyx()
    {
        return $this->state( function ( array $attributes ) {
            return [
                'username'  => 'alyx',
                'firstName' => 'Alyx',
                'lastName'  => 'Calder',
                'email'     => 'alyx@detale.ca',
                'password'  => '$2y$10$a0e9bx5D01AZllscZyKdCOoWpGFDpTIWsZGltSbvADb7GG4DZnoCu', // detale123
            ];
        } );
    }

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'username'          => $this->faker->username(),
            'firstName'         => $this->faker->firstName(),
            'lastName'          => $this->faker->lastName(),
            'email'             => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', //password;

            'remember_token' => Str::random( 10 ),
        ];
    }

    public function jai()
    {
        return $this->state( function ( array $attributes ) {
            return [
                'username'  => 'jai',
                'firstName' => 'Jai',
                'lastName'  => 'Paek',
                'email'     => 'jai@detale.ca',
                'password'  => '$2y$10$a0e9bx5D01AZllscZyKdCOoWpGFDpTIWsZGltSbvADb7GG4DZnoCu', // detale123
            ];
        } );
    }

    public function testTutee()
    {
        return $this->state( function ( array $attributes ) {
            return [
                'username' => 'testTutee',
                'email'    => 'tutee@detale.ca',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            ];
        } );
    }

    public function testTutor()
    {
        return $this->state( function ( array $attributes ) {
            return [
                'username' => 'testTutor',
                'email'    => 'tutor@detale.ca',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            ];
        } );
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return static
     */
    public function unverified()
    {
        return $this->state( function ( array $attributes ) {
            return [
                'email_verified_at' => null,
            ];
        } );
    }
}
