<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Wa\Laramessage\Models\Message;

class MessageFactory extends Factory {
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition() {
        $created = $this->faker->dateTimeBetween( '-3 months', '+ 2 months' );

        return [
            'created_at' => $created,
            'sender_id'  => \App\Models\User::inRandomOrder()->first()->id,
            'subject'    => implode( ' ', $this->faker->words( 5 ) ),
            'body'       => $this->faker->paragraph(),
        ];
    }

    public function newModel( array $attributes = [] ) {
        $model = new Message();

        return new $model( $attributes );
    }
}
