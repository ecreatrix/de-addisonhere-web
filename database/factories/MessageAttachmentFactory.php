<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Wa\Laramessage\Models\Message;
use Wa\Laramessage\Models\MessageAttachment;

class MessageAttachmentFactory extends Factory {
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition() {
        $message_id = Message::inRandomOrder()->first()->id;
        $filename   = $this->faker->word();
        //$image      = file_get_contents( 'https://picsum.photos/100/100' );
        $filepath = public_path( 'messages/uploads/' . $message_id . '-' . $filename . '.jpg' );

        //file_put_contents( $filepath, $image );

        return [
            'message_id' => $message_id,
            'filename'   => $filename,
            'filepath'   => $filepath,
        ];
    }

    public function newModel( array $attributes = [] ) {
        $model = new MessageAttachment();

        return new $model( $attributes );
    }
}