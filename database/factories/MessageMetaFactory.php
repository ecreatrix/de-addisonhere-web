<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Wa\Laramessage\Models\Message;
use Wa\Laramessage\Models\MessageMeta;

class MessageMetaFactory extends Factory {
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition() {
        $user = \App\Models\User::inRandomOrder()->first()->id;

        $created = $this->faker->dateTimeBetween( '-3 months', '+ 2 months' );

        return [
            'message_id'   => Message::inRandomOrder()->first()->id,
            'user_id'      => $user,

            'last_read_at' => null,
            'created_at'   => $created,
        ];
    }

    public function newModel( array $attributes = [] ) {
        $model = new MessageMeta();

        return new $model( $attributes );
    }

    public function read() {
        return $this->state( function ( array $attributes ) {
            $created = $attributes['created_at'];

            return [
                'last_read_at' => $this->faker->dateTimeBetween( $created, $created->format( 'Y-m-d H:i:s' ) . ' +2 weeks' ),
            ];
        } );
    }

    public function updated_at() {
        return $this->state( function ( array $attributes ) {
            $created = $attributes['created_at'];

            return [
                'updated_at' => $this->faker->dateTimeBetween( $created, $created->format( 'Y-m-d H:i:s' ) . ' +2 weeks' ),
            ];
        } );
    }
}
