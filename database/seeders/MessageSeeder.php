<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Wa\Laramessage\Models\Message;
use Wa\Laramessage\Models\MessageAttachment;
use Wa\Laramessage\Models\MessageMeta;
use Wa\Laramessage\Models\MessageRecipient;
use Wa\Laramessage\Models\MessageTag;

class MessageSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function helper_random_users( $sender, $recipient ) {
        $sender_is_user = rand( 1, 2 );
        $random_user    = User::whereNot( 'id', $sender->id )->inRandomOrder()->first();
        if ( 1 === $sender_is_user ) {
            $recipient = $random_user;
        } else {
            $sender    = $random_user;
            $recipient = $sender;
        }

        return [
            'sender'    => $sender,
            'recipient' => $recipient,
        ];
    }

    public function run() {
        $users = User::all();
        //$users = User::where( 'id', 3 )->orWhere( 'id', 1 )->orWhere( 'id', 10 )->orWhere( 'id', 5 )->get();
        //$users = User::where( 'id', 3 )->get();

        // Randomize message creation by user
        foreach ( $users as $sender ) {
            $messages = Message::factory( 5 )->set( 'sender_id', $sender )->create();

            foreach ( $messages as $message ) {
                $recipient          = User::whereNot( 'id', $sender->id )->inRandomOrder()->first();
                $message_created_at = $message->created_at;

                $meta_sender = MessageMeta::factory( 1 )->for( $message )->for( $sender )->read()->create();
                //MessageTag::factory( 1 )->for( $message )->for( $sender )->create();

                $meta_recipient = MessageMeta::factory( 1 )->for( $message )->for( $recipient );
                //MessageTag::factory( 1 )->for( $message )->for( $recipient )->create();

                MessageRecipient::factory( 1 )->for( $message )->set( 'recipient_id', $recipient->id )->set( 'created_at', $message_created_at )->create();

                $factory_collection = collect( [$meta_recipient] );

                $multiple_recipients = rand( 1, 100 );
                while ( $multiple_recipients < 20 ) {
                    $recipient2 = User::inRandomOrder()->first();

                    $meta_recipient = MessageMeta::factory( 1 )->for( $message )->for( $recipient2 );
                    //MessageTag::factory( 1 )->for( $message )->for( $recipient2 )->create();

                    $multiple_recipients = rand( 1, 100 );
                }

                foreach ( $factory_collection as $factory ) {
                    $read_state = rand( 1, 100 );
                    if ( $read_state < 70 ) {
                        $factory = $factory->read();
                    }

                    $edited = rand( 1, 100 );
                    if ( $edited < 5 ) {
                        // If it's edited, it must have been read
                        $factory = $factory->read()->updated_at();
                    }

                    $meta = $factory->create()->first();

                    $message_chain_type = rand( 1, 100 );
                    $has_next           = false;
                    $has_previous       = false;
                    if ( $message_chain_type < 20 ) {
                        // Has next message
                        $has_next = true;
                    } else if ( $message_chain_type < 50 ) {
                        // Has previous message
                        $has_previous = true;
                    } else if ( $message_chain_type < 65 ) {
                        // Has next and previous message
                        $has_next     = true;
                        $has_previous = true;
                    }

                    //$has_next = true;
                    //$has_previous = true;

                    $reply_date = $message_created_at->addDays( rand( 1, 8 ) );
                    if ( $has_next ) {
                        $message_recipient = $message->recipients->first()->recipient;

                        $message2 = Message::factory( 1 )->set( 'sender_id', $message_recipient )->set( 'parent_id', $message->id )->set( 'created_at', $reply_date )->create()->first();

                        $meta_sender2    = MessageMeta::factory( 1 )->for( $message2 )->for( $sender )->set( 'created_at', $reply_date )->create();
                        $meta_recipient2 = MessageMeta::factory( 1 )->for( $message2 )->for( $message_recipient )->set( 'created_at', $reply_date )->create();

                        MessageRecipient::factory( 1 )->for( $message2 )->set( 'recipient_id', $sender )->set( 'created_at', $reply_date )->create();
                    }

                    if ( $has_previous ) {
                        $message_recipient = $message->recipients->first()->recipient;

                        $message2   = Message::factory( 1 )->set( 'sender_id', $message_recipient )->create()->first();
                        $reply_date = $message2->created_at->addDays( 4 );

                        $meta_sender2    = MessageMeta::factory( 1 )->for( $message2 )->for( $sender )->set( 'created_at', $reply_date )->create();
                        $meta_recipient2 = MessageMeta::factory( 1 )->for( $message2 )->for( $message_recipient )->set( 'created_at', $reply_date )->create();

                        MessageRecipient::factory( 1 )->for( $message2 )->set( 'recipient_id', $sender )->set( 'created_at', $reply_date )->create();

                        $message->parent_id                      = $message2->id;
                        $message->created_at                     = $reply_date;
                        $message->recipients->first()->recipient = $sender->id;
                        $message->save();
                    }
                }

                // Add attachments to some messages
                $attachment_chance = rand( 1, 100 );
                if ( $attachment_chance < 20 ) {
                    MessageAttachment::factory( 1 )->for( $message )->set( 'created_at', $message_created_at )->create();
                }

                $tag_chance = rand( 1, 100 );
                if ( $tag_chance < 20 ) {
                    MessageTag::factory( 1 )->for( $message )->set( 'created_at', $message_created_at )->create();
                }
            }
            //break;
        }
    }
}