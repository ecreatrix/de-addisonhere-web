<?php

namespace Database\Seeders;

use App\Models\Lesson;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LessonSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Lesson::factory( 75 )->create();
        Lesson::factory( 25 )->testTutor()->create();
        Lesson::factory( 25 )->testTutee()->create();
    }
}
