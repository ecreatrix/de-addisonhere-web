<?php

namespace Database\Seeders;

use App\Helpers\Misc;
use App\Models\Admin;
use App\Models\Tutee;
use App\Models\Tutor;
use App\Models\TutorTutee;
use App\Models\User;
use App\Models\UserMeta;
use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * The current Faker instance.
     *
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * Create a new seeder instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->faker = $this->withFaker();
    }

    public function new_device()
    {
        $proficiencyOptions = Misc::$proficiencyOptions;
        $device             = [
            'type'        => $this->faker->word(),
            'useLength'   => date_diff( now(), $this->faker->dateTimeThisDecade() )->format( '%m months' ),
            'purposes'    => $this->faker->text( 15 ),
            'proficiency' => $proficiencyOptions[array_rand( $proficiencyOptions )],
        ];

        return $device;
    }

    public function new_language()
    {
        $proficiencyOptions = Misc::$proficiencyOptions;
        $language           = [
            'name'        => locale_get_display_language( 'sl-Latn-IT-nedis', $this->faker->languageCode() ),
            'proficiency' => $proficiencyOptions[array_rand( $proficiencyOptions )],
        ];
        return $language;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->alyx()->create();
        User::factory()->jai()->create();
        User::factory()->testTutor()->create();
        User::factory()->testTutee()->create();

        User::factory( 70 )->unverified()->create();

        $users = User::all();

        $gender    = array_keys( Misc::$genderOptions );
        $age       = array_keys( Misc::$ageOptions );
        $education = array_keys( Misc::$educationOptions );

        $availability = array_keys( Misc::$availabilityOptions );

        $travel = array_keys( Misc::$travelOptions );

        $internet = array_keys( Misc::$internetOptions );

        $techTime = array_keys( Misc::$techTimeOptions );

        $software = array_keys( Misc::$softwareOptions );

        $app = array_keys( Misc::$appExperienceOptions );

        $appsUsed = array_keys( Misc::$appsUsedOptions );

        $weekStart = Misc::$weekStart;

        foreach ( $users as $user ) {
            $random_user_category = rand( 0, 1 );

            UserMeta::factory( 1 )->for( $user )->set( 'key', 'phone' )->set( 'value', $this->faker->areaCode() . $this->faker->unique()->numerify( '#######' ) )->create();

            UserMeta::factory( 1 )->for( $user )->set( 'key', 'address' )->set( 'value', $this->faker->streetAddress() )->create();
            UserMeta::factory( 1 )->for( $user )->set( 'key', 'city' )->set( 'value', $this->faker->city() )->create();
            UserMeta::factory( 1 )->for( $user )->set( 'key', 'province' )->set( 'value', $this->faker->province() )->create();
            UserMeta::factory( 1 )->for( $user )->set( 'key', 'postalCode' )->set( 'value', $this->faker->postcode() )->create();
            UserMeta::factory( 1 )->for( $user )->set( 'key', 'weekStart' )->set( 'value', rand( 0, 6 ) )->create();

            if ( 'alyx' === $user->username || 'jai' === $user->username ) {
                Admin::factory()->for( $user )->create();
            } else {
                $tutor_tutee = new TutorTutee();

                UserMeta::factory( 1 )->for( $user )->set( 'key', 'gender' )->set( 'value', $gender[array_rand( $gender )] )->create();

                UserMeta::factory( 1 )->for( $user )->set( 'key', 'age' )->set( 'value', $age[array_rand( $age )] )->create();
                UserMeta::factory( 1 )->for( $user )->set( 'key', 'education' )->set( 'value', $education[array_rand( $education )] )->create();
                UserMeta::factory( 1 )->for( $user )->set( 'key', 'availability' )->set( 'value', $availability[array_rand( $availability )] )->create();
                UserMeta::factory( 1 )->for( $user )->set( 'key', 'travel' )->set( 'value', $travel[array_rand( $travel )] )->create();

                UserMeta::factory( 1 )->for( $user )->set( 'key', 'nativeLanguage' )->set( 'value', locale_get_display_language( 'sl-Latn-IT-nedis', $this->faker->languageCode() ) )->create();

                $languages       = [$this->new_language()];
                $anotherLanguage = rand( 1, 10 );
                if ( $anotherLanguage < 8 ) {
                    $languages[]     = $this->new_language();
                    $anotherLanguage = rand( 1, 10 );

                    if ( $anotherLanguage < 5 ) {
                        $languages[]     = $this->new_language();
                        $anotherLanguage = rand( 1, 10 );

                        if ( $anotherLanguage < 3 ) {
                            $languages[] = $this->new_language();
                        }
                    }
                }
                UserMeta::factory( 1 )->for( $user )->set( 'key', 'languages' )->set( 'value', json_encode( $languages ) )->create();

                $devices       = [$this->new_device()];
                $anotherDevice = rand( 1, 10 );
                if ( $anotherDevice < 7 ) {
                    $devices[]     = $this->new_device();
                    $anotherDevice = rand( 1, 10 );

                    if ( $anotherDevice < 5 ) {
                        $devices[]     = $this->new_device();
                        $anotherDevice = rand( 1, 10 );

                        if ( $anotherDevice < 3 ) {
                            $devices[]     = $this->new_device();
                            $anotherDevice = rand( 1, 10 );

                            if ( $anotherDevice < 2 ) {
                                $devices[] = $this->new_device();
                            }
                        }
                    }
                }
                UserMeta::factory( 1 )->for( $user )->set( 'key', 'devices' )->set( 'value', json_encode( $devices ) )->create();

                UserMeta::factory( 1 )->for( $user )->set( 'key', 'internetProficiency' )->set( 'value', $internet[array_rand( $internet )] )->create();
                UserMeta::factory( 1 )->for( $user )->set( 'key', 'techTime' )->set( 'value', $techTime[array_rand( $techTime )] )->create();

                $selectedSoftware   = [$software[array_rand( $software )]];
                $selectedSoftware[] = 'other';
                UserMeta::factory( 1 )->for( $user )->set( 'key', 'software' )->set( 'value', json_encode( $selectedSoftware ) )->create();
                if ( in_array( 'other', $selectedSoftware ) ) {
                    UserMeta::factory( 1 )->for( $user )->set( 'key', 'otherSoftware' )->set( 'value', implode( ', ', $this->faker->words() ) )->create();
                }

                $selectedAppsExperience   = [$app[array_rand( $app )]];
                $selectedAppsExperience[] = 'other';
                UserMeta::factory( 1 )->for( $user )->set( 'key', 'appExperience' )->set( 'value', json_encode( $selectedAppsExperience ) )->create();
                if ( in_array( 'other', $selectedAppsExperience ) ) {
                    UserMeta::factory( 1 )->for( $user )->set( 'key', 'otherAppExperience' )->set( 'value', $this->faker->sentence() )->create();
                }

                $selectedAppsUsed   = [$appsUsed[array_rand( $appsUsed )]];
                $selectedAppsUsed[] = 'other';
                UserMeta::factory( 1 )->for( $user )->set( 'key', 'appsUsed' )->set( 'value', json_encode( $selectedAppsUsed ) )->create();
                if ( in_array( 'other', $selectedAppsUsed ) ) {
                    UserMeta::factory( 1 )->for( $user )->set( 'key', 'otherAppsUsed' )->set( 'value', $this->faker->sentence() )->create();
                }

                $value = $this->faker->boolean();
                UserMeta::factory( 1 )->for( $user )->set( 'key', 'internetSolutions' )->set( 'value', $value )->create();
                if ( $value ) {
                    UserMeta::factory( 1 )->for( $user )->set( 'key', 'internetSolutionsExtra' )->set( 'value', $this->faker->sentence() )->create();
                }

                $value = $this->faker->boolean();
                UserMeta::factory( 1 )->for( $user )->set( 'key', 'printerExperience' )->set( 'value', $value )->create();
                if ( $value ) {
                    UserMeta::factory( 1 )->for( $user )->set( 'key', 'printerExperienceExtra' )->set( 'value', $this->faker->sentence() )->create();
                }

                $value = $this->faker->boolean();
                UserMeta::factory( 1 )->for( $user )->set( 'key', 'tutoringExperience' )->set( 'value', $value )->create();
                if ( $value ) {
                    UserMeta::factory( 1 )->for( $user )->set( 'key', 'tutoringExperienceExtra' )->set( 'value', $this->faker->sentence() )->create();
                }

                $value = $this->faker->boolean();
                UserMeta::factory( 1 )->for( $user )->set( 'key', 'assistedRelativesExperience' )->set( 'value', $value )->create();
                if ( $value ) {
                    UserMeta::factory( 1 )->for( $user )->set( 'key', 'assistedRelativesExperienceExtra' )->set( 'value', $this->faker->sentence() )->create();
                }

                $addComment = rand( 1, 10 );
                if ( $addComment < 7 ) {
                    UserMeta::factory( 1 )->for( $user )->set( 'key', 'comments' )->set( 'value', $this->faker->sentence() )->create();
                }

                if ( 'testTutor' === $user->username ) {
                    // remove ->first() from tutor/tutee model if getting foreign key error
                    $tutor = Tutor::factory()->for( $user )->create();
                } else if ( 'testTutee' === $user->username ) {
                    $tutee = Tutee::factory()->for( $user )->create();
                } else {
                    $tutor_tutee->tutor_user_id = Tutor::inRandomOrder()->first()->user->id;
                    $tutor_tutee->tutee_user_id = Tutee::inRandomOrder()->first()->user->id;

                    if ( $random_user_category ) {
                        Tutor::factory()->for( $user )->create();
                        $tutor_tutee->tutor_user_id = $user->id;
                    } else {
                        Tutee::factory()->for( $user )->create();
                        $tutor_tutee->tutee_user_id = $user->id;
                    }

                    $tutor_tutee->save();
                }
            }
        }
    }

    /**
     * Get a new Faker instance.
     *
     * @return \Faker\Generator
     */
    protected function withFaker()
    {
        return Container::getInstance()->make( Generator::class );
    }
}
