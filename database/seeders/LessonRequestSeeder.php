<?php

namespace Database\Seeders;

use App\Models\Lesson;
use App\Models\LessonRequest;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LessonRequestSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $lessons = Lesson::all();

        foreach ( $lessons as $lesson ) {
            $random_creator = $lesson->tutor_user_id;
            $request        = LessonRequest::factory( 1 )->set( 'lesson_id', $lesson->id )->set( 'user_id', $random_creator );

            // Only lessons that are just at booked stage can be unapproved
            if ( 'booked' !== $lesson->status ) {
                $request = $request->approved();
            }

            $request = $request->create();
        }
    }
}
