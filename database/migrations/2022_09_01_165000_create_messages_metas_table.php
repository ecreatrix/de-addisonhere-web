<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesMetasTable extends Migration {
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists( 'messages_metas' );
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create( 'messages_metas', function ( Blueprint $table ) {
            $table->id();

            $table->foreignId( 'message_id' )
                  ->references( 'id' )->on( 'messages' )
                  ->onUpdate( 'cascade' )
                  ->onDelete( 'cascade' );

            $table->foreignId( 'user_id' )
                  ->references( 'id' )->on( 'users' )
                  ->onUpdate( 'cascade' )
                  ->onDelete( 'cascade' );

            $table->dateTime( 'last_read_at' )->nullable();
            $table->timestamps();
        } );
    }
};
