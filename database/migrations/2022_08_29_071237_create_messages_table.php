<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Wa\Laramessage\Models;

class CreateMessagesTable extends Migration {
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists( 'messages' );
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create( 'messages', function ( Blueprint $table ) {
            $table->id();

            $table->foreignId( 'sender_id' ) // sender
                  ->references( 'id' )->on( 'users' )
                  ->onUpdate( 'cascade' )
                  ->onDelete( 'cascade' );

            $table->foreignId( 'parent_id' )->nullable() // for threading
                  ->references( 'id' )->on( 'messages' );

            $table->longText( 'subject' )->nullable();
            $table->longText( 'body' )->nullable();

            $table->softDeletes();
            $table->timestamps();
        } );
    }
}
