<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonsTable extends Migration {
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists( 'lessons' );
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create( 'lessons', function ( Blueprint $table ) {
            $table->id();

            $table->foreignId( 'tutor_user_id' )
                  ->references( 'id' )->on( 'users' )
                  ->onUpdate( 'cascade' )
                  ->onDelete( 'cascade' );

            $table->foreignId( 'tutee_user_id' )
                  ->references( 'id' )->on( 'users' )
                  ->onUpdate( 'cascade' )
                  ->onDelete( 'cascade' );

            $table->dateTime( 'timeslot' )->nullable();
            $table->integer( 'cost' )->nullable();
            $table->string( 'status' )->nullable();
            $table->longText( 'description' )->nullable();

            $table->timestamps();
        } );
    }
};
