<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Wa\Laramessage\Models;

class CreateMessagesAttachmentsTable extends Migration {
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists( 'messages_attachments' );
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create( 'messages_attachments', function ( Blueprint $table ) {
            $table->id();

            $table->foreignId( 'message_id' )
                  ->references( 'id' )->on( 'messages' )
                  ->onUpdate( 'cascade' )
                  ->onDelete( 'cascade' );

            $table->string( 'filename' )->nullable();
            $table->string( 'filepath' )->nullable();

            $table->timestamps();
        } );
    }
};
