<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonsRequestsTable extends Migration {
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists( 'lessons_requests' );
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create( 'lessons_requests', function ( Blueprint $table ) {
            $table->id();

            $table->foreignId( 'lesson_id' )
                  ->references( 'id' )->on( 'lessons' )
                  ->onUpdate( 'cascade' )
                  ->onDelete( 'cascade' );

            $table->foreignId( 'user_id' )
                  ->references( 'id' )->on( 'users' )
                  ->onUpdate( 'cascade' )
                  ->onDelete( 'cascade' );

            $table->string( 'tutor_status' )->nullable();
            $table->string( 'tutee_status' )->nullable();
            $table->string( 'admin_status' )->nullable();

            $table->timestamps();
        } );
    }
}