<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTutorsTuteesTable extends Migration {
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists( 'tutors_tutees' );
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create( 'tutors_tutees', function ( Blueprint $table ) {
            $table->id();

            $table->foreignId( 'tutor_user_id' )
                  ->references( 'id' )->on( 'users' )
                  ->onUpdate( 'cascade' )
                  ->onDelete( 'cascade' );

            $table->foreignId( 'tutee_user_id' )
                  ->references( 'id' )->on( 'users' )
                  ->onUpdate( 'cascade' )
                  ->onDelete( 'cascade' );

            $table->timestamps();
        } );
    }
}