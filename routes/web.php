<?php

//use App\Livewire\Dashboard\UserManagement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::middleware( 'web' )->group( function () {
    Route::match( 'get', '/', function () {
        return view( 'pages/home' );
    } )->name( 'home' );

    Route::match( 'get', '/careers', function () {
        return view( 'pages/careers' );
    } )->name( 'careers' );

    Route::match( 'get', '/about', function () {
        return view( 'pages/about' );
    } )->name( 'about' );

    Route::match( 'get', '/contact', function () {
        return view( 'pages/contact' );
    } )->name( 'contact' );

    Route::match( 'get', '/earnings', function () {
        return view( 'pages/earnings' );
    } )->name( 'earnings' );

    Route::match( 'get', '/help-tutor', function () {
        return view( 'pages/help-tutor' );
    } )->name( 'help-tutor' );
    Route::match( 'get', '/help-tutee', function () {
        return view( 'pages/help-tutee' );
    } )->name( 'help-tutee' );

    Route::match( 'get', '/tutor', function () {
        return view( 'pages/home-tutor' );
    } )->name( 'tutor' );
    Route::match( 'get', '/tutee', function () {
        return view( 'pages/home-tutee' );
    } )->name( 'tutee' );

    Route::match( 'get', '/locations', function () {
        return view( 'pages/locations' );
    } )->name( 'locations' );

    Route::match( 'get', '/assessment', function () {
        return view( 'pages/money-tutor' );
    } )->name( 'assessment' );

    Route::match( 'get', '/help', function () {
        return view( 'pages/help' );
    } )->name( 'help' );

    Route::match( 'get', '/fees', function () {
        return view( 'pages/money-tutee' );
    } )->name( 'fees' );

    Route::get( 'dashboard', function () {
        if ( Auth::check() ) {
            return view( 'pages/dashboard' );
        } else {
            return view( 'auth/login' );
        }
    } )->name( 'dashboard' );

    Route::get( 'register', function () {
        return view( 'auth/register' );
    } )->name( 'register' );

    Route::get( 'login', function () {
        if ( Auth::check() ) {
            return view( 'pages/dashboard' );
        } else {
            return view( 'auth/login' );
        }
    } )->name( 'login' );

    Route::get( 'forgot-password', function () {
        return view( 'auth/forgot-password' );
    } )->name( 'password.request' );

    Route::get( 'reset-password/{token}', function () {
        return view( 'auth/reset-password' );
    } )->name( 'password.reset' );
} );

Route::middleware( 'auth' )->group( function () {
    //Route::get( 'profile', Dashboard::class )->name( 'profile' );
    //Route::get( 'settings', Dashboard::class )->name( 'settings' );
    //Route::get( 'messages', Dashboard::class )->name( 'messages' );
    //Route::get( 'user-management', UserManagement::class )->name( 'user-management' );

    Route::get( '/mailable', function () {
        return new App\Mail\SendForgotPassword( 'Forgot Password', 'Link' );
    } );
} );

require __DIR__ . '/auth.php';
