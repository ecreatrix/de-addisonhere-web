<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use Illuminate\Support\Facades\Route;

Route::middleware( 'auth' )->group( function () {
    Route::get( 'scheme', function () {
        return view( 'pages.scheme' );
    } )->name( 'scheme' );

    Route::get( 'verify-email', function () {
        return view( 'auth/verify-email' );
    } )->name( 'verification.notice' );

    Route::get( 'verify-email/{id}/{hash}', function () {
        return view( 'auth/verify-email' );
    } )->middleware( ['signed', 'throttle:6,1'] )->name( 'verification.verify' );

    //Route::post( 'email/verification-notification', function () {
    //    return view( 'auth/verify-email' );
    //} )->middleware( 'throttle:6,1' )->name( 'verification.send' );

    Route::get( 'confirm-password', function () {
        return view( 'auth/confirm-password' );
    } )->name( 'password.confirm' );

    //Route::post( 'confirm-password', function () {
    //    return view( 'auth/confirm-password' );
    //} );

    Route::get( 'logout', [AuthenticatedSessionController::class, 'destroy'] )->name( 'logout' );
} );
