import { defineConfig } from 'vite';
import laravel, { refreshPaths } from 'laravel-vite-plugin';
import purge from '@erbelion/vite-plugin-laravel-purgecss'

let assets = [
    'resources/styles/dashboard.css','resources/scripts/dashboard.js',
    'resources/styles/app.css','resources/scripts/app.js'
];

export default defineConfig(({ command, mode, ssrBuild }) => {
    if (command === 'serve') {
        return {
            // dev specific config
            plugins: [
                laravel({
                    input: assets,
                    refresh: [
                        ...refreshPaths,
                        'app/Providers/**',
                        'app/Livewire/**',
                        'app/Models/*',
                        'resources/**',
                        'resources/vendors/**',
                    ],        
                }),
            ],
        }
    } else {
        // command === 'build'
        return {
            // prod specific config
            plugins: [
                laravel({
                    input: assets,
                }),
                //purge({
                //    templates: ['blade'],
                //    paths: ['resources/views/*']
                //})
            ],
            outDir: 'public',
        }
    }
});
