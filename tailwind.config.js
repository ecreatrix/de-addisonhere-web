import defaultTheme from 'tailwindcss/defaultTheme';

/** @type {import('tailwindcss').Config} */
export default {
    darkMode: 'class',

    safelist: [
        {
            pattern: /border-(white|black|green|blue|pink|gray|neutral)*/,
        },
    ],
  
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        "./node_modules/tw-elements/dist/js/**/*.js",
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Montserrat', ...defaultTheme.fontFamily.sans],
            },
            lineHeight: {
                '14': '3.5rem',
            },
            colors: {
                current: 'currentColor',
                green: '#8AC75A',
                pink: '#F1666A',
                blue: '#27BDBE',
                lightBlue: '#C4F3F3',
            },
            padding: {
                '1/2': '50%',
                full: '100%',
            },
            width: {
                '1/6': '16.6666666666667%',
                '3/10': '30%',
                '1/3': '33%',
                '45/100': '45%',
                '1/2': '50%',
                '2/3': '66%',
            },
            maxWidth: {
                '1/3': '33%',
                '45/100': '45%',
                '1/2': '50%',
                '2/3': '66%',
            }
        },
    },

    plugins: [
        require('@tailwindcss/forms'),
        require("tw-elements/dist/plugin.cjs"),
        //require('tailwindcss-plugins/pagination')({}),
        require('tailwindcss/plugin')(function ({addBase, addVariant}) {
            //addVariant('not-last', '&:not(:last-child)')
          /*addBase({
            '[type="search"]::-webkit-search-decoration': {display: 'none'},
            '[type="search"]::-webkit-search-cancel-button': {display: 'none'},
            '[type="search"]::-webkit-search-results-button': {display: 'none'},
            '[type="search"]::-webkit-search-results-decoration': {display: 'none'},
          })*/
        }),
    ],
};
