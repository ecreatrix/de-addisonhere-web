<?php

namespace App\Helpers;

class Misc
{
    public static $provinces = [
        'AB' => 'Alberta',
        'BC' => 'British Columbia',
        'MB' => 'Manitoba',
        'NB' => 'New Brunswick',
        'NL' => 'Newfoundland and Labrador',
        'NS' => 'Nova Scotia',
        'NT' => 'Northwest Territories',
        'NU' => 'Nunavut',
        'ON' => 'Ontario',
        'PE' => 'Prince Edward Island',
        'QC' => 'Quebec',
        'SK' => 'Saskatchewan',
        'YT' => 'Yukon',
    ],
    $weekStart     = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
    $genderOptions = [
        'male'   => 'Male',
        'female' => 'Female',
        'other'  => 'Other',
        'prefer' => 'Prefer not to say',
    ],
    $ageOptions = [
        '17-younger' => '17 or younger',
        '18-25'      => '18-25',
        '26-34'      => '26-34',
        '35-39'      => '35-39',
        '40-over'    => 'Over 40',
    ],
    $educationOptions = [
        'high-school'     => 'High School degree or equivalent',
        'some-university' => 'Some university but no degree',
        'bachelors'       => 'Bachelor’s Degree',
        'masters'         => 'Master’s Degree',
        'phd'             => 'PhD',
    ],
    $availabilityOptions = [
        '5-less'  => 'Less than five',
        '6-10'    => '6-10',
        '11-20'   => '11-20',
        '21-30'   => '21-30',
        '30-over' => 'Over 30',
    ],
    $travelOptions = [
        'yes'    => 'Yes',
        'no'     => 'No',
        'unsure' => 'Unsure',
    ],
    $internetOptions = [
        'very-poor' => 'Very Poor',
        'poor'      => 'Poor',
        'average'   => 'Average',
        'good'      => 'Good',
        'excellent' => 'Excellent',
    ],
    $techTimeOptions = [
        'none'    => 'None',
        '2-less'  => 'Less than 2 hours',
        '3-6'     => '3-6 hours',
        '7-9'     => '7-9 hours',
        '10-over' => 'Over 10 hours',
    ],
    $softwareOptions = [
        'windows' => 'Windows',
        'macos'   => 'MacOS',
        'linux'   => 'Linux',
        'other'   => 'Other (please specify)',
    ],
    $appExperienceOptions = [
        'microsoft'  => 'Microsoft Office (Word, Excel, PowerPoint)',
        'google'     => 'G Suite (Gmail, Google Docs)',
        'comm-tools' => 'Communication tools (Microsoft Teams, Slack, Skype)',
        'other'      => 'Other (please specify)',
    ],
    $appsUsedOptions = [
        'social'        => 'Social media apps (Facebook, Instagram, Twitter)',
        'utility'       => 'Utility apps (Email)',
        'entertainment' => 'Entertainment apps (YouTube, Netflix, Disney +, Amazon Prime Video)',
        'news'          => 'News apps',
        'gaming'        => 'Gaming apps',
        'online'        => 'Online banking apps',
        'weather'       => 'Weather apps',
        'transport'     => 'Transportation apps (Uber, Lyft)',
        'video'         => 'Video-calling apps (Facetime, WhatsApp, Zoom)',
        'music'         => 'Music streaming apps (Apple Music, Spotify)',
        'other'         => 'Other (please specify)',
    ],
    $proficiencyOptions = ['Excellent', 'Great', 'Good', 'Decent', 'Poor'];

    public static function formatPhone( $phone, $pretty = true )
    {
        $phone = intval( preg_replace( '/[^0-9]/', '', $phone ) );

        if ( $pretty ) {
            $phone = preg_replace( '/^(\(?\d{3}\)?)?[- .]?(\d{3})[- .]?(\d{4})$/', '(\\1) \\2-\\3', $phone );
        }

        return $phone;
    }

    public static function provinceKey( $value )
    {
        return array_search( $value, self::provinces() );
    }

    public static function provinceValue( $key )
    {
        $provinces = self::$provinces;
        return array_key_exists( $key, $provinces ) ? $provinces[$key] : $key;
    }

    public static function provinces()
    {
        return self::$provinces;
    }
}
