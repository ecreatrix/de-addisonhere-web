<?php

namespace App\Models;

use App\Models\Admin;
use App\Models\Lesson;
use App\Models\Tutee;
use App\Models\Tutor;
use App\Models\UserMeta;
use Filament\Panel;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;
use Wa\Laramessage\Models\Message;
use Wa\Laramessage\Models\MessageMeta;
use Wa\Laramessage\Models\MessageRecipient;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email',
        'username',
        'password',
        'firstName',
        'lastName',
        'phone',
        'address',
        'city',
        'province',
        'postalCode',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function actionable()
    {
        $actionable = \App\Models\Admin::admin_users();

        if ( $this->is_admin() ) {
            return User::all();
        } else if ( $this->is_tutor() ) {
            $tutees = $this->assigned_tutees;
            //$actionable = $actionable->merge( $tutees );
            $actionable = $tutees;
        } else if ( $this->is_tutee() ) {
            $tutors = $this->assigned_tutors;
            //$actionable = $actionable->merge( $tutors );
            $actionable = $tutors;
        }

        return $actionable->unique();
    }

    public function admin()
    {
        return $this->hasOne( Admin::class );
    }

    public function all_messages()
    {
        $received_messages = $this->received_messages();
        $sent_messages     = $this->sent_messages();

        return $received_messages->merge( $sent_messages )->sortBy( 'created_at' );
    }

    public function assigned_tutees()
    {
        return $this->hasManyThrough( User::class, TutorTutee::class, 'tutor_user_id', 'id', 'id', 'tutee_user_id' );
    }

    public function assigned_tutors()
    {
        return $this->hasManyThrough( User::class, TutorTutee::class, 'tutee_user_id', 'id', 'id', 'tutor_user_id' );
    }

    public function canAccessPanel( Panel $panel ): bool
    { //str_ends_with($this->email, '@yourdomain.com') &&
        return $this->hasVerifiedEmail();
    }

    public function full_name()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function get_id()
    {
        $user = Auth::user();

        return $user->id;
    }

    public function get_type()
    {
        if ( $this->is_admin() ) {
            return 'admin';
        } else if ( $this->is_tutor() ) {
            return 'tutor';
        } else if ( $this->is_tutee() ) {
            return 'tutee';
        }
    }

    public function is_admin()
    {
        return $this->admin()->exists();
    }

    public function is_tutee()
    {
        return $this->tutee()->exists();
    }

    public function is_tutor()
    {
        return $this->tutor()->exists();
    }

    public function lesson_requests()
    {
        return $this->hasMany( Lesson::class, 'user_id', 'id' )->get();
    }

    public function lessons()
    {
        if ( $this->is_admin() ) {
            return Lesson::all();
        } else if ( $this->is_tutor() ) {
            return $this->hasMany( Lesson::class, 'tutor_user_id', 'id' )->get();
        } else if ( $this->is_tutee() ) {
            return $this->hasMany( Lesson::class, 'tutee_user_id', 'id' )->get();
        }
    }

    public function meta()
    {
        return $this->hasMany( UserMeta::class );
    }

    public function received_messages()
    {
        //return $this->hasMany( MessageMeta::class );
        //return $this->belongsToMany( MessageRecipient::class, 'recipient_id', 'id' );
        return $this->hasManyThrough( Message::class, MessageRecipient::class, 'recipient_id', 'id', 'id', 'message_id' )->withTrashed()->whereNull( 'parent_id' )->get()->sortBy( 'created_at' );
    }

    public function sent_messages()
    {
        //return $this->hasMany( MessageMeta::class );
        return $this->hasMany( Message::class, 'sender_id', 'id' )->withTrashed()->whereNull( 'parent_id' )->get()->sortBy( 'created_at' );
    }

    public function tutee()
    {
        return $this->hasOne( Tutee::class );
    }

    public function tutor()
    {
        return $this->hasOne( Tutor::class );
    }
}
