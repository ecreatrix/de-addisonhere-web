<?php

namespace App\Models;

use App\Models\Message;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model {
    use HasFactory;

    protected $fillable = [
        'user_id',
    ];

    public static function admin_users() {
        $admins = Admin::all();
        $users  = collect( [] );
        foreach ( $admins as $admin ) {
            $users->add( $admin->user );
        }

        return $users;
    }

    public function user() {
        return $this->belongsTo( User::class, 'user_id', 'id' );
    }
}
