<?php

namespace App\Models;

use App\Models\Admin;
use App\Models\Message;
use App\Models\Tutee;
use App\Models\TutorTutee;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tutor extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
    ];

    public function lessons()
    {
        return $this->hasMany( Lesson::class, 'tutor_user_id', 'id' );
    }

    public function user( $first = false )
    {
        $user = $this->belongsTo( User::class, 'user_id', 'id' );

        if ( $first ) {
            return $user->first();
        }

        return $user;
    }
}
