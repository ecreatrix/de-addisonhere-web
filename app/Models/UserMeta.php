<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'key',
        'value',
    ];

    public function all_values( $user )
    {
        $meta = $this->where( 'user_id', $user->id )->get()->keyBy( 'key' )->toArray();
        return $meta;
    }

    public function user()
    {
        return $this->belongsTo( User::class, 'user_id', 'id' );
    }

    public function value( $user, $key )
    {
        //$user = $this->user()->first();
        //clock( $this );
        //clock( $this->user_id );
        //clock( $this->belongsTo( User::class, 'user_id', 'id' )->get()->all() );
        //clock( $this->user()->first() );
        //clock( $this->user()->get() );
        $meta = $this->where( ['user_id' => $user->id, 'key' => $key] )->get()->toArray();
        //clock( $this->where( 'user_id', $user->id )->get()->keyBy( 'key' )->toArray() );
        return $meta;
    }
}
