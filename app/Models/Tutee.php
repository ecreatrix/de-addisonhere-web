<?php

namespace App\Models;

use App\Models\Admin;
use App\Models\Tutor;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tutee extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
    ];

    public function lessons()
    {
        //return $this->belongsToMany( Tutor::class, 'lessons' )->withPivot( 'id', 'cost', 'timeslot', 'status', 'description' )->with( ['user:id,firstName,lastName'] );
        return $this->hasMany( Lesson::class, 'tutee_id', 'id' );
    }

    public function tutors()
    {
        return $this->belongsToMany( User::class, 'tutors_tutees', 'id', 'tutor_user_id' )->using( TutorTutee::class );
    }

    public function user( $first = false )
    {
        $user = $this->belongsTo( User::class, 'user_id', 'id' );

        if ( $first ) {
            return $user->first();
        }

        return $user;
    }
}
