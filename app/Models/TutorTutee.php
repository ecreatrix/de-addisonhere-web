<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TutorTutee extends Pivot {
    public $table = "tutors_tutees";

    public function tutee() {
        return $this->belongsTo( User::class, 'tutee_user_id', 'id' );
    }

    public function tutor() {
        return $this->belongsTo( User::class, 'tutor_user_id', 'id' );
    }
}
