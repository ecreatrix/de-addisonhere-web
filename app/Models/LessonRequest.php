<?php

namespace App\Models;

use App\Models\Lesson;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LessonRequest extends Model {
    use HasFactory;

    public $table = "lessons_requests";

    protected $fillable = [
        'user_id',
        'lesson_id',
        'admin_status', // Potential statuses: approved, declined
        'tutor_status', // Potential statuses: approved, declined
        'tutee_status', // Potential statuses: approved, declined
    ];

    public function is_approved() {
        $approved_status = 'approved';

        return $this->admin_status === $approved_status && $this->tutor_status === $approved_status && $this->tutee_status === $approved_status;
    }

    public function lesson() {
        return $this->belongsTo( Lesson::class, 'lesson_id', 'id' );
    }

    public function user() {
        return $this->belongsTo( User::class, 'user_id', 'id' );
    }
}
