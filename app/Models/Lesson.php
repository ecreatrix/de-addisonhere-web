<?php

namespace App\Models;

use App\Models\Admin;
use App\Models\LessonRequest;
use App\Models\Tutee;
use App\Models\Tutor;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Lesson extends Model {
    use HasFactory;

    public $table = "lessons";

    protected $fillable = [
        'tutor_user_id',
        'tutee_user_id',
        'cost',
        'date',
        'description',
        'timeslot',
        'status', // Potential statuses: booked, payment-pending, no-payment, payment-received
    ];

    public function admin() {
        return $this->belongsTo( Admin::class, 'admin_id', 'id' );
    }

    public function is_approved() {
        $lesson_request = $this->request;

        return null == $lesson_request || $lesson_request->is_approved();
    }

    public function request() {
        return $this->belongsTo( LessonRequest::class, 'id', 'lesson_id' );
    }

    public function tutee() {
        return $this->belongsTo( User::class, 'tutee_user_id', 'id' );
    }

    public function tutor() {
        return $this->belongsTo( User::class, 'tutor_user_id', 'id' );
    }

    public function user() {
        $user = Auth::user();

        if ( $user->is_tutor() ) {
            return User::where( 'id', $this->tutee_user_id )->first();
        } else if ( $user->is_tutee() ) {
            return User::where( 'id', $this->tutor_user_id )->first();
        }

        return false;
    }
}