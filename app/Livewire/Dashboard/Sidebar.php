<?php

namespace App\Livewire\Dashboard;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Sidebar extends Component
{
    public $sections_dashboard = [], $sections_others = [], $start = 'overview';

    public static function checkActiveSection( $section )
    {
        //clock( $section );
        //clock( session()->get( 'dashboard-active' ) == $section );
        return session()->get( 'dashboard-active' ) == $section;

        if (
            session()->has( 'dashboard-profile-active' ) || session()->has( 'dashboard-profile-updated' ) || session()->has( 'dashboard-profile-message' ) || session()->has( 'dashboard-profile-alert' ) ||
            session()->has( 'dashboard-password-updated' ) || session()->has( 'dashboard-password-message' ) || session()->has( 'dashboard-password-alert' ) ) {
            return 'profile';
        } else if ( session()->has( 'dashboard-settings-active' ) || session()->has( 'dashboard-settings-updated' ) || session()->has( 'dashboard-settings-message' ) || session()->has( 'dashboard-settings-alert' ) ) {
            return 'settings';
        } else if ( session()->has( 'dashboard-lessons-active' ) || session()->has( 'dashboard-lessons-updated' ) || session()->has( 'dashboard-lessons-message' ) || session()->has( 'dashboard-lessons-alert' ) ) {
            return 'lessons';
        } else if ( session()->has( 'dashboard-messages-active' ) || session()->has( 'dashboard-messages-updated' ) || session()->has( 'dashboard-message-messages' ) || session()->has( 'dashboard-alert-messages' ) ) {
            return 'messages';
        } else if (
            session()->has( 'dashboard-assessment-active' ) || session()->has( 'dashboard-assessment-message' ) || session()->has( 'dashboard-assessment-alert' ) || session()->has( 'dashboard--assessment-updated' ) ) {
            return 'assessment';
        } else if (
            session()->has( 'dashboard-calendar-active' ) || session()->has( 'dashboard-calendar-message' ) || session()->has( 'dashboard-calendar-updated' ) || session()->has( 'dashboard-calendar-alert' ) ) {
            return 'calendar';
        } else {
            //return 'lessons';
            return 'overview';
        }
    }

    public function mount()
    {
        $this->sections_dashboard = [
            'overview' => 'gauge',
            'profile'  => 'circle-user',
            'password' => 'key',
            'settings' => 'gear',
        ];

        if ( Auth::user()->is_tutor() ) {
            $this->sections_dashboard['assessment'] = 'rectangle-list';
        }

        $this->sections_others = [
            //'calendar' => 'calendar',
            'lessons'  => 'receipt',
            'messages' => 'comment',
        ];
    }

    public function render()
    {
        //clock( session()->get( 'dashboard-active' ) );
        return view( 'livewire.dashboard.sidebar' );
    }

    public static function setSection( $active = 'overview' )
    {
        //clock( $active );
        session()->flash( 'dashboard-active', $active );
        //clock( session()->all() );
        /*if ($active === 'profile' || $active === 'password' ) {
    } else if ( $active === 'settings' ) {
    return 'settings';
    } else if ( $active === 'lessons' ) {
    return 'lessons';
    } else if ( $active === 'messages' ) {
    return 'messages';
    } else if (            $active === 'assessment' ) {
    return 'assessment';
    } else if ($active === 'calendar' ) {
    return 'calendar';
    } else {
    //return 'lessons';
    return 'overview';
    }*/
    }
}
