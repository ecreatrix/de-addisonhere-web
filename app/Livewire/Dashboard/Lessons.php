<?php

namespace App\Livewire\Dashboard;

use App\Models\Admin;
use App\Models\Lesson;
use App\Models\LessonRequest;
use App\Models\Tutee;
use App\Models\Tutor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Livewire\Component;

class Lessons extends Component
{
    public $total_sets_by_date, $set_by_date = 0, $previous_by_date = false, $next_by_date = false, $lessons_by_date = [],

    $total_sets_by_status, $set_by_status = 1, $previous_by_status = false, $next_by_status = false, $lessons_by_status = [],

    $lessons = [], $lessonUpdateDateUser = [], $lessonUpdateStatus = [];

    public function lesson_info( $user, $lesson, $status, $i )
    {
        $lesson_array           = $lesson->toArray();
        $lesson_array['status'] = $status;
        $lesson_array['future'] = $lesson->timeslot <= now();
        $lesson_array['i']      = $i;

        $request = $lesson->request;
        if ( $user->is_tutor() ) {
            $lesson_array['approved'] = 'approved' === $request->tutor_status;
        } else if ( $user->is_tutee() ) {
            $lesson_array['approved'] = 'approved' === $request->tutor_status;
        } else if ( $user->is_admin() ) {
            $lesson_array['approved'] = 'approved' === $request->tutor_status;
        }

        return $lesson_array;
    }

    public function mount()
    {
        $this->set_display_info();
    }

    public function nextSetByDate( $next )
    {
        if ( $next <= $this->total_sets_by_date ) {
            $this->set_by_date = $next;

            $this->previous_by_date = $this->set_by_date - 1;
            $this->next_by_date     = $this->set_by_date + 1;
        }
        $this->lessonUpdateStatus   = [];
        $this->lessonUpdateDateUser = [];
        //return session()->flash( 'dashboard-updated-lessons', false );
    }

    public function nextSetByStatus( $next )
    {
        if ( $next <= $this->total_sets_by_status ) {
            $this->set_by_status = $next;

            $this->previous_by_date = $this->set_by_date - 1;
            $this->next_by_date     = $this->set_by_date + 1;
        }
        $this->lessonUpdateStatus   = [];
        $this->lessonUpdateDateUser = [];
        //return session()->flash( 'dashboard-updated-lessons', false );
    }

    public function previousSetByDate( $previous )
    {
        if ( $previous >= 0 ) {
            $this->set_by_date = $previous;

            $this->previous_by_date = $this->set_by_date - 1;
            $this->next_by_date     = $this->set_by_date + 1;
        }
        $this->lessonUpdateStatus   = [];
        $this->lessonUpdateDateUser = [];

        //return session()->flash( 'dashboard-updated-lessons', false );
    }

    public function previousSetByStatus( $previous )
    {
        if ( $previous >= 0 ) {
            $this->set_by_status = $previous;

            $this->previous_by_date = $this->set_by_date - 1;
            $this->next_by_date     = $this->set_by_date + 1;
        }
        $this->lessonUpdateStatus   = [];
        $this->lessonUpdateDateUser = [];
        //return session()->flash( 'dashboard-updated-lessons', false );
    }

    public function render()
    {
        //clock( $this->total_sets_by_date );
        //clock( $this->set_by_date );
        return view( 'livewire.dashboard.lessons' );
    }

    public function selectedByDate( $id )
    {
        if ( in_array( $id, $this->lessonUpdateDateUser ) ) {
            $this->lessonUpdateDateUser[$id] = false;
        } else {
            $this->lessonUpdateDateUser[$id] = $id;
        }

        $this->lessonUpdateStatus = [];
    }

    public function selectedByStatus( $key )
    {
        if ( in_array( $key, $this->lessonUpdateStatus ) ) {
            $this->lessonUpdateStatus[$key] = false;
        } else {
            $this->lessonUpdateStatus[$key] = $key;
        }

        $this->lessonUpdateDateUser = [];
    }

    public function set_display_info()
    {
        $user          = Auth::user();
        $lessons_array = $user->lessons();

        // Figure out how to group/sort using eloquent
        $i             = 1;
        $set_by_date   = '';
        $set_by_status = '';

        $set_no_by_date   = 0;
        $set_no_by_status = 0;

        $previous_id     = '';
        $previous_date   = '';
        $previous_status = '';

        //clock( $lessons_array );
        foreach ( $lessons_array as $lesson ) {
            $i++;
            $id = $lesson->id;

            $status       = $lesson->is_approved() ? $lesson->status : 'request pending';
            $status_title = Str::replace( '-', ' ', Str::title( $status ) );
            $status_key   = LessonsHelpers::status_key( $status );

            $date     = $lesson->timeslot;
            $date_key = strtotime( $date );

            $this->lessons[$id] = $this->lesson_info( $user, $lesson, $status, $i );

            $current_month = date( 'm, Y', strtotime( $date ) );

            $lesson_user = $lesson->user();
            $user_id     = $lesson_user->id;
            $user_name   = $lesson_user->full_name();

            $month_key = (int) date( 'Ym', strtotime( $date ) );
            if (  ! array_key_exists( $month_key, $this->lessons_by_date ) ) {
                //New month
                $set_no_by_date++;
                //clock( $month_key );
            }
            $this->lessons_by_date[$month_key]['timeslot'] = date( 'F, Y', strtotime( $date ) );

            if ( $previous_status !== $status_key ) {
                //New status
                $set_no_by_status++;

                $previous_status = $status_key;

                $this->lessons_by_status[$status_key]['camel'] = Str::camel( $status );
                $this->lessons_by_status[$status_key]['slug']  = Str::slug( $status );
                $this->lessons_by_status[$status_key]['title'] = $status_title;
            }

            $this->lessons_by_date[$month_key]['users'][$user_id]['name']                                    = $lesson_user->full_name();
            $this->lessons_by_date[$month_key]['users'][$user_id]['status'][$status_key]['title']            = $status_title;
            $this->lessons_by_date[$month_key]['users'][$user_id]['status'][$status_key]['slug']             = Str::slug( $status );
            $this->lessons_by_date[$month_key]['users'][$user_id]['status'][$status_key]['items'][$date_key] = $id;

            $this->lessons_by_status[$status_key]['users'][$user_id]['name']             = $lesson_user->full_name();
            $this->lessons_by_status[$status_key]['users'][$user_id]['items'][$date_key] = $id;
        }

        ksort( $this->lessons_by_date );
        $this->lessons_by_date = array_values( $this->lessons_by_date );
        //clock( $this->lessons_by_date );

        ksort( $this->lessons_by_status );
        //clock( $this->lessons_by_status );
        //clock( $this->lessons );

        $this->total_sets_by_date = $set_no_by_date - 1;
        //clock( $this->total_sets_by_date );
        $this->total_sets_by_status = $set_no_by_status;
    }

    public function statusAcceptLesson( $lesson_id )
    {
        $user = Auth::user();
        LessonsHelpers::statusAcceptLesson( $user, $lesson_id );
        $this->set_display_info();
    }

    public function statusRejectLesson( $lesson_id )
    {
        $user = Auth::user();
        LessonsHelpers::statusRejectLesson( $user, $lesson_id );
        $this->set_display_info();
    }
}
