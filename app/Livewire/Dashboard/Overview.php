<?php

namespace App\Livewire\Dashboard;

use Livewire\Component;

class Overview extends Component
{
    public function mount()
    {
        //session()->flush();
        $sessions = ['profile', 'password', 'settings', 'lessons', 'messages', 'assessment', 'calendar'];
        foreach ( $sessions as $session ) {
            session()->forget( 'dashboard-updated-' . $session );
            session()->forget( 'dashboard-message-' . $session );
            session()->forget( 'dashboard-alert-' . $session );
        }
    }

    public function render()
    {
        //session()->flush();
        return view( 'livewire.dashboard.overview' );

        if ( \Auth::user()->is_tutor() ) {
            return view( 'livewire.dashboard-tutor' );
        } else {
            return view( 'livewire.dashboard-tutee' );
        }
    }

    public function tutor_menu_items()
    {
        return [
            'Overview'   => 'overview',
            'Clock In'   => 'clock-in',
            'Calendar'   => 'calendar',
            'Tutee List' => 'tutees',
            'Messages'   => 'messages',
        ];
    }
}
