<?php

namespace App\Livewire\Dashboard;

use App\Helpers\Misc;
use App\Livewire\Dashboard\Sidebar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules as Rules;
use Illuminate\Validation\ValidationException;
use Livewire\Component;

class Profile extends Component
{
    public $firstName, $lastName, $phone, $email, $address, $city, $province, $postalCode;

    public function clearSuccessMessage()
    {
        Sidebar::setSection( 'profile' );
        session()->forget( 'dashboard-message-profile' );
    }

    public function formatPhoneNumber()
    {
        Sidebar::setSection( 'profile' );

        $this->phone = Misc::formatPhone( $this->phone );
    }

    public function mount()
    {
        $user            = Auth::user();
        $this->firstName = $user->firstName;
        $this->lastName  = $user->lastName;

        $this->phone = Misc::formatPhone( $user->phone );
        $this->email = $user->email;

        $this->address    = $user->address;
        $this->city       = $user->city;
        $this->province   = $user->province;
        $this->postalCode = $user->postalCode;
    }

    public function profile()
    {
        Sidebar::setSection( 'profile' );

        $user = Auth::user();

        $validator = Validator::make( [
            'firstName'  => $this->firstName,
            'lastName'   => $this->lastName,

            'phone'      => Misc::formatPhone( $this->phone, false ),
            'email'      => $this->email,

            'address'    => $this->address,
            'city'       => $this->city,
            'province'   => Misc::provinceValue( $this->province ),
            'postalCode' => $this->postalCode,
        ], [
            'firstName'  => 'required|string',
            'lastName'   => 'required|string',

            'phone'      => 'required|digits:10',
            'email'      => 'required|email|unique:users,email,' . $user->id . ',id',

            'address'    => 'required',
            'city'       => 'required',
            'province'   => 'required',
            'postalCode' => 'required',
        ], [
            'required' => [
                'firstName'  => 'Please enter your first name.',
                'lastName'   => 'Please enter your last name.',
                'email'      => 'Please enter your email.',
                'phone'      => 'Please enter your phone number.',
                'address'    => 'Please enter your street address.',
                'city'       => 'Please enter your city/town.',
                'province'   => 'Please enter your province.',
                'postalCode' => 'Please enter your postal code.',
            ], 'unique' => [
                'email' => 'This email is in use.',
            ], 'digits' => [
                'phone' => 'Please enter a valid phone number.',
            ],
        ] );
        //clock( $validator );

        if ( $validator->fails() ) {
            session()->flash( 'dashboard-alert-profile', 'Please fix above errors and try again.' );
            throw new ValidationException( $validator );
        }

        $this->resetErrorBag();

        $data = $validator->validate();

        $user_info = [
            'firstName'  => $data['firstName'],
            'lastName'   => $data['lastName'],

            'phone'      => $data['phone'],
            'email'      => $data['email'],

            'address'    => $data['address'],
            'city'       => $data['city'],
            'province'   => $data['province'],
            'postalCode' => $data['postalCode'],
        ];
        //clock( $user_info );

        $user->firstName  = $data['firstName'];
        $user->lastName   = $data['lastName'];
        $user->phone      = $data['phone'];
        $user->email      = $data['email'];
        $user->address    = $data['address'];
        $user->city       = $data['city'];
        $user->province   = $data['province'];
        $user->postalCode = $data['postalCode'];
        //clock( $user );

        $saved = $user->save();

        //clock( $saved );

        if (  ! $saved ) {
            // TODO send automatic email to admin with errors
            return session()->flash( 'dashboard-alert-profile', 'Profile update unsuccessful, please get in touch for help' );
        }

        return session()->flash( 'dashboard-message-profile', 'Profile updated' );
    }

    public function render()
    {
        return view( 'livewire.dashboard.profile' );
    }
}
