<?php

namespace App\Livewire\Dashboard;

use App\Helpers\Misc;
use App\Livewire\Dashboard\Sidebar;
use App\Models\Tutor;
use App\Models\User;
use App\Models\UserMeta;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Livewire\Component;

class Settings extends Component
{
    public $weekStart = '';

    protected $messages = [
        'weekStart.required' => 'Please select a day.',
    ];

    public function clearSuccessMessage()
    {
        Sidebar::setSection( 'settings' );
        session()->forget( 'dashboard-message-settings' );
    }

    public function mount()
    {
        $user  = Auth::user();
        $metas = UserMeta::where( 'user_id', $user->id )->get()->keyBy( 'key' )->all();

        $this->weekStart = array_key_exists( 'weekStart', $metas ) ? $metas['weekStart']->value : false;
    }

    public function render()
    {
        return view( 'livewire.dashboard.settings' );
    }

    public function settings()
    {
        Sidebar::setSection( 'settings' );
        $user      = Auth::user();
        $validator = Validator::make( [
            'weekStart' => $this->weekStart,
        ], [
            'weekStart' => ['required', 'not_in:' . 'false'],
        ] );

        if ( $validator->fails() ) {
            session()->flash( 'dashboard-alert-settings', 'Please fix above errors and try again.' );
            throw new ValidationException( $validator );
        }
        $this->resetErrorBag();

        $data = $validator->validate();

        $params = ['user_id' => $user->id];

        $saved = UserMeta::updateOrCreate( array_merge( $params, ['key' => 'weekStart'] ), ['key' => 'weekStart', 'value' => $this->weekStart] );

        if (  ! $saved ) {
            // TODO send automatic email to admin with errors
            return session()->flash( 'dashboard-alert-settings', 'Settings update unsuccessful, please get in touch for help' );
        }

        return session()->flash( 'dashboard-settings-message', 'Settings updated' );
    }
}
