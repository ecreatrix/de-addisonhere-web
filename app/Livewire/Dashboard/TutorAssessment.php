<?php

namespace App\Livewire\Dashboard;

use App\Helpers\Misc;
use App\Models\Tutor;
use App\Models\User;
use App\Models\UserMeta;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Livewire\Component;

class TutorAssessment extends Component
{
    public $firstName, $lastName, $phone, $email, $address, $city, $province, $postalCode, $gender, $genderExtra, $age, $education, $availability, $travel, $nativeLanguage, $internetProficiency, $techTime, $software, $otherSoftware, $appExperience, $otherAppExperience, $appsUsed, $otherAppsUsed, $internetSolutions, $internetSolutionsExtra, $printerExperience, $printerExperienceExtra, $tutoringExperience, $tutoringExperienceExtra, $assistedRelativesExperience, $assistedRelativesExperienceExtra, $comments, $languages = [], $devices = [];

    public $genderClass = 'hidden', $internetSolutionsExtraClass = 'hidden', $printerExperienceExtraClass = 'hidden', $tutoringExperienceExtraClass = 'hidden', $assistedRelativesExperienceExtraClass = 'hidden', $otherSoftwareClass = 'hidden', $otherAppExperienceClass = 'hidden', $otherAppsUsedClass = 'hidden';

    public $user;

    public function changeAppExperienceClass( $skip = true )
    {
        $this->otherAppExperienceClass = $this->classChangeField( 'other', $this->appExperience );

        if ( $skip ) {
            $this->skipRender();
        }
    }

    public function changeAppsUsedClass( $skip = true )
    {
        $this->otherAppsUsedClass = $this->classChangeField( 'other', $this->appsUsed );

        if ( $skip ) {
            $this->skipRender();
        }
    }

    public function changeAssistedRelativesExperienceExtraClass( $skip = true )
    {
        $this->assistedRelativesExperienceExtraClass = $this->classChangeField( true, boolval( $this->assistedRelativesExperience ) );

        if ( $skip ) {
            $this->skipRender();
        }
    }

    public function changeGenderClass( $skip = true )
    {
        $this->genderClass = $this->classChangeField( 'other', $this->gender );

        if ( $skip ) {
            $this->skipRender();
        }
    }

    public function changeInternetSolutionsExtraClass( $skip = true )
    {
        $this->internetSolutionsExtraClass = $this->classChangeField( true, boolval( $this->internetSolutions ) );

        if ( $skip ) {
            $this->skipRender();
        }
    }

    public function changePrinterExperienceExtraClass( $skip = true )
    {
        $this->printerExperienceExtraClass = $this->classChangeField( true, boolval( $this->printerExperience ) );

        if ( $skip ) {
            $this->skipRender();
        }
    }

    public function changeSoftwareClass( $skip = true )
    {
        $this->otherSoftwareClass = $this->classChangeField( 'other', $this->software );

        if ( $skip ) {
            $this->skipRender();
        }
    }

    public function changeTutoringExperienceExtraClass( $skip = true )
    {
        $this->tutoringExperienceExtraClass = $this->classChangeField( true, boolval( $this->tutoringExperience ) );

        if ( $skip ) {
            $this->skipRender();
        }
    }

    public function classChangeField( $other_field_key, $field )
    {
        if ( is_array( $field ) && in_array( $other_field_key, $field ) ) {
            $class = '';
        } else if ( is_bool( $field ) && $field === $other_field_key ) {
            $class = '';
        } else if ( is_string( $field ) && $field === $other_field_key ) {
            $class = '';
        } else if ( is_string( $field ) && $field === $other_field_key ) {
            $class = '';
        } else {
            $class = 'hidden';
        }

        return $class;
    }

    public function deviceAdd()
    {
        $this->devices[] = [
            'type'        => '',
            'useLength'   => '',
            'purposes'    => '',
            'proficiency' => '',
        ];

        $this->skipRender();
    }

    public function deviceRemove( $device_i )
    {
        unset( $this->devices[$device_i] );
        $this->devices = array_values( $this->devices );

        $this->skipRender();
    }

    public function languageAdd()
    {
        $this->languages[] = ['name' => '', 'proficiency' => ''];

        $this->skipRender();
    }

    public function languageRemove( $language_i )
    {
        unset( $this->languages[$language_i] );
        $this->languages = array_values( $this->languages );

        $this->skipRender();
    }

    public function mount()
    {
        //$user = User::all()->random();
        //$user_is_admin = $user->is_admin();
        //$admin_model   = Admin::where( 'user_id', $user->id )->exists();

        //clock( 'User ID is ' . $user->id . ' is a ' . $user->get_type() . '.' );
        //$this->messages = array_merge( $this->set_default_message(), $this->messages );
        $user = auth()->user();
        if ( $user ) {
            $metas = UserMeta::where( 'user_id', $user->id )->get()->keyBy( 'key' )->all();
            //clock( $metas );
            $this->gender         = array_key_exists( 'gender', $metas ) ? $metas['gender']->value : false;
            $this->genderExtra    = array_key_exists( 'genderExtra', $metas ) ? $metas['genderExtra']->value : false;
            $this->age            = array_key_exists( 'age', $metas ) ? $metas['age']->value : false;
            $this->education      = array_key_exists( 'education', $metas ) ? $metas['education']->value : false;
            $this->availability   = array_key_exists( 'availability', $metas ) ? $metas['availability']->value : false;
            $this->travel         = array_key_exists( 'travel', $metas ) ? $metas['travel']->value : false;
            $this->nativeLanguage = array_key_exists( 'nativeLanguage', $metas ) ? $metas['nativeLanguage']->value : false;

            $languages = array_key_exists( 'languages', $metas ) ? json_decode( $metas['languages']->value ) : false;
            if (  ! is_null( $languages ) && is_array( $languages ) ) {
                foreach ( $languages as $language ) {
                    $this->languages[] = [
                        'name'        => $language->name,
                        'proficiency' => $language->proficiency,
                    ];
                }
            }

            $devices = array_key_exists( 'devices', $metas ) ? json_decode( $metas['devices']->value ) : false;
            if (  ! is_null( $devices ) && is_array( $devices ) ) {
                foreach ( $devices as $device ) {
                    $this->devices[] = [
                        'type'        => $device->type,
                        'useLength'   => $device->useLength,
                        'purposes'    => $device->purposes,
                        'proficiency' => $device->proficiency,
                    ];
                }
            }

            $this->internetProficiency = array_key_exists( 'internetProficiency', $metas ) ? $metas['internetProficiency']->value : false;

            $this->techTime      = array_key_exists( 'techTime', $metas ) ? $metas['techTime']->value : false;
            $this->software      = array_key_exists( 'software', $metas ) ? json_decode( $metas['software']->value ) : false;
            $this->otherSoftware = array_key_exists( 'otherSoftware', $metas ) ? $metas['otherSoftware']['value'] : '';

            $this->appExperience      = array_key_exists( 'appExperience', $metas ) ? json_decode( $metas['appExperience']->value ) : false;
            $this->otherAppExperience = array_key_exists( 'otherAppExperience', $metas ) ? $metas['otherAppExperience']['value'] : '';
            $this->appsUsed           = array_key_exists( 'appsUsed', $metas ) ? json_decode( $metas['appsUsed']->value ) : false;
            $this->otherAppsUsed      = array_key_exists( 'otherAppsUsed', $metas ) ? $metas['otherAppsUsed']['value'] : '';

            $this->internetSolutions      = array_key_exists( 'internetSolutions', $metas ) ? json_decode( $metas['internetSolutions']->value ) : false;
            $this->internetSolutionsExtra = array_key_exists( 'internetSolutionsExtra', $metas ) ? $metas['internetSolutionsExtra']['value'] : '';

            $this->printerExperience      = array_key_exists( 'printerExperience', $metas ) ? json_decode( $metas['printerExperience']->value ) : false;
            $this->printerExperienceExtra = array_key_exists( 'printerExperienceExtra', $metas ) ? $metas['printerExperienceExtra']['value'] : '';

            $this->tutoringExperience      = array_key_exists( 'tutoringExperience', $metas ) ? json_decode( $metas['tutoringExperience']->value ) : false;
            $this->tutoringExperienceExtra = array_key_exists( 'tutoringExperienceExtra', $metas ) ? $metas['tutoringExperienceExtra']['value'] : '';

            $this->assistedRelativesExperience      = array_key_exists( 'assistedRelativesExperience', $metas ) ? json_decode( $metas['assistedRelativesExperience']->value ) : false;
            $this->assistedRelativesExperienceExtra = array_key_exists( 'assistedRelativesExperienceExtra', $metas ) ? $metas['assistedRelativesExperienceExtra']['value'] : '';

            $this->comments = array_key_exists( 'comments', $metas ) ? $metas['comments']['value'] : '';

            $this->changeInternetSolutionsExtraClass( false );
            $this->changePrinterExperienceExtraClass( false );
            $this->changeTutoringExperienceExtraClass( false );
            $this->changeAppExperienceClass( false );
            $this->changeAppsUsedClass( false );
            $this->changeSoftwareClass( false );
            $this->changeGenderClass( false );
            $this->changeAssistedRelativesExperienceExtraClass( false );
        }
    }

    public function render()
    {
        if ( count( $this->getErrorBag()->all() ) > 0 ) {
            session()->flash( 'assessment-alert', 'Submission unsuccessful, look for errors in the form above.' );
        }

        //clock( $this->internetSolutions );

        return view( 'livewire.dashboard.tutor-assessment' );
    }

    /*public function set_default_message()
    {
    $messages = [];

    foreach ( $this->validation as $key => $field ) {
    $validate = explode( '|', $field );
    if ( in_array( 'required', $validate ) ) {
    $messages[$key . ''] = 'This field is required';
    }
    }

    return $messages;
    }*/

    public function storeAssessment()
    {
        $user = auth()->user();

        $validator = Validator::make( [
            'gender'                           => $this->gender,
            'genderExtra'                      => $this->genderExtra,
            'age'                              => $this->age,
            'education'                        => $this->education,
            'availability'                     => $this->availability,
            'travel'                           => $this->travel,
            'nativeLanguage'                   => $this->nativeLanguage,

            'languages'                        => $this->languages,

            'devices'                          => $this->devices,

            'internetProficiency'              => $this->internetProficiency,

            'techTime'                         => $this->techTime,

            'software'                         => $this->software,
            'otherSoftware'                    => $this->otherSoftware,

            'appExperience'                    => $this->appExperience,
            'otherAppExperience'               => $this->otherAppExperience,

            'appsUsed'                         => $this->appsUsed,
            'otherAppsUsed'                    => $this->otherAppsUsed,

            'internetSolutions'                => $this->internetSolutions,
            'internetSolutionsExtra'           => $this->internetSolutionsExtra,

            'printerExperience'                => $this->printerExperience,
            'printerExperienceExtra'           => $this->printerExperienceExtra,

            'tutoringExperience'               => $this->tutoringExperience,
            'tutoringExperienceExtra'          => $this->tutoringExperienceExtra,

            'assistedRelativesExperience'      => $this->assistedRelativesExperience,
            'assistedRelativesExperienceExtra' => $this->assistedRelativesExperienceExtra,

            'comments'                         => $this->comments,
        ], [
            'gender'                           => 'required|string',
            'genderExtra'                      => 'required_if:gender,other',
            'age'                              => 'required|string',
            'education'                        => 'required|string',
            'availability'                     => 'required|string',
            'travel'                           => 'required|string',
            'nativeLanguage'                   => 'required|string',
            'languages'                        => 'required',
            'devices'                          => 'required',

            'internetProficiency'              => 'required|string',

            'techTime'                         => 'required|string',

            'software'                         => 'required|array',
            'otherSoftware'                    => 'required_if:software.other,other',

            'appExperience'                    => 'required|array',
            'otherAppExperience'               => 'required_if:appExperience.other,other',

            'appsUsed'                         => 'required|array',
            'otherAppsUsed'                    => 'required_if:appsUsed.other,other',

            'internetSolutions'                => '',
            'internetSolutionsExtra'           => 'required_if:internetSolutions,true',

            'printerExperience'                => '',
            'printerExperienceExtra'           => 'required_if:printerExperience,true',

            'tutoringExperience'               => '',
            'tutoringExperienceExtra'          => 'required_if:tutoringExperience,true',

            'assistedRelativesExperience'      => '',
            'assistedRelativesExperienceExtra' => 'required_if:assistedRelativesExperience,true',

            'comments'                         => '',
        ], [
            'required_if'   => [
                'genderExtra'                      => 'This field is required',
                'otherSoftware'                    => 'This field is required',
                'otherAppExperience'               => 'This field is required',
                'otherAppsUsed'                    => 'This field is required',
                'internetSolutionsExtra'           => 'Please enter more information.',
                'printerExperienceExtra'           => 'Please enter more information.',
                'tutoringExperienceExtra'          => 'Please enter more information.',
                'assistedRelativesExperienceExtra' => 'Please enter more information.',
            ],
            'required_with' => [
            ],
            'required'      => [
                'gender'                      => 'This field is required',
                'age'                         => 'This field is required',
                'education'                   => 'This field is required',
                'availability'                => 'This field is required',
                'travel'                      => 'This field is required',
                'nativeLanguage'              => 'This field is required',

                'internetProficiency'         => 'This field is required',
                'techTime'                    => 'This field is required',
                'software'                    => 'This field is required',
                'appsUsed'                    => 'This field is required',
                'appExperience'               => 'This field is required',

                'internetSolutions'           => 'This field is required',
                'printerExperience'           => 'This field is required',
                'tutoringExperience'          => 'This field is required',
                'assistedRelativesExperience' => 'This field is required',
            ],
        ] );

        //clock( $validator );
        if ( $validator->fails() ) {
            session()->flash( 'assessment-alert', 'Please fix above errors and try again.' );
            throw new ValidationException( $validator );
        }

        $this->resetErrorBag();

        $validated = $validator->validate();

        $params = ['user_id' => $user->id];

        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'gender'] ), ['key' => 'gender', 'value' => $this->gender] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'genderExtra'] ), ['key' => 'genderExtra', 'value' => $this->genderExtra] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'age'] ), ['key' => 'age', 'value' => $this->age] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'education'] ), ['key' => 'education', 'value' => $this->education] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'availability'] ), ['key' => 'availability', 'value' => $this->availability] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'travel'] ), ['key' => 'travel', 'value' => $this->travel] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'nativeLanguage'] ), ['key' => 'nativeLanguage', 'value' => $this->nativeLanguage] );

        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'languages'] ), ['key' => 'languages', 'value' => json_encode( $this->languages )] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'devices'] ), ['key' => 'devices', 'value' => json_encode( $this->devices )] );

        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'internetProficiency'] ), ['key' => 'internetProficiency', 'value' => $this->internetProficiency] );

        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'techTime'] ), ['key' => 'techTime', 'value' => $this->techTime] );

        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'software'] ), ['key' => 'software', 'value' => json_encode( $this->software )] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'otherSoftware'] ), ['key' => 'otherSoftware', 'value' => $this->otherSoftware] );

        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'appExperience'] ), ['key' => 'appExperience', 'value' => json_encode( $this->appExperience )] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'otherAppExperience'] ), ['key' => 'otherAppExperience', 'value' => $this->otherAppExperience] );

        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'appsUsed'] ), ['key' => 'appsUsed', 'value' => json_encode( $this->appsUsed )] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'otherAppsUsed'] ), ['key' => 'otherAppsUsed', 'value' => $this->otherAppsUsed] );

        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'internetSolutions'] ), ['key' => 'internetSolutions', 'value' => true == $this->internetSolutions ? true : false] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'internetSolutionsExtra'] ), ['key' => 'internetSolutionsExtra', 'value' => $this->internetSolutionsExtra] );

        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'printerExperience'] ), ['key' => 'printerExperience', 'value' => true == $this->printerExperience ? true : false] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'printerExperienceExtra'] ), ['key' => 'printerExperienceExtra', 'value' => $this->printerExperienceExtra] );

        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'tutoringExperience'] ), ['key' => 'tutoringExperience', 'value' => true == $this->tutoringExperience ? true : false] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'tutoringExperienceExtra'] ), ['key' => 'tutoringExperienceExtra', 'value' => $this->tutoringExperienceExtra] );

        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'assistedRelativesExperience'] ), ['key' => 'assistedRelativesExperience', 'value' => true == $this->assistedRelativesExperience ? true : false] );
        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'assistedRelativesExperienceExtra'] ), ['key' => 'assistedRelativesExperienceExtra', 'value' => $this->assistedRelativesExperienceExtra] );

        UserMeta::updateOrCreate( array_merge( $params, ['key' => 'comments'] ), ['key' => 'comments', 'value' => $this->comments] );

        session()->flash( 'assessment-message', 'Your assessment has been submitted successfully.' );
    }

    protected function rules()
    {
        return [
            'gender'                           => 'required|string',
            'age'                              => 'required|string',
            'education'                        => 'required|string',
            'availability'                     => 'required|string',
            'travel'                           => 'required|string',
            'nativeLanguage'                   => 'required|string',
            'languages'                        => 'required',
            'devices'                          => 'required',

            //'languageName.0'                   => 'required',
            //'languageProficiency.0'            => 'required',
            //'languageName.*'                   => 'required',
            //'languageProficiency.*'            => 'required',

            //'deviceName.0'                     => 'required',
            //'deviceProficiency.0'              => 'required',
            //'deviceName.*'                     => 'required',
            //'deviceProficiency.*'              => 'required',

            'internetProficiency'              => 'required|string',

            'techTime'                         => 'required|string',

            //'software'                         => 'required|array',
            'otherSoftware'                    => 'required_if:software.other,other',

            //'app'                              => 'required|array',
            'otherApp'                         => 'required_if:app.other,other',

            //'appsUsed'                         => 'required|array',
            'otherAppsUsed'                    => 'required_if:appsUsed.other,other',

            //'internetSolutions'                => '',
            'internetSolutionsExtra'           => 'required_with:internetSolutions',

            'printerExperience'                => '',
            'printerExperienceExtra'           => 'required_with:printerExperience',

            'tutoringExperience'               => '',
            'tutoringExperienceExtra'          => 'required_with:tutoringExperience',

            'assistedRelativesExperience'      => '',
            'assistedRelativesExperienceExtra' => 'required_with:assistedRelativesExperience',

            'comments'                         => '',
        ];
    }

    private function resetInputFields()
    {
        $this->name  = '';
        $this->phone = '';

        $this->skipRender();
    }
}
