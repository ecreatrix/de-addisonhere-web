<?php

namespace App\Livewire\Dashboard;

use App\Models\Admin;
use App\Models\Tutee;
use App\Models\Tutor;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules as Rules;
use Illuminate\Validation\ValidationException;
use Livewire\Component;
use Wa\Laramessage\Models\Message;
use Wa\Laramessage\Models\MessageAttachment;
use Wa\Laramessage\Models\MessageMeta;
use Wa\Laramessage\Models\MessageRecipient;
use Wa\Laramessage\Models\MessageTag;

class Messages extends Component
{
    public $all_messages = [], $actionable = [], $user = false, $active_chain = false, $active_message = false, $composeParentId = null, $composeRecipient = null, $composeSubject = null, $composeBody = null;

    public function actions_activate( $top_id, $id )
    {
        $this->active_message = $id;
        $this->active_chain   = $top_id;

        //clock( 'chain: ' . $this->active_chain . ', activate: ' . $this->active_message );

        //return session()->flash( 'dashboard-messages-updated', true );
    }

    public function actions_delete( $id )
    {
        $message = Message::withTrashed()->find( $id );

        if ( $message->trashed() ) {
            $message->restore();
        } else {
            $message->delete();
        }

        //$message->save();
        $this->update_messages( $message );
    }

    public function actions_read( $top_id, $id, $read )
    {
        $this->actions_activate( $top_id, $id );

        $date = null;
        if ( $read ) {
            $date = now();
        }

        $messages = Message::withTrashed()->whereId( $top_id )->orWhere( 'parent_id', $top_id )->get();
        foreach ( $messages as $message ) {
            //clock( 'message: ' . $message->id );
            $ancestors = $message->ancestors();
            if ( $ancestors && $ancestors->first() && $ancestors->first()->id != $message->id ) {
                foreach ( $ancestors as $ancestor ) {
                    $this->actions_read_mark( $ancestor, $date );
                }
            }

            $this->actions_read_mark( $message, $date );

            $children = [];
            $children = $message->children( $message, $children );
            if ( $children ) {
                //clock( 'children' );
                foreach ( $children as $child ) {
                    $this->actions_read_mark( $child, $date );
                }
            }
        }
    }

    public function actions_read_mark( $message, $date )
    {
        $meta               = MessageMeta::where( [['user_id', $this->user->id], ['message_id', $message->id]] )->first();
        $meta->last_read_at = $date;

        $meta->save();
        $this->update_messages( $message );
    }

    public function actions_set_parent( $top_id )
    {
        if ( $top_id != $this->composeParentId ) {
            $this->composeRecipient = null;
            $this->composeSubject   = null;
            $this->composeBody      = null;

            $this->resetErrorBag();
        }

        $this->composeParentId = $top_id;
    }

    public function actions_tags( $top_id, $id, $name = null )
    {
        $this->actions_activate( $top_id, $id );

        $this->user = auth()->user();

        $message = Message::withTrashed()->find( $id );

        $tags     = $message->tags;
        $archived = $tags->where( 'name', 'archived' )->first();
        $reported = $tags->where( 'name', 'reported' )->first();
        if ( 'unarchive' === $name && $archived && 'archived' === $archived->name ) {
            $archived->delete();
        } else if ( 'unreport' === $name && $reported && 'reported' === $reported->name ) {
            $reported->delete();
        } else {
            $new = MessageTag::create( ['user_id' => $this->user->id, 'message_id' => $id, 'name' => $name] );
        }

        $this->update_messages( $message );
    }

    public function compose()
    {
        $validator = Validator::make( [
            'composeRecipient' => $this->composeRecipient,
            'composeSubject'   => $this->composeSubject,
            'composeBody'      => $this->composeBody,
        ], [
            'composeRecipient' => 'required|string',
            'composeSubject'   => 'required|string',
            'composeBody'      => 'required|string',
        ], [
            'composeRecipient.required' => 'Please choose a recipient.',
            'composeSubject.required'   => 'Please enter your subject.',
            'composeBody.required'      => 'Please enter your message.',
        ] );

        if ( $validator->fails() ) {
            session()->flash( 'dashboard-alert-compose-messages', 'Please fix above errors and try again' );
            throw new ValidationException( $validator );
        }

        $this->resetErrorBag();

        $data = $validator->validate();

        $message = Message::withTrashed()->create( ['sender_id' => $this->user->id, 'parent_id' => $this->composeParentId, 'subject' => $this->composeSubject, 'body' => $this->composeBody] );

        if (  ! $message ) {
            // TODO send automatic email to admin with errors
            return session()->flash( 'dashboard-alert-compose-messages', 'Message was not sent, please get in touch for help' );
        }

        MessageMeta::create( ['user_id' => $this->user->id, 'message_id' => $message->id] );

        $recipients = $this->composeRecipient;
        if (  ! is_array( $recipients ) ) {
            $recipients = [$recipients];
        }

        foreach ( $recipients as $recipient ) {
            $recipient = (int) $recipient;

            MessageMeta::create( ['user_id' => $recipient, 'message_id' => $message->id] );
            MessageRecipient::create( ['recipient_id' => $recipient, 'message_id' => $message->id] );
        }

        $key = $message->created_at->getTimestamp();

        if ( null == $this->composeParentId ) {
            $this->all_messages[$key]['parent'] = $this->get_info( $message );
        } else {
            $parent_key = Message::withTrashed()->find( $this->composeParentId )->created_at->getTimestamp();

            $this->all_messages[$parent_key]['children'][$key] = $this->get_info( $message );
        }

        return session()->flash( 'dashboard-message-compose-messages', 'Your message was sent' );
    }

    public function get_classes( $top_id, $message_id, $button = false )
    {
        $message = Message::withTrashed()->find( $message_id );

        if ( 25 == $message_id ) {
            //clock( $message->trashed() && $message->deleted_at );
        }

        $sender = $message->user;
        $parent = $message->parent;

        $children = [];
        $children = $message->children( $message, $children );

        $meta = $message->metas( $this->user );
        $tags = $message->tags()->pluck( 'name' )->all();

        $classes = ['dashboard-message-' . $message_id, 'p-4', 'hover:bg-neutral-100'];

        $classes[] = $parent ? 'has-parent parent-' . $parent->id : 'no-parent';

        //clock( $message_id . ' ' . $meta->last_read_at );
        $classes[] = null != $meta->last_read_at ? 'read' : 'unread border-l-4 border-blue';

        if ( $button ) {
            $classes[] = is_array( $tags ) && in_array( 'archived', $tags ) ? 'archived opacity-20 border-l-4 border-gray-600' : '';

            $classes[] = is_array( $tags ) && in_array( 'reported', $tags ) ? 'reported border-l-4 border-pink' : '';
        }

        $classes[] = $sender->id === $this->user->id ? 'sender' : 'recipient';

        $classes[] = $children ? 'has-child' : null;

        $classes[] = is_countable( $tags ) && count( $tags ) > 0 ? implode( ' ', $tags ) : 'no-tags';

        $classes[] = $message->trashed() && $message->deleted_at ? 'deleted' : null;

        if ( $button && $children ) {
            foreach ( $children as $child ) {
                $child_tags = $child->tags()->pluck( 'name' )->all();

                $classes[] = is_countable( $child_tags ) && count( $child_tags ) > 0 ? implode( ' ', $child_tags ) : null;
            }
        }

        $classes = implode( ' ', array_filter( $classes ) );

        return $classes;
    }

    public function get_info( $message )
    {
        $message_id = $message->id;
        $sender     = $message->user;
        $recipients = $message->recipients;
        $parent     = $message->parent;

        $meta       = $message->metas( $this->user );
        $tags       = $message->tags()->pluck( 'name' )->all();
        $created_at = $message->created_at;

        $parent_id = $parent ? $parent->id : $message_id;

        $recipients_names = [];
        foreach ( $recipients as $recipient ) {
            $recipients_names[] = $recipient->recipient->full_name();
        }

        if ( $message->user_permitted( $this->user ) ) {
            $info = [
                'message_id'       => $message_id,
                'deleted'          => $message->deleted_at ? $message->deleted_at->addMonth()->format( 'M d, Y' ) : null,
                'parent_id'        => null == $parent ? false : $parent->id,
                'sender_name'      => $sender->full_name(),
                'recipients_names' => implode( ', ', $recipients_names ),
                'date'             => $created_at->format( 'M d, Y' ),
                'date_time'        => $created_at->format( 'M d, Y' ) . ' ' . $created_at->format( 'g:i A' ),
                'message'          => $message->toArray(),
                'meta'             => $meta->toArray(),
                'tags'             => $tags,
                'sender'           => $sender->toArray(),
                'recipients'       => $recipients->toArray(),
            ];

            return $info;
        }
    }

    public function mount()
    {
        $this->user = auth()->user();

        foreach ( $this->user->actionable() as $user ) {
            $label = $user->full_name();

            $label = $user->is_admin() ? 'Admin: ' . $label : $label;
            $label = $user->is_tutor() ? 'Tutor: ' . $label : $label;
            $label = $user->is_tutee() ? 'Tutee: ' . $label : $label;

            $this->actionable[$user->id] = $label;
        }

        $messages_array = $this->user->all_messages();

        $this->users = User::where( 'id', 3 )->orWhere( 'id', 1 )->get();

        $messages = [];
        foreach ( $messages_array as $message ) {
            $message_id = $message->id;
            $parent     = $message->parent;
            $children   = [];
            $children   = $message->children( $message, $children );
            $created_at = $message->created_at->getTimestamp();

            $info = $this->get_info( $message );
            $key  = $created_at;

            $messages[$key]['parent'] = $this->get_info( $message );
            if ( $children ) {
                foreach ( $children as $child ) {
                    $child_key                              = $child->created_at->getTimestamp();
                    $messages[$key]['children'][$child_key] = $this->get_info( $child );
                }
            }
        }

        $this->all_messages = $messages;
    }

    public function render()
    {
        return view( 'livewire.dashboard.messages' );
    }

    public function update_messages( $message )
    {
        $ancestor = $message->first_in_chain();
        $children = [];
        $children = $message->children( $message, $children );
        $key      = $message->created_at->getTimestamp();

        if ( $ancestor->id !== $message->id ) {
            $created_at = $message->created_at->getTimestamp();

            $this->all_messages[$ancestor->created_at->getTimestamp()]['children'][$key] = $this->get_info( $message );
            //clock( $message->id . ' ' . $this->all_messages[$ancestor->created_at->getTimestamp()]['children'][$key]['meta']['last_read_at'] );
        } else {
            $this->all_messages[$key]['parent'] = $this->get_info( $message );
            //clock( $message->id . ' ' . $this->all_messages[$key]['parent']['meta']['last_read_at'] );
        }

        //clock( $this->all_messages );
    }
}
