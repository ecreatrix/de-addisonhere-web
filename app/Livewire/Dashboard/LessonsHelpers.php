<?php

namespace App\Livewire\Dashboard;

use App\Models\Lesson;
use App\Models\LessonRequest;
use CalendarHelpers;
//use Wa\LivewireCalendar\LivewireCalendarHelpers;
use Illuminate\Support\Str;

class LessonsHelpers
{
    public static function event_class( $event )
    {
        $parent_classes = CalendarHelpers::event_class( $event );
        $classes        = explode( ' ', $parent_classes );

        $lesson = Lesson::whereId( $event['id'] )->first();
        $status = $lesson->is_approved() ? $lesson->status : 'request pending';
        //clock( $lesson );

        $classes[] = Str::slug( 'status-' . $status );

        return implode( ' ', $classes );
    }

    public static function request_approved_status()
    {
        return 'approved';
    }

    public static function statusAcceptLesson( $user, $lesson_id )
    {
        $lesson_request = LessonRequest::where( 'lesson_id', $lesson_id )->first();

        $status = 'approved';

        if ( $user->is_tutor() ) {
            $lesson_request->tutor_status = $status;
        } else if ( $user->is_tutee() ) {
            $lesson_request->tutee_status = $status;
        } else if ( $user->is_admin() ) {
            $lesson_request->admin_status = $status;
        }

        $lesson_request->save();

        // This user's accepted the lesson
        return true;
    }

    public static function statusRejectLesson( $user, $lesson_id )
    {
        $lesson_request = LessonRequest::where( 'lesson_id', $lesson_id )->first();

        $status = 'declined';

        if ( $user->is_tutor() ) {
            $lesson_request->tutor_status = $status;
        } else if ( $user->is_tutee() ) {
            $lesson_request->tutee_status = $status;
        } else if ( $user->is_admin() ) {
            $lesson_request->admin_status = $status;
        }
        //clock( $lesson_request );

        $lesson_request->save();

        // This user has rejected the lesson
        return false;
    }

    public static function status_key( $status )
    {
        $status_key = 1;
        if ( 'booked' === $status ) {
            $status_key = 2;
        } else if ( 'no-payment' === $status ) {
            $status_key = 3;
        } else if ( 'payment-pending' === $status ) {
            $status_key = 4;
        } else if ( 'payment-received' === $status ) {
            $status_key = 5;
        }

        return $status_key;
    }
}
