<?php

namespace App\Livewire\Dashboard;

use App\Helpers\Misc;
use App\Livewire\Dashboard\Sidebar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules as Rules;
use Illuminate\Validation\ValidationException;
use Livewire\Component;

class Password extends Component
{
    public $firstName, $lastName, $phone, $email, $address, $city, $province, $postalCode, $currentPassword, $newPassword, $confirmPassword, $showPassword = false;

    public function clearSuccessMessage()
    {
        Sidebar::setSection( 'password' );
        session()->forget( 'dashboard-message-password' );
    }

    public function password()
    {
        Sidebar::setSection( 'password' );
        $user = Auth::user();
        clock( $this->currentPassword );
        clock( $this->newPassword );

        $validator = Validator::make( [
            'currentPassword' => $this->currentPassword,
            'newPassword'     => $this->newPassword,
            'confirmPassword' => $this->confirmPassword,
        ], [
            'currentPassword' => 'required',
            'newPassword'     => 'required',
            'confirmPassword' => 'required|same:newPassword',
        ], [
            'required' => 'The :attribute field is required.',
            'same'     => [
                'confirmPassword' => 'The new password and the confirmation fields do not match.',
            ],
        ] );

        clock( $validator );

        if ( $validator->fails() ) {
            session()->flash( 'dashboard-alert-password', 'Please fix above errors and try again' );
            throw new ValidationException( $validator );
        }

        $this->resetErrorBag();

        $validated = $validator->validate();

        $user->password = \Hash::make( $this->newPassword );

        $saved = $user->save();

        if (  ! $saved ) {
            // TODO send automatic email to admin with errors
            return session()->flash( 'dashboard-alert-password', 'Password update unsuccessful, please get in touch for help' );
        }

        return session()->flash( 'dashboard-message-password', 'Password updated' );
    }

    public function render()
    {
        return view( 'livewire.dashboard.password' );
    }
}
