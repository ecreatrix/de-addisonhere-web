<?php

namespace App\Livewire\Dashboard;

use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use Livewire\Component;

//use Wa\LivewireCalendar\LivewireCalendarHelpers;

class CalendarHelpers extends Component
{
    public static function day_class( $day )
    {
        $classes      = [];
        $calendar_day = $day->copy()->toDateString();

        if ( self::day_current( $calendar_day ) ) {
            $classes[] = 'current-day';
        } else if ( self::day_future( $calendar_day ) ) {
            $classes[] = 'upcoming-day';

            if ( self::day_next( $calendar_day ) ) {
                $classes[] = 'tomorrow';
            }
        } else if ( self::day_past( $calendar_day ) ) {
            $classes[] = 'past-day';

            if ( self::day_last( $calendar_day ) ) {
                $classes[] = 'yesterday';
            }
        }

        return implode( ' ', $classes );
    }

    public static function day_current( $calendar_day )
    {
        return Carbon::now()->toDateString() === $calendar_day;
    }

    public static function day_future( $calendar_day )
    {
        return $calendar_day > Carbon::now()->toDateString();
    }

    public static function day_last( $calendar_day )
    {
        return Carbon::now()->subDay()->toDateString() == $calendar_day;
    }

    public static function day_next( $calendar_day )
    {
        return Carbon::now()->addDay()->toDateString() == $calendar_day;
    }

    public static function day_past( $calendar_day )
    {
        return $calendar_day < Carbon::now()->toDateString();
    }

    public static function event_class( $event )
    {
        $classes = ['event', 'event-' . $event['id'], 'bg-white', 'rounded', 'border', 'py-2', 'px-3', 'cursor-pointer'];

        return implode( ' ', $classes );
    }

    public static function month_class( $month )
    {
        $calendar_month      = $month->get( 1 )->first()->copy();
        $calendar_month->day = Carbon::now()->format( 'd' );
        $calendar_month      = $calendar_month->toDateString();

        $classes = ['flex month'];
        if ( self::month_current( $calendar_month ) ) {
            $classes[] = 'current-month';
        } else if ( self::month_future( $calendar_month ) ) {
            $classes[] = 'upcoming-month';

            if ( self::month_next( $calendar_month ) ) {
                $classes[] = 'next-month';
            }
        } else if ( self::month_past( $calendar_month ) ) {
            $classes[] = 'past-month';

            if ( self::month_last( $calendar_month ) ) {
                $classes[] = 'last-month';
            }
        }

        return implode( ' ', $classes );
    }

    public static function month_current( $calendar_month )
    {
        return Carbon::now()->toDateString() == $calendar_month;
    }

    public static function month_future( $calendar_month )
    {
        return $calendar_month > Carbon::now()->toDateString();
    }

    public static function month_last( $calendar_month )
    {
        return Carbon::now()->subMonth()->toDateString() == $calendar_month;
    }

    public static function month_next( $calendar_month )
    {
        return Carbon::now()->addMonth()->toDateString() == $calendar_month;
    }

    public static function month_past( $calendar_month )
    {
        return $calendar_month < Carbon::now()->toDateString();
    }
}
