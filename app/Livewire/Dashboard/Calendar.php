<?php

//declare ( strict_types = 1 );

namespace App\Livewire\Dashboard;

use BombenProdukt\LivewireCalendar\Data\Event;
use BombenProdukt\LivewireCalendar\Http\Livewire\AbstractCalendar;
use Illuminate\Support\Collection;

final class Calendar
{
    public function events(): Collection
    {
        return new Collection( [] );
    }
}
