<?php

namespace App\Livewire\Auth;

use App\Mail\SendForgotPassword;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Livewire\Component;

class ForgotPassword extends Component
{
    public $email, $showPassword = false;

    protected $messages = [
        'email.required' => 'Please enter your email.',
        'email.email'    => 'Invalid email.',
    ];

    public function render()
    {
        return view( 'livewire.auth.forgot-password' );
    }

    public function store( Request $request )
    {
        $credentials = $this->validate( [
            'email' => ['required', 'email'],
        ] );

        $user = User::where( 'email', $this->email )->first();
        //session()->flush();
        if ( $user ) {
            $name = $user->firstName . ' ' . $user->lastName;
            $request->session()->regenerate();
            $token = Password::createToken( $user );
            $link  = url( 'reset-password/' . $token );

            //clock( $link );

            $send = Mail::to( env( 'MAIL_FROM_ADDRESS', 'hello@addisonhere.com' ) )->send( new SendForgotPassword( $name, $link ) );

            session()->flash( 'message', 'We have sent a password reset link to your email address, if you need further help, feel free to <a class="text-red" href="/contact-us">get in touch</a>.' );

        }

        session()->flash( 'alert', 'This email does not match our records. Would you like to <a class="text-red" href="/sign-up">Sign Up</a> instead?' );
    }
}
