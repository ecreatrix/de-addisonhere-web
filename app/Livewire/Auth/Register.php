<?php

namespace App\Livewire\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\ValidationException;
use Livewire\Component;

class Register extends Component
{
    public $category, $email, $password, $password_confirmation, $firstName, $lastName, $phone, $address, $city, $province, $postalCode, $username, $terms, $showPassword = false;

    public function register()
    {
        $this->registerForm =  ! $this->registerForm;
    }

    public function render()
    {
        //clock( $this );
        clock( $this->category );

        return view( 'livewire.auth.register' );
        //return view( 'auth.sign-up', ['message' => $this->message] );
    }

    public function store()
    {
        $user = Auth::user();

        $validator = Validator::make( [
            'category'              => $this->category,

            'firstName'             => $this->firstName,
            'lastName'              => $this->lastName,

            'phone'                 => $this->phone,
            'email'                 => $this->email,

            'address'               => $this->address,
            'city'                  => $this->city,
            'province'              => $this->province,
            'postalCode'            => $this->postalCode,

            'username'              => $this->username,
            //'password'              => Hash::make( $this->password ),
            'password'              => $this->password,
            //'password_confirmation' => Hash::make( $this->password_confirmation ),
            'password_confirmation' => $this->password_confirmation,
        ], [
            'category'   => ['required'],

            'firstName'  => ['required'],
            'lastName'   => ['required'],

            'email'      => ['required', 'unique:users,email'],

            'phone'      => ['required', 'digits:10'],

            'username'   => ['required', 'unique:users,username'],

            'password'   => ['required', 'confirmed', Password::min( 8 )],
            //'password_confirmation' => ['required'],

            'terms'      => ['required'],

            'address'    => ['required'],
            'city'       => ['required'],
            'province'   => ['required', 'not_in:false'],
            'postalCode' => ['required'],
        ], [
            'category.required'   => 'Please select a category.',

            'firstName.required'  => 'Please enter your first name.',
            'lastName.required'   => 'Please enter your last name.',

            'email.required'      => 'Please enter your email.',
            'email.unique'        => 'This email is in use.',

            'phone.required'      => 'Please enter your phone number.',
            'phone.digits'        => 'Please enter a valid phone number with area code.',

            'username.required'   => 'Please enter a username.',
            'username.unique'     => 'This username is in use.',

            'password.required'   => 'Please enter a password.',
            'password.confirmed'  => 'Your password and confirmation do not match.',
            //'password_confirmation.required' => 'Please confirm your password.',

            'terms.required'      => 'Please confirm that you agree with our terms and conditions.',

            'address.required'    => 'Please enter your street address.',
            'city.required'       => 'Please enter your city/town.',
            'province.required'   => 'Please enter your province.',
            'province.not_in'     => 'Please enter your province.',
            'postalCode.required' => 'Please enter your postal code.',
        ] );

        clock( $validator );
        clock( $validator->fails() );

        if ( $validator->fails() ) {
            session()->flash( 'alert', 'Please fix above errors and try again' );
            throw new ValidationException( $validator );
        }

        //$user                     = \App\Models\User::create( $user_info );
        //$user->set_calendar_start = 'Mon';

        //clock( $user );

        if (  ! $user ) {
            session()->flash( 'message', 'Registration unsuccessful, please get in touch for help.' );
        }

        if ( false && 'tutor' === $data['category'] ) {
            //$submodel = \App\Models\Tutor::create( ['user_id' => $user->id] );
        } else {
            //$submodel = \App\Models\Tutee::create( ['user_id' => $user->id] );
        }
        //clock( $submodel );

        session()->flash( 'message', 'Registration successful.' );
    }

    public function store2()
    {
        $data = $this->validate( [
            'category'        => 'required|string',

            'firstName'       => 'required|string',
            'lastName'        => 'required|string',

            'phone'           => 'required|digits_between:10,11',
            'email'           => 'required|email|unique:users',

            'address'         => ['required'],
            'city'            => ['required'],
            'province'        => ['required'],
            'postalCode'      => ['required'],

            'terms'           => ['required'],

            'username'        => 'required|unique:users',
            'password'        => ['required', Rules\Password::defaults()],
            'confirmPassword' => 'required|same:password',
        ] );

        $user_info = [
            'firstName'  => $data['firstName'],
            'lastName'   => $data['lastName'],

            'phone'      => $data['phone'],
            'email'      => $data['email'],

            'address'    => $data['address'],
            'city'       => $data['city'],
            'province'   => $data['province'],
            'postalCode' => $data['postalCode'],

            'username'   => $data['username'],
            'password'   => Hash::make( $data['password'] ),
        ];

        clock( $data );

        $user                     = \App\Models\User::create( $user_info );
        $user->set_calendar_start = 'Mon';

        //clock( $user );

        if ( $user ) {
            if ( 'tutor' === $data['category'] ) {
                $submodel = \App\Models\Tutor::create( ['user_id' => $user->id] );
            } else {
                $submodel = \App\Models\Tutee::create( ['user_id' => $user->id] );
            }
            //clock( $submodel );

            session()->flash( 'message', 'Registration successful.' );
        } else {
            session()->flash( 'message', 'Registration unsuccessful, please get in touch for help.' );
        }
    }

    private function resetInputFields()
    {
        $this->name     = '';
        $this->email    = '';
        $this->password = '';
    }
}
