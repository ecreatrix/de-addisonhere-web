<?php

namespace App\Livewire\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    public $password, $username, $remember, $referrer, $showPassword = false;

    protected $messages = [
        'username.required' => 'Please enter your username.',
        'password.required' => 'Please enter your password.',
    ];

    public function mount( Request $request )
    {
        //$this->referrer = URL::previous();
    }

    public function render()
    {
        if (  ! Auth::check() ) {
            return view( 'livewire.auth.login' );
        } else {
            return view( 'livewire.dashboard.overview' );
        }
    }

    public function store( Request $request )
    {
        $credentials = $this->validate( [
            'username' => 'required',
            'password' => 'required',
        ] );

        $attempt = Auth::attempt( $credentials, $this->remember );
        //clock( $attempt );

        if ( $attempt ) {
            $request->session()->regenerate();

            return redirect()->intended( 'dashboard' );
        }

        session()->flash( 'alert', 'Login unsuccessful, these credentials do not match our records.' );
        $this->resetInputFields();
    }

    private function resetInputFields()
    {
        $this->username = '';
        $this->password = '';
    }
}
