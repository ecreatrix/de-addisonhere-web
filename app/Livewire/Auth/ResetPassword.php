<?php

namespace App\Livewire\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\Rules;
use Livewire\Component;

class ResetPassword extends Component
{
    public $token, $email, $password, $confirmPassword, $showPassword = false;

    protected $messages = [
        'email.required'           => 'Please enter your email.',

        'password.required'        => 'Please enter a password.',
        'confirmPassword.required' => 'Please confirm your password.',
        'confirmPassword.same'     => 'Your password and the password confirmation fields must match.',
    ];

    public function mount( Request $request )
    {
        $this->token = $request->route( 'token' );
    }

    public function render()
    {
        return view( 'livewire.auth.reset-password' );
    }

    public function store()
    {
        $credentials = $this->validate( [
            'token'           => ['required'],
            'email'           => ['required', 'email'],
            'password'        => ['required', Rules\Password::defaults()],
            'confirmPassword' => 'required|same:password',
        ] );

        $user = User::where( 'email', $credentials['email'] )->first();

        $status = Password::tokenExists( $user, $credentials['token'] );

        if ( $status ) {
            $user->password = Hash::make( $credentials['password'] );
            //$user->password = $credentials['password'];
            $user->save();

            return redirect()->intended( 'dashboard' );
        }

        session()->flash( 'alert', 'Reset unsuccessful, please contact us for further support.' );
    }
}
