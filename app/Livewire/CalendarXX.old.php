<?php

namespace App\Livewire;

use App\Models\Lesson;
use App\Models\LessonRequest;
use App\Models\Tutee;
use App\Models\Tutor;
use App\Models\TutorTutee;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Wa\LivewireCalendar\LivewireCalendar;

class CalendarOld extends LivewireCalendar
{
    public $id, $toasts = [], $tutors = [], $tutees = [], $user, $calendarTuteeSelect, $calendarTutorSelect, $calendarEventID = null, $calendarEventStatus, $calendarEventNew = true,
    $calendarEventComposeSubject, $calendarEventComposeCost, $calendarEventComposeBody,
    $calendarEventApproved = false, $calendarEventComposeDatetime;

    public function createLesson()
    {
        $validator = Validator::make( [
            'calendarTuteeSelect'          => $this->calendarTuteeSelect,
            'calendarTutorSelect'          => $this->calendarTutorSelect,
            'calendarEventComposeCost'     => $this->calendarEventComposeCost,
            'calendarEventComposeBody'     => $this->calendarEventComposeBody,
            'calendarEventComposeDatetime' => $this->calendarEventComposeDatetime,
        ], [
            'calendarTuteeSelect'          => 'required',
            'calendarTutorSelect'          => 'required',
            'calendarEventComposeCost'     => 'required',
            'calendarEventComposeBody'     => 'required|string',
            'calendarEventComposeDatetime' => 'required',
        ], [
            'calendarTutorSelect.required'          => 'Please choose a tutor',
            'calendarTuteeSelect.required'          => 'Please choose a tutee',
            'calendarEventComposeCost.required'     => 'Please enter the lesson cost',
            'calendarEventComposeDatetime.required' => 'Please choose a date and time',
            'calendarEventComposeBody.required'     => 'Please enter your message',
        ] );

        if ( $validator->fails() ) {
            session()->flash( 'dashboard-alert-calendar-create', 'Please fix above errors and try again' );
            throw new ValidationException( $validator );
        }
        $this->resetErrorBag();

        $validator->validate();

        if ( $this->calendarEventID ) {
            $lesson = Lesson::find( $this->calendarEventID );

            $lesson->update( $this->form_params() );
            $lesson->save();
        } else {
            $lesson = Lesson::create( $this->form_params() );
        }

        if (  ! $lesson ) {
            // TODO send automatic email to admin with errors
            return session()->flash( 'dashboard-alert-calendar-create', 'Lesson was not created, please get in touch for help' );
        }

        $params = [
            'user_id'      => $this->user->id,
            'lesson_id'    => $lesson->id,
            'tutor_status' => 'declined',
            'tutee_status' => 'declined',
            'admin_status' => 'declined',
        ];

        $request_approved_status = LessonsHelpers::request_approved_status();
        if ( $this->user->is_tutor() ) {
            $params['tutor_status'] = $request_approved_status;
        } else if ( $this->user->is_tutee() ) {
            $params['tutee_status'] = $request_approved_status;
        } else if ( $this->user->is_admin() ) {
            $params['admin_status'] = $request_approved_status;
        }

        $lesson_request = LessonRequest::create( $params );

        if (  ! $lesson_request ) {
            // TODO send automatic email to admin with errors
            return session()->flash( 'dashboard-alert-calendar-create', 'Lesson was not created, please get in touch for help' );
        }

        $this->formClear();
        if ( $this->calendarEventNew ) {
            session()->flash( 'dashboard-message-calendar-create', 'Your event was created' );
        } else {
            session()->flash( 'dashboard-message-calendar-create', 'Your event was updated' );
        }

        $this->calendarEventNew = true;
        return;
    }

    public function eventUpdatable( $lesson, $dateString, $dispatch = true )
    {
        $now               = Carbon::now()->toDateString();
        $original_timeslot = Carbon::parse( $lesson->timeslot )->toDateString();
        $new_timeslot      = Carbon::create( $dateString )->toDateString();

        if ( $original_timeslot < $now ) {
            // Event happened in the past, can't be moved
            $dispatch && $this->dispatchBrowserEvent( 'toast-added', ['eventId' => $lesson->id, 'toastId' => '#past-event'] );
            return false;
        } else if ( $new_timeslot < $now ) {
            // Future event happened trying to be moved to past, can't be moved
            $dispatch && $this->dispatchBrowserEvent( 'toast-added', ['eventId' => $lesson->id, 'toastId' => '#move-past'] );
            return false;
        }

        return true;
    }

    public function events(): Collection
    {
        $lessons_array = $this->user->lessons();

        $calendar_events = [];

        foreach ( $lessons_array as $key => $lesson ) {
            $lesson_user = $lesson->user();
            $status      = $lesson->is_approved() ? $lesson->status : 'request pending';
            $lesson_time = Carbon::create( $lesson->timeslot )->format( 'Y-m-d H:i' );

            $calendar_events[] = [
                'id'          => $lesson->id,
                'title'       => $lesson_user->full_name(),
                'status'      => Str::replace( '-', ' ', Str::title( $status ) ),
                'status_slug' => Str::slug( 'status ' . $status ),
                'description' => $lesson->description,
                'date'        => Carbon::create( $lesson->timeslot ),
                'updatable'   => $this->eventUpdatable( $lesson, $lesson_time, false ),
            ];
        }

        return collect( $calendar_events );
    }

    public function formClear()
    {
        $this->calendarTuteeSelect          = null;
        $this->calendarTutorSelect          = null;
        $this->calendarEventComposeSubject  = null;
        $this->calendarEventComposeCost     = null;
        $this->calendarEventComposeBody     = null;
        $this->calendarEventComposeDatetime = null;
        $this->calendarEventApproved        = false;
        $this->calendarEventID              = null;

        if ( $this->user->is_tutor() ) {
            $this->calendarTutorSelect = $this->user->id;
        } else if ( $this->user->is_tutee() ) {
            $this->calendarTuteeSelect = $this->user->id;
        }

        $this->calendarEventNew = true;
    }

    public function form_params()
    {
        return [
            'tutor_user_id' => $this->calendarTutorSelect,
            'tutee_user_id' => $this->calendarTuteeSelect,
            'cost'          => $this->calendarEventComposeCost,
            'status'        => 'booked',
            'description'   => $this->calendarEventComposeBody,
            'timeslot'      => date( 'Y-m-d H:i:s', strtotime( $this->calendarEventComposeDatetime ) ),
        ];
    }

    // TODO: only show approved events
    public function mount( $initialYear = null,
        $initialMonth = null,
        $weekStartsAt = null,
        $calendarView = 'components.calendar.calendar',
        $dayView = 'components.calendar.day',
        $eventView = 'components.calendar.day-of-week',
        $dayOfWeekView = 'components.calendar.event',
        $dragAndDropClasses = null,
        $beforeCalendarView = null,
        $afterCalendarView = null,
        $pollMillis = null,
        $pollAction = null,
        $dragAndDropEnabled = true,
        $dayClickEnabled = true,
        $eventClickEnabled = true,
        $extras = [] ) {
        $this->user = Auth::user();
        $this->setCalendarStart( $weekStartsAt );

        $initialYear  = $initialYear ?? Carbon::today()->year;
        $initialMonth = $initialMonth ?? Carbon::today()->month;

        $this->startsAt = Carbon::createFromDate( $initialYear, $initialMonth, 1 )->startOfDay();
        $this->endsAt   = $this->startsAt->clone()->endOfMonth()->startOfDay();

        $this->gridStartsAt = $this->startsAt->clone()->startOfWeek( $this->weekStartsAt );
        $this->gridEndsAt   = $this->endsAt->clone()->endOfWeek( $this->weekEndsAt );

        $this->calculateGridStartsEnds();

        $this->setupViews( $calendarView, $dayView, $eventView, $dayOfWeekView, $beforeCalendarView, $afterCalendarView );

        $this->setupPoll( $pollMillis, $pollAction );

        $this->dragAndDropEnabled = $dragAndDropEnabled;
        $this->dragAndDropClasses = $dragAndDropClasses ?? 'border border-green border-1';

        $this->dayClickEnabled   = $dayClickEnabled;
        $this->eventClickEnabled = $eventClickEnabled;

        $this->month = Carbon::now()->format( 'F' );
        $this->afterMount( $extras );

        $this->toasts[] = [
            'id' => 'past-event', 'title' => 'Error', 'small' => '', 'body' => 'Past events can\'t be changed.',
        ];

        $this->toasts[] = ['id' => 'move-past', 'title' => 'Error', 'small' => '', 'body' => 'Events can\'t be moved into the past.',
        ];

        $this->formClear();

        $tutees = Tutee::all();
        $tutors = Tutor::all();

        if ( $this->user->is_tutor() ) {
            $tutees = $this->user->assigned_tutees()->get();
            $tutors = [];
        } else if ( $this->user->is_tutee() ) {
            $tutees = [];
            $tutors = $this->user->assigned_tutors()->get();
        }

        foreach ( $tutees as $tutee ) {
            $label = $tutee->full_name();

            $this->tutees[$tutee->id] = $label;
        }

        foreach ( $tutors as $tutor ) {
            $label = $tutor->full_name();

            $this->tutors[$tutor->id] = $label;
        }
    }

    public function onEventClick( $lessonId )
    {
        $lesson      = Lesson::find( $lessonId );
        $lesson_time = Carbon::create( $lesson->timeslot )->format( 'Y-m-d H:i' );

        if (  ! $this->eventUpdatable( $lesson, $lesson_time ) ) {
            return;
        }

        $this->calendarEventNew = false;

        $this->calendarEventID              = $lessonId;
        $this->calendarEventStatus          = $lesson->status;
        $this->calendarTuteeSelect          = $lesson->tutee->id;
        $this->calendarTutorSelect          = $lesson->tutor->id;
        $this->calendarEventComposeSubject  = $lesson->subject;
        $this->calendarEventComposeCost     = $lesson->cost;
        $this->calendarEventComposeBody     = $lesson->description;
        $this->calendarEventComposeDatetime = $lesson_time;

        $request = $lesson->request;

        if ( $this->user->is_tutor() ) {
            $this->calendarEventApproved = 'approved' === $request->tutor_status;
        } else if ( $this->user->is_tutee() ) {
            $this->calendarEventApproved = 'approved' === $request->tutee_status;
        } else if ( $this->user->is_admin() ) {
            $this->calendarEventApproved = 'approved' === $request->admin_status;
        }

        //return session()->flash( 'dashboard-updated-calendar', true );
    }

    public function onEventDropped( $eventId, $dateString, $year, $month, $day )
    {
        $lesson_item = Lesson::find( $eventId );

        if ( $this->eventUpdatable( $lesson_item, $dateString ) ) {
            $lesson_item->timeslot = Carbon::create( $dateString )->format( 'Y-m-d H:i' );
            $lesson_item->save();
        }
    }

    public function render()
    {
        $events = $this->events();
        /*return view( 'livewire.dashboard.calendar' )
    ->with( [
    'componentId'     => $this->id,
    'monthGrid'       => $this->monthGrid(),
    'events'          => $events,
    'getEventsForDay' => function ( $day ) use ( $events ) {
    return $this->getEventsForDay( $day, $events );
    },
    // ] );*/
    }

    public function setCalendarStart( $weekStartsAt = null )
    {
        $start_of_week     = $this->user->weekStart;
        $start_of_week_num = 0;

        if ( 'Monday' === $start_of_week ) {
            $start_of_week = Carbon::MONDAY;
        } else if ( 'Tuesday' === $start_of_week ) {
            $start_of_week = Carbon::TUESDAY;
        } else if ( 'Wednesday' === $start_of_week ) {
            $start_of_week = Carbon::WEDNESDAY;
        } else if ( 'Thursday' === $start_of_week ) {
            $start_of_week = Carbon::THURSDAY;
        } else if ( 'Friday' === $start_of_week ) {
            $start_of_week = Carbon::FRIDAY;
        } else if ( 'Saturday' === $start_of_week ) {
            $start_of_week = Carbon::SATURDAY;
        } else {
            $start_of_week = Carbon::SUNDAY;
        }
        $this->weekStartsAt = $weekStartsAt ?? $start_of_week;

        $this->weekEndsAt = Carbon::SUNDAY == $this->weekStartsAt ? Carbon::SATURDAY
        : collect( [0, 1, 2, 3, 4, 5, 6] )->get( $this->weekStartsAt - 1 );
    }

    public function setupViews( $calendarView = null,
        $dayView = null,
        $eventView = null,
        $dayOfWeekView = null,
        $beforeCalendarView = null,
        $afterCalendarView = null ) {
        $this->calendarView  = $calendarView ?? 'livewire-calendar::calendar';
        $this->dayView       = $dayView ?? 'livewire-calendar::day';
        $this->eventView     = $eventView ?? 'components.admin.calendar-event';
        $this->dayOfWeekView = $dayOfWeekView ?? 'livewire-calendar::day-of-week';

        $this->beforeCalendarView = $beforeCalendarView ?? null;
        $this->afterCalendarView  = $afterCalendarView ?? null;
    }

    public function statusAcceptLesson()
    {
        $this->calendarEventApproved = LessonsHelpers::statusAcceptLesson( $this->user, $this->calendarEventID );
    }

    public function statusRejectLesson()
    {
        $this->calendarEventApproved = LessonsHelpers::statusRejectLesson( $this->user, $this->calendarEventID );
    }
}
