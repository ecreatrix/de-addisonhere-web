import { Select } from "tw-elements";

export function selectable() {
	const singleSelect = document.getElementsByClassName("singleSelection");

	if (singleSelect.length > 0) {
		for (var i = 0; i < singleSelect.length; i++) {
			const singleSelectInstance = Select.getInstance(singleSelect.item(i));
			const value = singleSelect.item(i).dataset.selected

			if(value && !!value) {
			//console.log(singleSelect.item(i))
			//console.log(singleSelectInstance)
				console.log(value)
				singleSelectInstance.setValue(value);
			}
		}
	}
}