var themeToggleDarkIcon = document.getElementById('theme-toggle-dark-icon');
var themeToggleLightIcon = document.getElementById('theme-toggle-light-icon');

// On page load or when changing themes, best to add inline in `head` to avoid FOUC
if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
	themeToggleLightIcon.classList.add('hidden');
	themeToggleDarkIcon.classList.remove('hidden');

	document.documentElement.classList.add('dark');
	document.documentElement.classList.remove('light');
} else {
	themeToggleDarkIcon.classList.remove('hidden');
	themeToggleLightIcon.classList.add('hidden');

	document.documentElement.classList.add('light');
	document.documentElement.classList.remove('dark');
};

document.getElementById('theme-toggle').addEventListener('click', function() {
	// toggle icons inside button
	themeToggleDarkIcon.classList.toggle('hidden');
	themeToggleLightIcon.classList.toggle('hidden');

	const theme = event.target.dataset.theme;

	if (theme === "system") {
		// if set via local storage previously
		localStorage.removeItem("theme");
		setSystemTheme();
	} else if (theme === "dark" || document.documentElement.classList.contains('dark')) {
		document.documentElement.classList.add('light');
		document.documentElement.classList.remove("dark");

		localStorage.theme = "light";

		localStorage.setItem('color-theme', 'light');
	} else {
		document.documentElement.classList.add("dark");
		document.documentElement.classList.remove('light');

		localStorage.theme = "dark";

		localStorage.setItem('color-theme', 'dark');
	}
	
	console.log(localStorage)
	console.log(localStorage.theme)
});