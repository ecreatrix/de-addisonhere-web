import 'tw-elements/dist/js/tw-elements.es.min.js'
import { selectable, } from './modules/tw-elements.js'

// Initialization for ES Users
import {
	Tab,
	Dropdown,
	Select,
  	Collapse,
  	initTE,
	Datepicker,
	Input,
	Sidenav,
	Ripple,
} from "tw-elements";

document.addEventListener('livewire:init', () => {
    // Runs after Livewire is loaded but before it's initialized
    initTE({ Tab, Dropdown,Sidenav, Datepicker, Input, Select, Collapse, Ripple, });
	
	selectable();

	window.onLivewireCalendarEventDragStart = function(event, eventId) {
		event.dataTransfer.setData('id', eventId);
	};

	window.onLivewireCalendarEventDragEnter = function(event, componentId, dateString, dragAndDropClasses) {
		event.stopPropagation();
		event.preventDefault();

		const element = document.getElementById(`${componentId}-${dateString}`);
		element.className = `${element.className} ${dragAndDropClasses}`;
	};

	window.onLivewireCalendarEventDragLeave = function(event, componentId, dateString, dragAndDropClasses) {
		event.stopPropagation();
		event.preventDefault();

		const element = document.getElementById(`${componentId}-${dateString}`);
		element.className = element.className.replace(dragAndDropClasses, '');
	};

	window.onLivewireCalendarEventDragOver = function(event) {
		event.stopPropagation();
		event.preventDefault();
	};

	window.onLivewireCalendarEventDrop = function(event, componentId, dateString, dragAndDropClasses) {
		event.stopPropagation();
		event.preventDefault();

		const element = document.getElementById(`${componentId}-${dateString}`);
		element.className = element.className.replace(dragAndDropClasses, '');

		window.Livewire
			.find(componentId)
			.call('onEventDropped', event.dataTransfer.getData('id'), dateString);
	};
})


document.addEventListener('livewire:update', function () {
	console.log('update ')
	initTE({ Tab, Sidenav, Datepicker, Input, Select, Collapse });
	
	selectable();
});
