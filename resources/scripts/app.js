//import './bootstrap';
import 'tw-elements/dist/js/tw-elements.es.min.js'
//import './modules/dark.js'

//import { onMounted } from 'vue'
//import { initFlowbite } from 'flowbite'

// initialize components based on data attribute selectors
//onMounted(() => {
//    initFlowbite();
//})

//import Alpine from 'alpinejs';

//window.Alpine = Alpine;

//Alpine.start();

// Initialization for ES Users
import {
	Input,
	//  Tab,
	Collapse,
	initTE,
	//  Ripple,
	Dropdown,
	Select,
	//  Sidenav,
} from "tw-elements";

//initTE({ Ripple, Tab, Collapse, Sidenav, });
initTE({ Select, Input, Collapse, Dropdown });

import './modules/tw-elements.js'

var navbarToggler = document.getElementById('navbarToggler');
navbarToggler.addEventListener('click', function() {
	var menu_open = ! navbarToggler.hasAttribute('data-te-collapse-collapsed')
	//console.log(menu_open)

	if( menu_open ) {
		document.documentElement.classList.add('menu-open');
	} else {
		document.documentElement.classList.remove('menu-open');
	}
});

import.meta.glob([
	'../images/**',
	'../fonts/**',
]);
