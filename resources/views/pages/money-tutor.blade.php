@extends('layouts.app')

@section('title', 'Tutor Assessment')
@section('entry-block', 'Tutor Assessment')
@section('page-class', 'bg-gray-100')

@section('content')
@if ( !Auth::check() )
    <div class="text-flex-block"><div class="container">
        <div class="flex">
            <div class="w-1/2">
                <p>Thank you for your interest in becoming an AddisonHere Technology Tutor. Once you have a Tutor account, you will be able to fill out an assessment so we can get to know you better and gauge your technical skillset.</p>

                <div class="btn-group w-full">
                    <a class="btn-outline-pink" href="{{route('register')}}">Sign Up</a>
                    <a class="btn-outline-blue" href="{{route('admin/login')}}">Login</a>
                </div>
            </div>
            <div class="w-1/3 offset-2">
                <h2 class="text-pink">You don’t need a<br/>specialized technical<br/>degree, just a familiarity<br/>with devices like<br/>smartphones, tablets,<br/>and laptops.</h2>
            </div>
        </div>
    </div></div>
@else
    <div class="jumbotron form-block assessment-form"><div class="container">
        <p>Thank you for your interest in becoming an AddisonHere Technology Tutor. Please fill out this assessment so we can get to know you better and gauge your technical skillset. Answer the questions as accurately as possible. Information retrieved from the assessment is confidential and will be kept in the IdeaCase database in order to effectively match clients with tutors.</p>

        @livewire('tutor-assessment')
    </div></div>
@endif
@stop
