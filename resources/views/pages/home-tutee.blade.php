@extends('layouts.app')

@section('title', 'Tutee Home')

@section('content')
    <div class="bg-blue py-0"><div class="container pb-0">
        <div class="flex block">
            <div class="w-full md:w-1/2 flex flex-col justify-center items-center md:items-start">
                <h1 class="w-full text-white pb-4 title-case text-center md:text-left">Need help with<br>your smartphone<br>or computer?</h1>

                <div class="btn-group w-full mx-auto md:mx-0 justify-center md:justify-start my-5 md:my-0 flex">
                    <a class="btn-outline-white" href="{{route('register')}}">Let's Begin</a>
                </div>
            </div>

            <div class="w-full md:w-1/2 items-center flex pt-4 pt-md-0 right-md">
                <img class="w-full" src="{{Vite::asset('resources/images/content/man-tablet-transparent.png')}}" alt="man with tablet" height="1000" width="1000" />
            </div>
        </div>
    </div></div>

    <div class="py-36"><div class="container">
        <div class="flex justify-between">
            <h2 class="w-full md:w-2/5 text-blue mb-9 md:mb-0">The technology tutors at AddisonHere are here to help with any problems you have on your device or laptop.</h2>

            <div class="w-full md:w-2/5">
                <p>AddisonHere is an online personalized IT service that connects local technology experts with older adults struggling to navigate the digital world. Learning how to use computers, tablets and smartphones without guidance can be overwhelming, but with the help of our tutors you can say toodaloo to all your tech problems. Contact AddisonHere today and receive personalized technology assistance from one of our tutors over the phone or in-person. Your tutor will build a plan to guide you through your technology needs.</p>

                <div class="btn-group w-full mt-4 md:mt-0 flex justify-center md:justify-start">
                    <a class="btn-outline-blue" href="{{route('contact')}}">Contact us</a>
                </div>
            </div>
        </div>
    </div></div>
    </div></div>

    <div class="h-[500px] md:h-[750px] md:bg-fixed" style="background-image:url({{Vite::asset('resources/images/content/couple-laptop-patio.jpg')}});">
    </div>

    <div class="py-36"><div class="container">
        <div class="flex justify-between">
            <h2 class="w-full md:w-2/5 text-pink mb-9 md:mb-0">Get the expert technology help you need and make the most of your devices today.</h2>

            <div class="w-full md:w-2/5">
                <p>Assisting older Canadians to improve their digital literacy skills and increase their user confidence is our goal. Our digital age society is constantly evolving, and it can be challenging to keep up with trends. Get the expert technology help you need and make the most of your devices today.</p>

                <div class="btn-group w-full mt-4 md:mt-0 flex justify-center md:justify-start">
                    <a class="btn-outline-pink" href="{{route('register')}}">Let's begin</a>
                </div>
            </div>
        </div>
    </div></div>
@stop
