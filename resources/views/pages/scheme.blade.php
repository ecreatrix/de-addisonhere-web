@extends('layouts.app')

@section('title', 'Colour Scheme')

@section('content')
        <div class="py-36 bg-neutral-300"><div class="container">
        <div class="flex justify-between">
            <div class="w-full md:w-2/5 ">
                <h1 class="text-white mb-9 md:mb-0">H1</h1>
                <h2 class="text-white mb-9 md:mb-0">H2</h2>
                <h3 class="text-white mb-9 md:mb-0">H3</h3>
                <h4 class="text-white mb-9 md:mb-0">H4</h4>
                <br />
                <h1 class="text-pink mb-9 md:mb-0">H1</h1>
                <h2 class="text-pink mb-9 md:mb-0">H2</h2>
                <h3 class="text-pink mb-9 md:mb-0">H3</h3>
                <h4 class="text-pink mb-9 md:mb-0">H4</h4>
                <br />
                <h1 class="text-green mb-9 md:mb-0">H1</h1>
                <h2 class="text-green mb-9 md:mb-0">H2</h2>
                <h3 class="text-green mb-9 md:mb-0">H3</h3>
                <h4 class="text-green mb-9 md:mb-0">H4</h4>
                <br />
                <h1 class="text-blue mb-9 md:mb-0">H1</h1>
                <h2 class="text-blue mb-9 md:mb-0">H2</h2>
                <h3 class="text-blue mb-9 md:mb-0">H3</h3>
                <h4 class="text-blue mb-9 md:mb-0">H4</h4>
            </div>


            @php
                // Create a new ViewErrorBag instance.
                $viewErrorBag = new Illuminate\Support\ViewErrorBag;

                // Create a new MessageBag instance.
                $messageBag = new Illuminate\Support\MessageBag;
                $messageBag->add('one', 'Error for one');
                $messageBag->add('four', 'Error for four');

                // Add the $messageBag to the $viewErrorBag
                // with some key.
                $viewErrorBag->put('default', $messageBag);

                // Get the number of MessageBag instances.
                $messageBagCount = count($viewErrorBag->getBags());
            @endphp

            <div class="w-full md:w-2/5">
                <p class="mb-5">Text</p>

                <h4 class="text-white">Radio</h4>
                <div class="justify-between mb-3 flex md:-mx-3 has-validation w-full">
                    <x-form.radio name="category" id="category" wire-model="category" key="category" wrapperClass="w-full md:w-45/100 md:mx-3" :options="[
                        'one' => 'One',
                        'two' => 'Two',
                        ]" />
                </div>

                <h4 class="text-white">Radio with error</h4>
                <div class="justify-between mb-6 flex md:-mx-3 has-validation is-invalid w-full">
                    <x-form.radio name="category" :errors="$viewErrorBag" id="category" wire-model="category" key="category" wrapperClass="w-full md:w-45/100 md:mx-3" errorWrapperClass="w-full md:mx-3" :options="[
                        'one' => 'One',
                        'two' => 'Two',
                        ]" />
                </div>

                <h4 class="text-white">Checklist</h4>
                <div class="justify-between mb-3 flex md:-mx-3">
                    <x-form.checklist id="app" wire:change="classChangeApp" wire:model="app" wrapperClass="w-full md:w-1/3 md:mx-3" :options="[
                        'one' => 'One',
                        'two' => 'Two',
                        'three' => 'Three',
                        'four' => 'Four',
                    ]" />
                </div>

                <h4 class="text-white">Checklist with error</h4>
                <div class="justify-between mb-6 flex md:-mx-3">
                    <x-form.checklist id="app" :errors="$viewErrorBag" wire:change="classChangeApp" wire:model="app" wrapperClass="w-full md:w-1/3 md:mx-3" errorWrapperClass="w-full md:mx-3" :options="[
                        'one' => 'One',
                        'two' => 'Two',
                        'three' => 'Three',
                        'four' => 'Four',
                    ]" />
                </div>

                <h4 class="text-white">Input</h4>
                <div class="justify-between mb-3 flex md:-mx-3">
                    <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" id="firstName" label="First Name" wire-model="one" />
                    <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" id="lastName" label="Last Name" wire-model="two" />
                </div>

                <h4 class="text-white">Input with error</h4>
                <div class="justify-between mb-6 flex md:-mx-3">
                    <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$viewErrorBag" id="firstName" label="First Name" wire-model="one" />
                    <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$viewErrorBag" id="lastName" label="Last Name" wire-model="two" />
                    <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$viewErrorBag" id="lastName" label="Last Name" wire-model="four" />
                </div>

                <h4 class="text-white">Select</h4>
                <div class="justify-between mb-3 flex md:-mx-3">
                    <x-form.select wrapperClass="w-full md:w-45/100 md:mx-3" value="one" id="weekStart" :options="[
                        'one' => 'One',
                        'two' => 'Two',
                        'three' => 'Three',
                        'four' => 'Four',
                        ]"
                    />
                </div>

                <h4 class="text-white">Select with error</h4>
                <div class="justify-between mb-6 flex md:-mx-3">
                    <x-form.select wrapperClass="w-full md:w-45/100 md:mx-3" errorWrapperClass="w-full md:mx-3" id="weekStart" :errors="$viewErrorBag" :options="[
                        'one' => 'One',
                        'two' => 'Two',
                        'three' => 'Three',
                        'four' => 'Four',
                        ]"
                    />
                </div>

                <div class="w-full mt-4 md:mt-0 flex justify-center md:justify-start">
                    <div class="btn-group">
                        <a class="btn-white" href="{{route('contact')}}">Contact us</a>
                        <a class="btn-outline-white" href="{{route('contact')}}">Contact us</a>
                    </div>

                    <div class="btn-group">
                        <a class="btn-pink" href="{{route('contact')}}">Contact us</a>
                        <a class="btn-outline-pink" href="{{route('contact')}}">Contact us</a>
                    </div>

                    <div class="btn-group">
                        <a class="btn-green" href="{{route('contact')}}">Contact us</a>
                        <a class="btn-outline-green" href="{{route('contact')}}">Contact us</a>
                    </div>

                    <div class="btn-group">
                        <a class="btn-blue" href="{{route('contact')}}">Contact us</a>
                        <a class="btn-outline-blue" href="{{route('contact')}}">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
    </div></div>
@stop
