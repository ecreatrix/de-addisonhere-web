@extends('layouts.app')

@section('title', 'Dashboard')
@section('admin', true)
@section('page-class', 'dashboard-page')

@section('content')
<div id="admin-page" class="flow bg-gray-200 min-h-screen">
	@livewire('dashboard.sidebar')

	<div id="admin-content" class="p-5 min-h-screen">
		@livewire('dashboard.overview')
		@livewire('dashboard.profile')
		@livewire('dashboard.password')
		@livewire('dashboard.settings')

		@if( Illuminate\Support\Facades\Auth::user()->is_tutor() )
			@livewire('dashboard.tutor-assessment')
		@endif

		@livewire('dashboard.lessons')
		@livewire('dashboard.messages')
	</div>

	</div>
@stop
