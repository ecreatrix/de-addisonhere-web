@extends('layouts.app')

@section('title', 'Tutor Home')

@section('content')
    <div class="bg-pink"><div class="container">
        <div class="flex block">
            <div class="w-full md:w-1/2 flex flex-col justify-center items-center md:items-start">
                <h1 class="w-full text-white pb-4 title-case text-center md:text-left">Getting Paid By<br>Helping Others.<br>Flexible Hours, Too!</h1>
                <div class="btn-group w-full mx-auto md:mx-0 justify-center md:justify-start my-5 md:my-0 flex">
                    <a class="btn-outline-white" href="{{route('register')}}">Let's Begin</a>
                </div>
            </div>
            <div class="w-full md:w-1/2 items-center flex pt-4 pt-md-0 right-md">
                <img class="w-full" src="{{Vite::asset('resources/images/content/woman-laptop-transparent.png')}}" alt="woman with laptop" height="1000" width="1000" />
            </div>
        </div>
    </div></div>

    <div class="py-36"><div class="container">
    	<div class="flex justify-between">
	        <h2 class="w-full md:w-2/5 text-pink mb-9 md:mb-0">You don’t need a specialized technical degree, just a familiarity with devices like smartphones, tablets, and laptops.</h2>

	        <div class="w-full md:w-2/5">
                <p>Introducing AddisonHere, the online platform that matches technology tutors (a.k.a. a millennial or Gen-Zer like yourself) with older adults to provide basic technology support. Best of all, you don’t need a specialized technical degree, just a familiarity with devices like smartphones, tablets, and laptops. Responsibilities of a tutor include: Assisting older adults with common technology issues (i.e., smartphone problems, configuring email/login information, printer set-up, navigating social media). Helping bridge the digital divide between older adults and technology.</p>

                <div class="btn-group w-full mt-4 md:mt-0 flex justify-center md:justify-start">
                    <a class="btn-outline-pink" href="{{route('register')}}">Let's Begin</a>
                </div>
		    </div>
		</div>
    </div></div>

    <div class="h-[500px] md:h-[750px] bg-cover md:bg-fixed bg-center" style="background-image:url({{Vite::asset('resources/images/content/woman-raised-hand.jpg')}});">
    </div>

    <div class="py-36"><div class="container">
        <div class="flex justify-between">
            <h2 class="w-full md:w-2/5 text-pink mb-9 md:mb-0">You create your own hours with no minimum. And sessions with clients are primarily done remotely.</h2>

            <div class="w-full md:w-2/5">
                <p>Why be a tutor at AddisonHere? We offer a flexible schedule for our tutors, meaning you create your own hours with no minimum. Sessions with clients are primarily done remotely. You will discuss your client’s technology needs and build a plan to provide ongoing support. Do you have what it takes to be an AddisonHere tutor?</p>

                <div class="btn-group w-full mt-4 md:mt-0 flex justify-center md:justify-start">
                    <a class="btn-outline-pink" href="{{route('register')}}">Register Now</a>
                </div>
            </div>
        </div>
    </div></div>
@stop
