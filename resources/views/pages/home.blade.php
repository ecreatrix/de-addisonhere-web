@extends('layouts.app')

@section('title', 'Home')

@section('content')

<div class="bg-white"><div class="container circle-media has-media-on-the-right"><div class="flex">
    <div class="w-full mt-12 md:mt-0 md:w-1/2 md:pb-1/2 pb-full h-0 rounded-full bg-cover bg-no-repeat bg-center md:order-2 max-h-40" style="background-image:url({{Vite::asset('resources/images/content/woman-on-phone.jpg')}});">
    </div>

    <div class="w-full md:w-1/2 flex left-center self-center md:order-1"><div class="py-0 pr-0 md:pr-12 md:py-24 lg:pr-32 w-full">
        <h1 class="text-center md:text-left mt-6 md:mt-0">Let’s Work<br>Together</h1>
        <div class="btn-group w-full flex mt-6 md:mt-0 justify-center md:justify-start my-5 md:my-0">
            <a class="btn-outline-pink mr-0 sm:mr-3" href="{{route('tutor')}}">Become a Tutor</a>
            <a class="btn-outline-blue" href="{{route('tutee')}}">Find your Tutor</a>
        </div>
    </div></div>
</div></div></div>

<div class="bg-gray-100"><div class="container circle-media has-media-on-the-right"><div class="flex">
    <div class="w-full mt-12 md:mt-0 md:w-1/2 md:pb-1/2 pb-full h-0 rounded-full bg-cover bg-no-repeat bg-center max-h-40" style="background-image:url({{Vite::asset('resources/images/content/woman-on-couch.jpg')}});background-position:50% 50%">
    </div>

    <div class="w-full md:w-1/2 flex justify-center self-center"><div class="py-0 pl-0 md:pl-12 md:py-24 lg:pl-32 w-full">
        <h2 class="text-center md:text-left text-pink mb-4 mt-6 md:mt-0">Earn Money By Helping Older Adults With Their Technology Problems.</h2>

            <p>Tired of helping your grandma fix her Skype camera? Dad forgot his email password again? These issues are easy to solve but tedious to complete. If only there was a way to get paid for doing it…oh, wait, there is!</p>

            <div class="btn-group w-full flex justify-center md:justify-start mt-4 md:mt-0">
                <a class="btn-outline-pink" href="{{route('tutor')}}">Become a Tutor</a>
            </div>
    </div></div>
</div></div></div>

<div class="bg-white"><div class="container circle-media has-media-on-the-right"><div class="flex">
    <div class="w-full mt-12 md:mt-0 md:w-1/2 md:pb-1/2 pb-full h-0 rounded-full bg-cover bg-no-repeat bg-center md:order-2 max-h-40" style="background-image:url({{Vite::asset('resources/images/content/woman-need-tech-help.jpg')}});background-position:50% 50%">
    </div>

    <div class="w-full md:w-1/2 flex justify-center self-center md:order-1"><div class="py-0 pr-0 md:pr-12 md:py-24 lg:pr-32 w-full">
        <h2 class="text-center md:text-left text-blue mb-4 mt-6 md:mt-0">Frustrated with<br>new technology?</h2>

        <p>Email not working? Forgot your password? Need IT support but don’t want to burden your adult children? If this sounds like you, look no further. The technology tutors at AddisonHere are here to help.</p>

        <div class="btn-group w-full flex justify-center md:justify-start mt-4 md:mt-0">
            <a class="btn-outline-blue" href="{{route('tutor')}}">Find your Tutor</a>
        </div>
    </div></div>
</div></div></div>
@stop
