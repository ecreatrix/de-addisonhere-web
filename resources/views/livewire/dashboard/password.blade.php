<div id="admin-password" class="bg-white p-4 rounded-md hidden opacity-100 transition-opacity duration-150 ease-linear data-[te-tab-active]:block" @if(App\Livewire\Dashboard\Sidebar::checkActiveSection('password')) data-te-tab-active @endif role="tabpanel" aria-labelledby="admin-password-tab">
    <div class="layout-item">
        <h4 class="pb-4">Update Password</h4>

        <form wire:submit.prevent="password" wire:loading.class="loading" enctype="multipart/form-data" wire:click="clearSuccessMessage()">
            @csrf
            <x-form.loading />

            <div class="justify-between items-start mb-6 flex md:-mx-3">
                <x-form.password type="password" wrapperClass="w-full md:mx-3" :errors="$errors" id="currentPassword" label="Current Password" wire-model="currentPassword" value="{{$currentPassword}}" autocomplete="off" showPassword="{{$showPassword}}" />
            </div>

            <div class="justify-between items-start mb-6 flex md:-mx-3">
                <x-form.password wireModelDefer=false type="password" wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="newPassword" label="New Password" wire-model="newPassword" value="{{ $newPassword }}" autocomplete="off" showPassword="{{$showPassword}}" />

                <x-form.password type="password" wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="confirmPassword" label="Confirm New Password" wire-model="confirmPassword" value="{{ $confirmPassword }}" autocomplete="off" showPassword="{{$showPassword}}" />
            </div>

            <div class="d-grid">
                <button type="submit" class="btn-outline-green hover:btn-green">Update password</button>
                <x-form.alert tag="h3" />
            </div>
        </form>

        <x-form.alert tag="h6" alert="dashboard-alert-password" />
        <x-form.message tag="h6" message="dashboard-message-password" />
    </div>
</div>
