<div id="admin-overview" class="bg-white p-4 rounded-md hidden opacity-100 transition-opacity duration-150 ease-linear data-[te-tab-active]:block" @if(App\Livewire\Dashboard\Sidebar::checkActiveSection('overview')) data-te-tab-active @endif role="tabpanel" aria-labelledby="admin-overview-tab" data-te-tab-active>
        <h4 class="pb-4">Home</h4>

        <h6 class="pb-3">Welcome, {{ Illuminate\Support\Facades\Auth::user()->full_name(); }}!</h6>

        [Put in stats/general information]

        <x-form.alert tag="h6" alert="dashboard-alert-overview" />
        <x-form.message tag="h6" message="dashboard-message-overview" />
</div>
