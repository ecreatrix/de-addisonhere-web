<div id="admin-messages" class="bg-white p-4 rounded-md hidden opacity-100 transition-opacity duration-150 ease-linear data-[te-tab-active]:block" @if(App\Livewire\Dashboard\Sidebar::checkActiveSection('messages')) data-te-tab-active @endif role="tabpanel" aria-labelledby="admin-messages-tab">
    <div class="layout-item">
        <div class="flex-1 mb-4 heading flex justify-between items-center">
            <h4 class="">Messages</h4>
            <button class="ms-4 compose-button compose-message-button flex items-center" role="button" aria-expanded="false"
                data-te-collapse-init data-te-ripple-init data-te-ripple-color="light"
                href="#compose-message" aria-controls="compose-message" wire:click="actions_set_parent( null )">
                <i class="fa-solid fa-pencil"></i>
                <span class="ml-2">Send a message</span>
            </button>
        </div>

        <div class="flex justify-between">
            <div class="max-h-screen overflow-y-scroll w-1/3 all-messages overflow-y-auto -ml-4 bg-gray-100/30" role="tablist" data-te-nav-ref>
                @foreach($all_messages as $key => $info)
                    <div class="border-x-0 border-b-2 border-t-0 border-transparent text-neutral-500 cursor-pointer button-{{ $info['parent']['message_id'] }}
                    hover:isolate hover:border-transparent
                    focus:isolate focus:border-transparent
                    data-[te-nav-active]:bg-blue/10 data-[te-nav-active]:text-green" data-te-toggle="pill" role="tab" aria-selected="true" wire:ignore.self
                        aria-controls="tabs-{{ $info['parent']['message_id'] }}" data-te-target="#tabs-{{ $info['parent']['message_id'] }}"
                    >
                        <div class="p-4 hover:bg-neutral-100 {{ App\Livewire\Dashboard\Messages::get_classes( $info['parent']['parent_id'], $info['parent']['message_id'], true ) }}" wire:click="actions_read({{$info['parent']['message_id'] }}, {{ $info['parent']['message_id'] }}, true)">
                            <div class="flex intro justify-between">
                                <div class="sender-name">{{ $info['parent']['sender_name'] }}</div>
                                <div class="date">{{ $info['parent']['date'] }}</div>
                            </div>
                            <div class="subject">{{ \Illuminate\Support\Str::limit( $info['parent']['message']['subject'], 40 ) }}</div>
                            <div class="body text-neutral-300">{{ \Illuminate\Support\Str::limit( $info['parent']['message']['body'], 30 ) }}</div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="max-h-screen overflow-y-scroll w-2/3 detailed relative py-3 px-4">
                <x-loading />

                @foreach($all_messages as $info)
                    <div id="tabs-{{ $info['parent']['message_id'] }}" class="hidden opacity-100 transition-opacity duration-150 ease-linear data-[te-tab-active]:block" role="tabpanel" aria-labelledby="tabs-{{ $info['parent']['message_id'] }}-tab" wire:ignore.self>
                        <x-admin.message-single top_id="{{ $info['parent']['message_id'] }}" :info="$info['parent']"/>

                        @if(array_key_exists('children', $info))
                            @foreach($info['children'] as $child)
                                <x-admin.message-single top_id="{{ $info['parent']['message_id'] }}" :info="$child"/>
                            @endforeach
                        @endif
                    </div>
                @endforeach

                <div id="compose-message-reply" class="!visible hidden compose reply" data-te-collapse-item wire:ignore.self>
                    <h6 class="text-blue">Reply to this message</h6>

                    <x-admin.message-compose id="reply" :actionable="$actionable" />
                </div>

                <div id="compose-message" class="!visible hidden compose reply" data-te-collapse-item wire:ignore.self>
                    <h6 class="text-blue">Compose a message</h6>

                    <x-admin.message-compose id="all" :actionable="$actionable" />
                </div>
            </div>
        </div>
    </div>
</div>
