<div id="admin-lessons" class="bg-white p-4 rounded-md hidden opacity-100 transition-opacity duration-150 ease-linear data-[te-tab-active]:block" @if(App\Livewire\Dashboard\Sidebar::checkActiveSection('lessons')) data-te-tab-active @endif role="tabpanel" aria-labelledby="admin-lessons-tab" wire:ignore.self>
    <div class="layout-item relative" wire:loading.class="updating" wire:ignore.self>
        <div class="mb-6 heading flex justify-between items-center">
            <h4 class="">Lessons</h4>

            <div class="flex justify-start rounded-md transition duration-150 ease-in-out" role="tablist" data-te-nav-ref wire:ignore>
                <button type="button" href="#tabs-display-by-date" class="block border-x-0 border-b-2 border-t-0 border-transparent mr-7 py-3 text-xs font-medium uppercase leading-tight text-neutral-500 hover:isolate hover:border-transparent hover:bg-neutral-100 focus:isolate focus:border-transparent data-[te-nav-active]:border-green data-[te-nav-active]:text-green" data-te-toggle="pill" data-te-target="#tabs-display-by-date" data-te-nav-active role="tab" aria-controls="tabs-display-by-date" aria-selected="true">
                    Lessons by Date
                </button>
                <button type="button" href="#tabs-display-by-status" class="block border-x-0 border-b-2 border-t-0 border-transparent py-3 text-xs font-medium uppercase leading-tight text-neutral-500 hover:isolate hover:border-transparent hover:bg-neutral-100 focus:isolate focus:border-transparent data-[te-nav-active]:border-green data-[te-nav-active]:text-green" data-te-toggle="pill" data-te-target="#tabs-display-by-status" role="tab" aria-controls="tabs-display-by-status" aria-selected="true">
                    Lessons by Status
                </button>
            </div>
        </div>

        <x-form.alert tag="h6" alert="dashboard-alert-lessons" />
        <x-form.message tag="h6" message="dashboard-message-lessons" />

        <div id="tabs-display-by-date" class="hidden opacity-100 transition-opacity duration-150 ease-linear data-[te-tab-active]:block relative" aria-labelledby="tabs-display-by-date-tab" data-te-tab-active role="tabpanel" wire:ignore.self>
            <x-loading />

            @foreach ( $lessons_by_date as $key => $date_block )
                <div class="by-date relative set set-{{ $key }}@if( $this->set_by_date !== $key ){{ ' hidden' }}@endif">
                    <div class="set-info flex justify-between items-center">
                        @if($key > 0)<span class="previous" wire:click="previousSetByDate({{ $key - 1 }})"><svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 320 512"><!--! Font Awesome Pro 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M15 239c-9.4 9.4-9.4 24.6 0 33.9L207 465c9.4 9.4 24.6 9.4 33.9 0s9.4-24.6 0-33.9L65.9 256 241 81c9.4-9.4 9.4-24.6 0-33.9s-24.6-9.4-33.9 0L15 239z"/></svg></span>
                        @else
                        <span class="previous"></span>
                        @endif

                        <h6 class="month d-inline text-blue">{{ $date_block['timeslot'] }}</h6>

                        @if($key < $total_sets_by_date)<span class="next" wire:click="nextSetByDate({{ $key + 1 }})"><svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 320 512"><!--! Font Awesome Pro 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M305 239c9.4 9.4 9.4 24.6 0 33.9L113 465c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l175-175L79 81c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0L305 239z"/></svg></span>
                        @else
                        <span class="next"></span>
                        @endif
                    </div>

                    <div class="accordion" id="accordionLessons-{{ $key }}">
                        @foreach ( $date_block['users'] as $user_id => $user )
                            <div class="user-{{ $user_id }} rounded-none border border-l-0 border-r-0 border-t-0 border-neutral-200 bg-white" wire:ignore.self>
                                <h2 class="mb-0" id="lessons-heading-{{ $user_id }}-{{ $key }}">
                                    <button class="group relative flex w-full items-center rounded-none border-0 bg-white py-4 text-left text-base text-neutral-800 transition [overflow-anchor:none] hover:z-[2] focus:z-[3] focus:outline-none"
                                        type="button" aria-expanded="false" data-te-collapse-init data-te-collapse-collapsed
                                        data-te-target="#lessons-collapse-{{ $user_id }}-{{ $key }}" aria-controls="lessons-collapse-{{ $user_id }}-{{ $key }}"
                                    >
                                            {{ $user['name'] }}

                                            <span class="-mr-1 ml-auto h-5 w-5 shrink-0 rotate-[-180deg] fill-[#336dec] transition-transform duration-200 ease-in-out group-[[data-te-collapse-collapsed]]:mr-0 group-[[data-te-collapse-collapsed]]:rotate-0 group-[[data-te-collapse-collapsed]]:fill-[#212529] motion-reduce:transition-none"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="h-6 w-6"><path stroke-linecap="round" stroke-linejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" /></svg></span>
                                    </button>
                                </h2>
                                <div id="lessons-collapse-{{ $user_id }}-{{ $key }}" data-te-collapse-item aria-labelledby="lessons-heading-{{ $user_id }}-{{ $key }}" data-te-parent="#XaccordionLessons-{{ $key }}" class="!visible hidden border-0"  wire:ignore.self>
                                    <div class="accordion-body">
                                        @foreach ( $user['status'] as $statuses )
                                            <h6 class="name">{{ $statuses['title'] }}</h6>
                                            @foreach ( $statuses['items'] as $id )
                                                <x-admin.lesson-single :item="$lessons[ $id ]" id="{{ $id }}" classes="date-{{ $key }} status-{{ $statuses['slug'] }}" />
                                            @endforeach
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>

        <div id="tabs-display-by-status" class="hidden opacity-100 transition-opacity duration-150 ease-linear data-[te-tab-active]:block relative" aria-labelledby="tabs-display-by-status-tab" role="tabpanel" wire:ignore.self>
            <x-loading />
            @foreach ( $lessons_by_status as $key => $status_block )
                <div class="by-status relative set set-{{ $status_block['camel'] }}">
                    <div class="set-info">
                        <h5 class="status d-inline"></h5>
                        <span class="hidden previous" wire:click="previousSetByStatus({{ $key  }})"></span>
                        <span class="hidden next" wire:click="nextSetByStatus({{ $key  }})"></span>
                    </div>

                    <div class="accordion" id="accordionLessons-{{ $status_block['camel'] }}">
                        <div class="accordion-item">
                            <h2 class="mb-0" id="lessons-heading-{{ $status_block['camel'] }}">
                                <button class="group relative flex w-full items-center rounded-none border-0 bg-white py-4 text-left text-base text-neutral-800 transition [overflow-anchor:none] hover:z-[2] focus:z-[3] focus:outline-none"
                                    type="button" aria-expanded="false" data-te-collapse-init data-te-collapse-collapsed
                                    data-te-target="#lessons-collapse-{{ $status_block['camel'] }}" aria-controls="lessons-collapse-{{ $status_block['camel'] }}"
                                >
                                        {{ $status_block['title'] }}

                                        <span class="-mr-1 ml-auto h-5 w-5 shrink-0 rotate-[-180deg] fill-[#336dec] transition-transform duration-200 ease-in-out group-[[data-te-collapse-collapsed]]:mr-0 group-[[data-te-collapse-collapsed]]:rotate-0 group-[[data-te-collapse-collapsed]]:fill-[#212529] motion-reduce:transition-none"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="h-6 w-6"><path stroke-linecap="round" stroke-linejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" /></svg></span>
                                </button>
                            </h2>

                            <div id="lessons-collapse-{{ $status_block['camel'] }}" data-te-collapse-item aria-labelledby="lessons-heading-{{ $status_block['camel'] }}" data-te-parent="#XaccordionLessons-{{ $key }}" class="!visible hidden border-0"  wire:ignore.self>
                                <div class="accordion-body">
                                    @foreach ( $status_block['users'] as $user_id => $user )
                                        <div class="user-block user-{{ $user_id }} pb-4" wire:ignore.self>
                                            <h6 class="name">{{ $user['name'] }}</h6>

                                            @foreach ( $user['items'] as $id )
                                                <x-admin.lesson-single :item="$lessons[ $id ]" id="{{ $id }}" classes="status-{{ $status_block['slug'] }}" />
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
