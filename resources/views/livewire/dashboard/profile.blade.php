<div id="admin-profile" class="bg-white p-4 rounded-md hidden opacity-100 transition-opacity duration-150 ease-linear data-[te-tab-active]:block" @if(App\Livewire\Dashboard\Sidebar::checkActiveSection('profile')) data-te-tab-active @endif aria-labelledby="admin-profile-tab">
    <div class="layout-item">
        <h4 class="pb-4">Profile</h4>

        <form wire:submit.prevent="profile" wire:loading.class="loading" enctype="multipart/form-data" wire:click="clearSuccessMessage()">
            @csrf
            <x-form.loading />

            <div class="justify-between items-start mb-6 flex md:-mx-3">
                <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="firstName" label="First Name" wire-model="firstName" value="{{$firstName}}" />

                <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="lastName" label="Last Name" wire-model="lastName" value="{{ $lastName }}" />
            </div>

            <div class="justify-between items-start mb-6 flex md:-mx-3">
                <x-form.input type="tel" wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="phone" label="Mobile" wire-model="phone" value="{{ $phone }}" wire:keyup="formatPhoneNumber" />

                <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="email" label="Email" wire-model="email" value="{{ $email }}" />
            </div>

            <div class="justify-between items-start mb-6 flex md:-mx-3">
                <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="address" label="Address" wire-model="address" value="{{ $address }}" />

                <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="city" label="City/Town" wire-model="city" value="{{ $city }}" />
            </div>

            <div class="justify-between items-start mb-6 flex md:-mx-3">
                <x-form.select provinces="true" wrapperClass="md:w-45/100 md:mx-3" label="Select province" :errors="$errors" value="{{ $province }}" />

                <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="postalCode" label="Postal Code" wire-model="postalCode" value="{{ $postalCode }}" />
            </div>

            <div class="d-grid">
                <button type="submit" class="btn-outline-green hover:btn-green">Save changes</button>
                <x-form.alert tag="h3" />
            </div>
        </form>

        <x-form.alert tag="h6" alert="dashboard-alert-profile" />
        <x-form.message tag="h6" message="dashboard-message-profile" />
    </div>
</div>
