<!-- Sidenav -->
<nav id="sidenav-1" class="sidenav-dashboard absolute py-4 left-0 top-0 z-[1035] h-full w-65 -translate-x-full overflow-hidden text-white bg-gray-700 shadow-[0_4px_12px_0_rgba(0,0,0,0.07),_0_2px_4px_rgba(0,0,0,0.05)] data-[te-Xsidenav-hidden='false']:translate-x-0" data-te-sidenav-init data-te-sidenav-hidden="false" data-te-sidenav-mode="side" data-te-sidenav-content="#admin-content">
    <a class="px-6 block w-full" href="{{ route('home') }}">
        <x-logos.logo-horizontal-green-white class="block h-9 w-auto fill-current w-full max-w-[200px]" />

        <div class="h-px border-b-1 border-white my-2 w-full" style="background: linear-gradient(to right, rgba(255, 255, 255, 0), white, rgba(255, 255, 255, 0));"></div>
    </a>
    <ul class="relative m-0 list-none" role="tablist" data-te-nav-ref>
        <li class="relative">
            <a class="flex cursor-pointer items-center truncate px-6 pt-4 text-[0.875rem] outline-none transition duration-300 ease-linear hover:bg-slate-50 hover:text-black hover:outline-none focus:bg-slate-50 focus:text-gray-700 focus:outline-none active:bg-slate-50 active:text-gray-700 active:outline-none">
                <i class="fa-solid fa-grid-horizontal mr-4 [&>svg]:h-4 [&>svg]:w-4 [&>svg]:text-gray-400"></i>

                <span>Dashboard</span>

                <span class="absolute right-0 ml-auto mr-[0.8rem] transition-transform duration-300 ease-linear motion-reduce:transition-none [&>svg]:text-white" data-te-sidenav-rotate-icon-ref>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="h-5 w-5">
                        <path fill-rule="evenodd" d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z" clip-rule="evenodd" />
                    </svg>
                </span>
            </a>

            <ul class="!visible relative m-0 hidden list-none p-0 data-[te-collapse-show]:block " data-te-sidenav-collapse-ref data-te-collapse-show>
                @foreach($sections_dashboard as $section => $fa)
                    <li class="relative" role="presentation">
                        <a class="flex cursor-pointer items-center truncate py-1 pl-[3.4rem] pr-6 text-[0.78rem] outline-none transition ease-linear motion-reduce:transition-none px-[0.2rem]
                        hover:outline-none hover:bg-slate-50 hover:text-black
                        focus:bg-slate-50 focus:text-gray-700 focus:outline-none data-[te-nav-focus]:outline-none
                        active:bg-slate-50 active:text-gray-700 active:outline-none
                        data-[te-nav-active]:bg-slate-50 data-[te-nav-active]:text-gray-700"
                            data-te-toggle="pill" role="tab" aria-selected="true"
                            data-te-target="#admin-{{ $section }}" aria-controls="admin-{{ $section }}" @if($section === $start)  data-te-nav-active @endif
                        >
                            <i class="w-[13px] fa-solid fa-{{ $fa }} mr-4 [&>svg]:h-4 [&>svg]:w-4 [&>svg]:text-gray-400"></i>
                            <span>{{ Str::title( $section ) }}</span>
                        </a>
                    </li>
                @endforeach

                <li class="relative">
                    <a class="flex cursor-pointer items-center truncate py-1 pl-[3.4rem] pr-6 text-[0.78rem] outline-none transition ease-linear motion-reduce:transition-none px-[0.2rem]
                    hover:outline-none hover:bg-slate-50 hover:text-black
                    focus:bg-slate-50 focus:text-gray-700 focus:outline-none data-[te-nav-focus]:outline-none
                    active:bg-slate-50 active:text-gray-700 active:outline-none
                    data-[te-nav-active]:bg-slate-50 data-[te-nav-active]:text-gray-700"
                        href="{{ route('logout') }}">
                        <i class="w-[13px] fa-solid fa-arrow-right-from-bracket mr-4 [&>svg]:h-4 [&>svg]:w-4 [&>svg]:text-gray-400"></i>
                        <span>Log Out</span>
                    </a>
                </li>
            </ul>
        </li>

        @foreach($sections_others as $section => $fa)
            <li class="relative" role="presentation">
                <a class="flex cursor-pointer items-center truncate px-6 py-2 text-[0.875rem] outline-none transition duration-300 ease-linear
                    hover:outline-none hover:bg-slate-50 hover:text-black
                    focus:bg-slate-50 focus:text-gray-700 focus:outline-none data-[te-nav-focus]:outline-none
                    active:bg-slate-50 active:text-gray-700 active:outline-none
                    data-[te-nav-active]:bg-slate-50 data-[te-nav-active]:text-gray-700"
                    data-te-toggle="pill" role="tab" aria-selected="true"
                    data-te-target="#admin-{{ $section }}" aria-controls="admin-{{ $section }}" @if($section === $start)  data-te-nav-active @endif
                >
                    <i class="w-[13px] fa-solid fa-{{ $fa }} mr-4 [&>svg]:h-4 [&>svg]:w-4 [&>svg]:text-gray-400"></i>
                    <span>{{ Str::title( $section ) }}</span>
                </a>
            </li>
        @endforeach
    </ul>

    <!-- Toggler -->
    <button class="hidden mt-10 inline-block rounded bg-green px-6 py-2.5 text-xs font-medium uppercase leading-tight text-white shadow-md transition duration-150 ease-in-out hover:bg-green-700 hover:shadow-lg focus:bg-green-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-green-800 active:shadow-lg" data-te-sidenav-toggle-ref data-te-target="#sidenav-1" aria-controls="#sidenav-1" aria-haspopup="true">
        <span class="block [&>svg]:h-5 [&>svg]:w-5 [&>svg]:text-white">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="h-5 w-5">
                <path fill-rule="evenodd" d="M3 6.75A.75.75 0 013.75 6h16.5a.75.75 0 010 1.5H3.75A.75.75 0 013 6.75zM3 12a.75.75 0 01.75-.75h16.5a.75.75 0 010 1.5H3.75A.75.75 0 013 12zm0 5.25a.75.75 0 01.75-.75h16.5a.75.75 0 010 1.5H3.75a.75.75 0 01-.75-.75z" clip-rule="evenodd" />
            </svg>
        </span>
    </button>
    <!-- Toggler -->
</nav>
<!-- Sidenav -->
