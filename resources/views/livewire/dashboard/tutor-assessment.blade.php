<div id="admin-assessment" class="bg-white p-4 rounded-md hidden opacity-100 transition-opacity duration-150 ease-linear data-[te-tab-active]:block" @if(App\Livewire\Dashboard\Sidebar::checkActiveSection('assessment')) data-te-tab-active @endif role="tabpanel" aria-labelledby="admin-assessment-tab" wire:ignore.self>
    <h3 class="mb-4">Assessment</h3>

    <form wire:submit.prevent="storeAssessment" @if(session()->has('assessment-message')) class="hidden needs-validation" @endif wire:loading.class="loading">
        @csrf
        <x-form.loading />
        <div class="demographic section">
            <h5 class="section-heading text-blue mb-4">Demographic</h5>

            <div class="mt-4 justify-between items-start mb-6 flex md:-mx-3">
                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">What gender do you identify as?</div>

                    <x-form.select label="Select Gender" :errors="$errors" id="gender" :options="App\Helpers\Misc::$genderOptions" wireModelDefer="false" wireModel="gender" wire:change="changeGenderClass" value="{{ $gender }}" />

                    <x-form.input wrapperClass="{{ $genderClass }} mt-4" :errors="$errors" id="genderExtra" label="Other gender(s)" wireModel="genderExtra" value="{{ $genderExtra }}" />
                </div>

                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">Which category below includes your age?</div>

                    <x-form.select label="Select Age" :errors="$errors" id="age" :options="App\Helpers\Misc::$ageOptions" wireModel="age" value="{{ $age }}" />
                </div>
            </div>

            <div class="mt-4 justify-between items-end mb-6 flex md:-mx-3">
                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">What is the highest degree or level of education you have completed?</div>
                    <x-form.select :errors="$errors" id="education" label="Select Education" value="{{ $education }}" wire:model.defer="education" :options="App\Helpers\Misc::$educationOptions" />
                </div>

                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">Do you have the means to travel to a client’s home if necessary?</div>
                    <x-form.select :errors="$errors" id="travel" label="Select Travel Availability" value="{{ $travel }}" wire:model.defer="travel" :options="App\Helpers\Misc::$travelOptions" />
                </div>
            </div>

            <div class="mt-4 justify-between items-end mb-6 flex md:-mx-3">
                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">How many hours a week are you available?</div>
                    <x-form.select :errors="$errors" id="availability" label="Select Availability" value="{{ $availability }}" wire:model.defer="availability" :options="App\Helpers\Misc::$availabilityOptions" />
                </div>
                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">What is your native language (mother tongue)?</div>
                    <x-form.input wrapperClass="" :errors="$errors" id="nativeLanguage" label="false" wire-model="nativeLanguage" value="{{ $nativeLanguage }}" />
                </div>
            </div>

            <div class="mt-4 justify-between items-end mb-2 flex md:-mx-3">
                <div class="question italic md:mx-3">Do you speak any additional languages? If yes, name them and rate your proficiency using the following terms: beginner, intermediate, advanced, fluent. (i.e. Mandarin, Fluent)</div>

                <div class="md:mx-3 w-full">
                    @foreach($languages as $key => $value)
                        <div class="flex items-center justify-between mt-2 language-{{ $key }}">
                            <x-form.input wrapperClass="w-50 md:w-2/5 mr-3" :errors="$errors"
                                id="languages.{{ $key }}.name"
                                label="Language Name"
                                wire-model="languages.{{ $key }}.name"
                                value="{{ $value['name'] }}"
                            />

                            <x-form.input wrapperClass="w-50 md:w-2/5" :errors="$errors"
                                id="languages.{{ $key }}.proficiency"
                                label="Language Proficiency"
                                wire-model="languages.{{ $key }}.proficiency"
                                value="{{ $value['proficiency'] }}"
                            />

                            <div class="remove-button text-end md:ml-3">
                                <button class="btn btn btn-outline-pink min-w-0 max-w-0 mt-0" wire:click.prevent="languageRemove({{$key}})"><x-icons.xmark class="-ml-[4px]" /></button>
                            </div>
                        </div>
                   @endforeach

                    <div class="add-button text-end w-full">
                        <button class="btn-outline-green hover:bg-green hover:text-white mt-2" wire:click.prevent="languageAdd">Add Language</button>
                    </div>
                </div>
           </div>
        </div>

        <div class="technical section">
            <h5 class="section-heading text-blue mb-4">Technical Questions</h5>

            <div class="mt-4 justify-between items-start mb-6 mt-4 flex md:-mx-3">
                <div class="md:mx-3">
                    <div class="question italic mb-2">For each device that you use daily, please fill in the type of device (i.e. phone, tablet, laptop, desktop, including brand, model number), how long you've be using it and what you use it for.</div>

                    <div class="flex items-center justify-between">
                        @foreach($devices as $key => $value)
                            <div class="flex items-center justify-between mt-2 w-full device-{{ $key }}">
                                <x-form.input wrapperClass="w-full md:w-1/3" :errors="$errors" label="Type of device"
                                    id="devices.{{ $key }}.type"
                                    wire-model="devices.{{ $key }}.type"
                                    value="{{ $value['type'] }}"
                                />

                                <x-form.input wrapperClass="w-full md:w-1/4" :errors="$errors" label="Length of use"
                                    id="devices.{{ $key }}.useLength"
                                    wire-model="devices.{{ $key }}.useLength"
                                    value="{{ $value['useLength'] }}"
                                />

                                <x-form.input wrapperClass="w-full md:w-1/3" :errors="$errors" label="Purposes"
                                    id="devices.{{ $key }}.purposes"
                                    wire-model="devices.{{ $key }}.purposes"
                                    value="{{ $value['purposes'] }}"
                                />

                                <div class="remove-button text-end md:ml-3">
                                    <button class="btn btn btn-outline-pink min-w-0 max-w-0 mt-0" wire:click.prevent="deviceRemove({{$key}})"><x-icons.xmark class="-ml-[4px]" /></button>
                                </div>
                            </div>
                       @endforeach

                        <div class="add-button text-end w-full">
                            <button class="btn-outline-green hover:bg-green hover:text-white mt-2" wire:click.prevent="deviceAdd">Add Device</button>
                        </div>
                    </div>
               </div>
            </div>

            <div class="mt-4 justify-between items-end mb-6 mt-4 flex md:-mx-3">
                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">How would you rate your ability to use the Internet? (i.e. perform basic functions like checking email, browsing social media, shopping online, basic troubleshooting)</div>
                    <x-form.select :errors="$errors" id="internetProficiency" label="Select internet proficiency" wire:model.defer="internetProficiency" value="{{ $internetProficiency }}" :options="App\Helpers\Misc::$internetOptions" />
                </div>

                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">Approximately how many hours per week do you spend using a computer/tablet/ laptop for personal activities?</div>

                    <x-form.select :errors="$errors" id="techTime" value="{{ $techTime }}" label="Select tech usage" wire:model.defer="techTime" :options="App\Helpers\Misc::$techTimeOptions" />
                </div>
            </div>


            <div class="mt-4 justify-between items-start mb-6 mt-4 flex md:-mx-3">
                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">What computer software do you have experience using? (Check all that apply)</div>

                    <x-form.checklist id="software" flex="false" label="" defer="false" wire:change="changeSoftwareClass" :value="$software" wire-model="software" :options="App\Helpers\Misc::$softwareOptions" />

                    <x-form.textarea wrapperClass="{{ $otherSoftwareClass }} mt-2" :errors="$errors" id="otherSoftware" label="Other Software(s)" wire-model="otherSoftware" value="{{ $otherSoftware }}" rows="6" style="height:100%;" />
                </div>

                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">Do you have experience working with the following applications? (Check all that apply)</div>

                    <x-form.checklist id="appExperience" flex="false" label="" defer="false" wire:change="changeAppExperienceClass" :value="$appExperience" wire-model="appExperience" :options="App\Helpers\Misc::$appExperienceOptions" />

                    <x-form.textarea wrapperClass="{{ $otherAppExperienceClass }} mt-2" :errors="$errors" id="otherAppExperience" label="Other App(s)" wire-model="otherAppExperience" value="{{ $otherAppExperience }}" rows="6" style="height:100%;" />
                </div>
            </div>

            <div class="mt-4 justify-between items-start mb-6 mt-4 flex md:-mx-3">
                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">Which type of apps do you currently have on your digital devices? (Check all that apply)</div>

                    <x-form.checklist id="appsUsed" flex="false" label="" defer="false" wire:change="changeAppsUsedClass" :value="$appsUsed" wire-model="appsUsed" :options="App\Helpers\Misc::$appsUsedOptions" />
                </div>

                <div class="md:w-45/100 md:mx-3">
                    <x-form.textarea wrapperClass="{{ $otherAppsUsedClass }}" :errors="$errors" id="otherAppsUsed" label="Other App(s)" wire-model="otherAppsUsed" value="{{ $otherAppsUsed }}" rows="6" style="height:100%;" />
                </div>
            </div>

            <div class="mt-4 justify-between items-end mb-6 mt-4 flex md:-mx-3">
                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">If you are faced with a technical situation that you don’t know how to solve immediately, are you able to find possible solutions on the Internet?</div>

                    <x-form.switch wireModelDefer="0" :errors="$errors" id="internetSolutions" label="Yes, I am able to find possible solutions on the Internet" wire:change="changeInternetSolutionsExtraClass" wire-model="internetSolutions" value="{{ $internetSolutions }}" />
                </div>

                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">Do you have experience setting up a printer or scanner?</div>

                    <x-form.switch wireModelDefer="0" :errors="$errors" id="printerExperience" label="Yes, I have experience setting up a printer or scanner" wire:change="changePrinterExperienceExtraClass" wire-model="printerExperience" value="{{ $printerExperience }}" />
                </div>
            </div>

            <div class="mt-4 justify-between items-start mb-6 -mt-4 flex md:-mx-3">
                <div class="md:w-45/100 md:mx-3">
                    <x-form.textarea wrapperClass="{{ $internetSolutionsExtraClass }}" :errors="$errors" id="internetSolutionsExtra" label="More Info" wire-model="internetSolutionsExtra" value="{{ $internetSolutionsExtra }}" rows="6" style="height:100%;" />
                </div>

                <div class="md:w-45/100 md:mx-3">
                    <x-form.textarea wrapperClass="{{ $printerExperienceExtraClass }}" :errors="$errors" id="printerExperienceExtra" label="More Info" wire-model="printerExperienceExtra" value="{{ $printerExperienceExtra }}" rows="6" style="height:100%;"  />
                </div>
            </div>

            <div class="mt-4 justify-between items-end mb-6 mt-4 flex md:-mx-3">
                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">Do you have experience tutoring?</div>

                    <x-form.switch wireModelDefer="0" :errors="$errors" id="tutoringExperience" label="Yes, I have experience tutoring" wire:change="changeTutoringExperienceExtraClass" wire-model="tutoringExperience" value="{{ $tutoringExperience }}" />
                </div>

                <div class="md:w-45/100 md:mx-3">
                    <div class="question italic mb-2">Have you ever assisted one of your older relatives with their technology issues?</div>

                    <x-form.switch wireModelDefer="0" :errors="$errors" id="assistedRelativesExperience" label="Yes, I have assisted one of my older relatives with their technology issues" wire:change="changeAssistedRelativesExperienceExtraClass" wire-model="assistedRelativesExperience" value="{{ $assistedRelativesExperience }}" />
                </div>
            </div>

            <div class="mt-4 justify-between items-start mb-6 -mt-4 flex md:-mx-3">
                <div class="md:w-45/100 md:mx-3">
                    <x-form.textarea wrapperClass="{{ $tutoringExperienceExtraClass }}" :errors="$errors" id="tutoringExperienceExtra" label="More Info" wire-model="tutoringExperienceExtra" value="{{ $tutoringExperienceExtra }}" rows="6" style="height:100%;"  />
                </div>

                <div class="md:w-45/100 md:mx-3">
                    <x-form.textarea wrapperClass="{{ $assistedRelativesExperienceExtraClass }}" :errors="$errors" id="assistedRelativesExperienceExtra" label="More Info" wire-model="assistedRelativesExperienceExtra" value="{{ $assistedRelativesExperienceExtra }}" rows="6" style="height:100%;" />
                </div>
            </div>

            <div class="mb-6 mt-4">
                <div class="part mt-5">
                    <div class="question italic mb-2">Do you have any comments, questions or concerns?</div>

                    <x-form.textarea wrapperClass="" :errors="$errors" id="comments" label="false" wire-model="comments" value="{{ $comments }}" rows="8" style="height:100%;" />
                </div>
            </div>
        </div>

        <div class="mt-4">
            <button type="submit" class="btn-outline-green hover:btn-green">Submit Assessment</button>
        </div>
    </form>

    <x-form.alert tag="h3" alert="assessment-alert" />
    <x-form.message tag="h6" message="assessment-message" />
</div>
