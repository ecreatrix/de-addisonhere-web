<div id="admin-settings" class="bg-white p-4 rounded-md hidden opacity-100 transition-opacity duration-150 ease-linear data-[te-tab-active]:block" @if(App\Livewire\Dashboard\Sidebar::checkActiveSection('settings')) data-te-tab-active @endif role="tabpanel" aria-labelledby="admin-settings-tab">
    <div class="layout-item">
        <h3 class="pb-4">Settings</h3>

        <form wire:submit.prevent="settings" wire:loading.class="loading" enctype="multipart/form-data" wire:click="clearSuccessMessage()">
            @csrf
            <x-form.loading />
            <h4 class="text-blue">Calendar</h4>
            <div class="mt-4 mb-3 flex items-center">
                <p class="w-full md:w-1/5 mr-4 font-semibold">First day of the week</p>

                <div class="flex md:-mx-3 w-full md:w-4/5">
                    <x-form.select label="Select day" :errors="$errors" id="weekStart" wireModel="weekStart" wrapperClass="w-full md:w-45/100 md:mx-3" value="{{ $weekStart }}" :options="App\Helpers\Misc::$weekStart" />
                </div>
            </div>

            <div class="d-grid">
                <button type="submit" class="btn-outline-green hover:btn-green">Update settings</button>
            </div>
        </form>
        <x-form.alert tag="h6" alert="dashboard-alert-settings" />
        <x-form.message tag="h6" message="dashboard-settings-message" />
    </div>
</div>
