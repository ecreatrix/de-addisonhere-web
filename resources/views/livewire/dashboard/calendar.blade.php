<div id="admin-calendar" class="bg-white p-4 rounded-md hidden opacity-100 transition-opacity duration-150 ease-linear data-[te-tab-active]:block" @if(App\Livewire\Dashboard\Sidebar::checkActiveSection('calendar')) data-te-tab-active @endif role="tabpanel" aria-labelledby="admin-calendar-tab" data-te-tab-active>
    <div class="layout-item">
        <div class="flex-1 mb-4 heading flex justify-between items-center">
            <h4 class="pb-4">Calendar</h4>

            <button class="ms-4 compose-button create-lesson-button flex items-center" role="button" aria-expanded="false"
                data-te-collapse-init data-te-ripple-init data-te-ripple-color="light"
                href="#compose-lesson" aria-controls="compose-lesson">
                <i class="fa-solid fa-pencil"></i>
                <span class="ml-2">Create an event</span>
            </button>
        </div>

        x-form.alert tag="h6" alert="dashboard-alert-calendar" />
        x-form.message tag="h6" message="dashboard-message-calendar" />

        livewire:dashboard.calendar />

        x-toast id="calendarToast" items="{ json_encode( $toasts ) }}" />

        x-admin.calendar-compose :tutors="$tutors" :tutees="$tutees" />
    </div>
</div>
