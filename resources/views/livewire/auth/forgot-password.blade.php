<div class="mt-5" @if(session()->has('message')) class="hidden" @endif>
    <h5 class="text-blue mb-4">
        No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.
    </h5>

    <form wire:submit.prevent="store" wire:loading.class="loading">
        @csrf
        <x-form.loading />

        <div class="mb-3">
            <input type="text" classes="" error_object="{{ $errors->__toString() }}" id="email" label="Email" name="email" wire:model.defer="email" />
        </div>

        <div class="">
            <button type="submit" class="btn btn btn-lg btn btn-outline-pink">Email Password Reset Link</button>
        </div>

        <x-form.alert tag="h6" />
    </form>

    @if (session()->has('message'))
        <h6 class="alert alert-green mt-4">{!! session('message') !!}</h6>
    @endif
</div>
