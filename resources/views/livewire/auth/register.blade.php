<div class="">
    <h4>Sign up to get started</h4>

    <div class="w-full md:w-2/3 mt-4">
        <form wire:submit.prevent="store" @if(session()->has('message')) class="hidden" @endif wire:loading.class="loading">
            @csrf
            <x-form.loading />

            <x-form.radio name="category" :errors="$errors" id="category" wire-model="category" key="category" parentWrapperClass="justify-between mb-6 flex md:-mx-3 has-validation" wrapperClass="w-full md:w-45/100 md:mx-3" wrapperClass="w-full md:w-45/100 md:mx-3" errorWrapperClass="w-full md:w-45/100 md:mx-3 -mt-3" :options="[
                'tutee' => 'I need help with technology',
                'tutor' => 'I would like to be a tutor',
            ]" />


            <div class="justify-between mb-6 flex md:-mx-3 has-validation @error('category') is-invalid @enderror">
                <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="firstName" label="First Name" wire-model="firstName" />
                <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="lastName" label="Last Name" wire-model="lastName" />
            </div>

            <div class="justify-between mb-6 flex md:-mx-3">
                <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="phone" label="Mobile" wire-model="phone" />
                <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="email" label="Email" wire-model="email" />
            </div>

            <div class="justify-between mb-6 flex md:-mx-3">
                <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="address" label="Address" wire-model="address" />
                <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="city" label="City/Town" wire-model="city" />
            </div>

            <div class="justify-between mb-6 flex md:-mx-3">
                <x-form.select provinces="true" wrapperClass="md:w-45/100 md:mx-3" label="Select your province" :errors="$errors" />
                <x-form.input wrapperClass="w-full md:w-45/100 md:mx-3" :errors="$errors" id="postalCode" label="Postal Code" wire-model="postalCode" />
            </div>

            <div class="mb-6 flex md:-mx-3">
                <x-form.input wrapperClass="w-full md:mx-3" :errors="$errors" id="username" label="Username" wire-model="username" />
            </div>

            <div class="justify-between mb-6 flex md:-mx-3">
                <x-form.password :errors="$errors" id="password" label="Password" wire-model="password" showPassword="{{$showPassword}}" />
                <x-form.password :errors="$errors" id="confirmPassword" label="Confirm Password" wire-model="confirmPassword" showPassword="{{$showPassword}}" />
            </div>

            <div class="mb-3">
                <div class="check" container_classes="col-12" id="terms" label="I agree to AddisonHere’s Terms and Conditions" wire-model="terms">
            </div>

            <button type="submit" class="btn btn btn-outline-pink w-full max-w-full">Create Account</button>

            <x-form.alert tag="h6" alert="alert" />
            <x-form.message tag="h6" message="message" />
        </form>
    </div>
</div>
