<div class="container">
    <x-auth-session-status class="mb-4" :status="session('status')" />

    <div class="md:w-2/3 mx-auto">
        <div class="w-full">
            <form wire:submit.prevent="store" @if(session()->has('message')) class="hidden" @endif wire:loading.class="loading">
                @csrf
                <x-form.loading />

                <x-form.input inputClass="mb-3" name="username" :errors="$errors" id="loginUsername" placeholder="Username" wireModel="username" label="Username" />

                <x-form.password :errors="$errors" id="password" label="Password" wire-model="password" showPassword="{{$showPassword}}" />

                <div class="flex items-center justify-between mt-3">
                    <div class="flex items-center">
                        <input type="checkbox" class="rounded-sm" id="remember" label="Remember Me" wire:model.defer="remember" />
                        <label for="remember" class="ml-3">Remember Me</label>
                    </div>

                    <div>
                        @if (Route::has('password.request'))
                            <a class="underline text-reset text-end pe-4" href="{{ route('password.request') }}">
                                {{ __('Forgot your password?') }}
                            </a>
                        @endif

                        <button type="submit" class="btn-outline-pink btn btn-lg mt-0">Log In</button>
                    </div>
                </div>

                <x-form.alert tag="h3" />
            </form>

            <x-form.message tag="h6" />
        </div>
    </div>
</div>
