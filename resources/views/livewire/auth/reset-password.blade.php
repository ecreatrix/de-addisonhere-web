<div>
    <form wire:submit.prevent="store" @if(session()->has('message')) class="hidden" @endif wire:loading.class="loading">
        @csrf
        <x-form.loading />

        <!-- Password Reset Token -->
        <input type="hidden" name="token" value="{{ $token }}">

        <div class="mb-3">
            <input type="text" error_object="{{ $errors->__toString() }}" classes="" id="email" label="Email" name="email" wire:model.defer="email" />
        </div>

        <div class="mb-3">
            <x-form.password :errors="$errors" id="password" label="Password" wire-model="password" showPassword="{{$showPassword}}" />
        </div>

        <div class="mb-3">
            <x-form.password :errors="$errors" id="confirmPassword" label="Confirm Password" wire-model="confirmPassword" showPassword="{{$showPassword}}" />
        </div>

        <div class="">
            <button type="submit" class="btn btn btn-lg btn btn-outline-pink">Reset Password</button>
        </div>

        @if(session()->has('alert'))
            <h6 class="alert alert-blue mt-4">{!! session('alert') !!}</h6>
        @endif
    </form>

    @if (session()->has('message'))
        <h6 class="alert alert-green mt-4">{!! session('message') !!}</h6>
    @endif
</div>
