@props(['active'])

@php
$default = 'block py-2 transition duration-200 hover:bg-green hover:text-black hover:ease-in-out focus:text-neutral-700 disabled:text-neutral-600/30 motion-reduce:transition-none lg:px-2 [&.active]:text-black/90 w-full';
$classes = ($active ?? false)
    ? $default. ' text-pink '
    : $default. ' text-black ';

//$list = filter_var($list, FILTER_VALIDATE_BOOLEAN);
@endphp



<a {{ $attributes->merge(['class' => $classes]) }} data-te-nav-link-ref>
    {{ $slot }}
</a>
