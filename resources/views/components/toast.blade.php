@props([
    'items' => '',
])

@if( $items != null )
    <div class="toast-container">
        @foreach( json_decode(html_entity_decode($items), true ) as $toast)
            <div id="{{ $toast[ 'id'] }}" class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-bs-delay="10000">
                <div class="toast-header">
                    <strong class="me-auto">{!! $toast[ 'title' ] !!}</strong>
                    <small>{!! $toast[ 'small' ] !!}</small>
                    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
                <div class="toast-body">
                    {!! $toast[ 'body' ] !!}
                </div>
            </div>
        @endforeach
    </div>
@endif
