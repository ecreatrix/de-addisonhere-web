@props([
    'wrapperClass' => 'w-full',
    'errorClass' => '',
    'errorWrapperClass' => '',
    'provinces' => false,
    'options' => false,
    'label' => false,
    'id' => false,
    'wireModel' => false,
    'wireModelDefer' => true,
    'value' => false,
    'errors' => (object)[],
])

@php
    $id_no = Str::random(9);

    //clock($wireModel);
    //clock($attributes);

    if($provinces) {
        $id = 'province';
        $label='Select Province';

        if($value) {
            $value = App\Helpers\Misc::$provinces[$value];
        }
    }

    $id_no = $id.'_'.$id_no;
@endphp

@if( $options )
<div class="{{ $wrapperClass }}">
<div class="bg-white" wire:ignore>
    <select class="singleSelection" data-selected="{{ $value }}" id="{{ $id_no }}" data-te-select-init data-te-select-size="lg"
        @if($wireModel)
            @if(!$wireModelDefer || $wireModelDefer == 'false')
                wire:model="{{ $wireModel }}"
            @else
                wire:model.defer="{{ $wireModel }}"
            @endif
        @endif
        {{ $attributes }}
    >
        <option class="h-14" value="false">{{ $label }}</option>

        @foreach($options as $key => $name)
            <option class="h-14" value="{{ $key }}" @if($key == $value) selected @endif>{{ $name }}</option>
        @endforeach
    </select>
</div>

<div class="{{ $errorWrapperClass }}">
    @if($errors && $errors->has($id))
        <div class="{{ $errorClass }} invalid-feedback mt-2 text-pink">
            {{$errors->first($id)}}
        </div>
    @endif
</div>
</div>
@endif
