@props([
    'tag' => 'h6',
    'message' => 'message'
])

@if (session()->has($message))
    <{{ $tag }} class="alert alert-green mt-4">{!! session($message) !!}</{{ $tag }}>
@endif
