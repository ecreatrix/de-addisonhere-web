@props([
    'tag' => 'h6',
    'alert' => 'alert'
])

@if(session()->has($alert))
    <{{ $tag }} class="alert text-pink mt-4">{!! session($alert) !!}</{{ $tag }}>
@endif
