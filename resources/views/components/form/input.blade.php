@props([
    'wrapperClass' => 'w-full',
    'inputClass' => '',
    'labelClass' => '',
    'label' => false,
    'type' => 'text',
    'value' => false,
    'wireModel' => false,
    'wireModelDefer' => true,
    'errors' => (object)[],
    'placeholder' => false,
])

@php
    $id = Str::random(9);

    $inputClass = trim($inputClass . " w-full rounded-md h-14 border-0 bg-white peer block min-h-[auto] px-3 py-[0.32rem] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 peer-focus:text-green data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none focus:border-transparent focus:ring-0 [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" );

    $labelClass = trim($labelClass . " pointer-events-none absolute left-3 top-0 mb-0 max-w-[90%] origin-[0_0] truncate leading-14 text-neutral-500 transition-all duration-200 ease-out peer-focus:-translate-y-[.15rem] peer-focus:scale-[0.8] peer-focus:text-green peer-data-[te-input-state-active]:-translate-y-[1.15rem] peer-data-[te-input-state-active]:scale-[0.8] motion-reduce:transition-none" );


@endphp

<div class="{{ $wrapperClass }}">
    <div class="relative w-full" data-te-input-wrapper-init wire:ignore>
        <input {{ $attributes }} id="{{ $id }}" class="{{ $inputClass }}"
            @if($placeholder) placeholder="{{ $placeholder }}" @endif
            @if($wireModel)
                @if($wireModelDefer)
                    wire:model.defer="{{ $wireModel }}"
                @else
                    wire:model="{{ $wireModel }}"
                @endif
            @endif
            @if($value) value="{{ $value }}" @endif
            type="{{ $type }}" {{ $attributes }}
        />

        @if($label && $label != 'false')<label for="{{ $id }}" class="{{ $labelClass }}">{{ $label }}</label>@endif
    </div>

    @if($errors && $errors->has($wireModel))
        <div class="invalid-feedback mb-5 mt-2 text-pink">
            {{ $errors->first($wireModel) }}
        </div>
    @endif
</div>
