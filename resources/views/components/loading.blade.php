@props([
])

<div class="processing absolute bg-black/40 h-full left-0 lesson-processing top-0 w-full z-[1000]" wire:loading><div class="w-full h-full flex items-center justify-center "><div class="h-8 w-8 animate-[spinner-grow_0.75s_linear_infinite] rounded-full bg-current align-[-0.125em] opacity-0 motion-reduce:animate-[spinner-grow_1.5s_linear_infinite]" role="status"><span class="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]">Loading...</span ></div></div></div>
