@props(['active'])

@php
$default = 'uppercase p-0 transition duration-200 hover:text-green hover:ease-in-out focus:text-neutral-700 disabled:text-neutral-600/30 motion-reduce:transition-none md:px-2 [&.active]:text-black/90 my-2 md:my-0';
$classes = ($active ?? false)
    ? $default. ' text-pink '
    : $default. ' text-black ';

//$list = filter_var($list, FILTER_VALIDATE_BOOLEAN);
@endphp


<a {{ $attributes->merge(['class' => $classes]) }} data-te-nav-link-ref>
    {{ $slot }}
</a>
