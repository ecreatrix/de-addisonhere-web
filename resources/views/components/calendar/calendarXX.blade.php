<div
    @if($pollMillis !== null && $pollAction !== null)
        wire:poll.{{ $pollMillis }}ms="{{ $pollAction }}"
    @elseif($pollMillis !== null)
        wire:poll.{{ $pollMillis }}ms
    @endif
>
    <div>
        @includeIf($beforeCalendarView)
    </div>

    <div class="month-info">
        <div class="today"><span class="btn btn-tertiary" wire:click="goToCurrentMonth()">Go To Current Month</span></div>
        <div class="controls col-4">
            <span class="col previous" wire:click="goToPreviousMonth()"></span>
            <h6 class="col month d-inline">{{ $month }}</h6>
            <span class="col next" wire:click="goToNextMonth()"></span>
        </div>
    </div>

    <div class="{{ Wa\LivewireCalendar\LivewireCalendarHelpers::month_class( $monthGrid ) }}">
        <div class="overflow-auto w-100">
            <div class="inline-block w-100">

                <div class="w-100 flex flex-row">
                    @foreach($monthGrid->first() as $day)
                        @include($dayOfWeekView, ['day' => $day])
                    @endforeach
                </div>

                @foreach($monthGrid as $week)
                    <div class="w-100 flex flex-row">
                        @foreach($week as $day)
                            @include($dayView, [
                                    'componentId' => $componentId,
                                    'day' => $day,
                                    'dayInMonth' => $day->isSameMonth($startsAt),
                                    'isToday' => $day->isToday(),
                                    'events' => $getEventsForDay($day, $events),
                                ])
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div>
        @includeIf($afterCalendarView)
    </div>
</div>
