<div class="flex col border -mt-px -ms-px flex items-center justify-center bg-indigo-100 weekday min-w-[10rem]">
    <span class="fs-sm my-1">
        {{ $day->format('l') }}
    </span>
</div>
