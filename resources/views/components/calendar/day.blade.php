<div
    ondragenter="onLivewireCalendarEventDragEnter(event, '{{ $componentId }}', '{{ $day }}', '{{ $dragAndDropClasses }}');"
    ondragleave="onLivewireCalendarEventDragLeave(event, '{{ $componentId }}', '{{ $day }}', '{{ $dragAndDropClasses }}');"
    ondragover="onLivewireCalendarEventDragOver(event);"
    ondrop="onLivewireCalendarEventDrop(event, '{{ $componentId }}', '{{ $day }}', {{ $day->year }}, {{ $day->month }}, {{ $day->day }}, '{{ $dragAndDropClasses }}');"
    class="day flex grow lg:h-48 border border-gray-200 mt-px ms-px {{ App\Livewire\Dashboard\CalendarHelpers::day_class($day) }}"
>

    {{-- Wrapper for Drag and Drop --}}
    <div
        class="w-full h-full"
        id="{{ $componentId }}-{{ $day }}">

        <div
            @if($dayClickEnabled)
                wire:click="onDayClick({{ $day->year }}, {{ $day->month }}, {{ $day->day }})"
            @endif
            class="w-full h-full p-2 {{ $dayInMonth ? $isToday ? 'bg-blue/10' : ' bg-white ' : 'bg-gray-100' }} flex flex-column">

            {{-- Number of Day --}}
            <div class="flex items-center">
                <p class="fs-sm col day-number {{ $dayInMonth ? ' ' : '' }}">
                    {{ $day->format('j') }}
                </p>
                <p class="fs-sm col num-events ms-4">
                    @if($events->isNotEmpty())
                        {{ $events->count() }} {{ Str::plural('event', $events->count()) }}
                    @endif
                </p>
            </div>

            {{-- Events --}}
            <div class="p-2 my-2 flex overflow-y-auto">
                <div class="events">
                    @foreach($events as $event)
                        <div
                            @if($dragAndDropEnabled)
                                draggable="true"
                            @endif
                            ondragstart="onLivewireCalendarEventDragStart(event, '{{ $event['id'] }}')">
                            @include($dayOfWeekView, [ 'event' => $event, ])
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div>
