@if(array_key_exists('event', get_defined_vars()['__data']))
    <div
        @if($eventClickEnabled)
        wire:click.stop="onEventClick('{{ $event['id']  }}')"
        @endif
        class="{{ App\Livewire\Dashboard\CalendarHelpers::event_class($event)  }}">

        <p class="fs-sm">
            {{ $event['title'] }}
        </p>
        <p class="mt-2 fs-sm">
            {{ $event['description'] ?? 'No description' }}
        </p>
    </div>
@endif
