@props([
    'classes',
    'id',
    'item' => [],
])

<div class="single flex {{ $classes }} justify-between items-center my-3" wire:ignore.self>
    <div class="date w-1/4">{{ date( 'd/m/Y', strtotime( $item['timeslot'] ) ) }}</div>
    <div class="cost w-1/4">${{ $item['cost'] }}</div>

    <button class="cost approval mt-0
        @if( $item['approved'] ) decline btn btn-pink" wire:click="statusRejectLesson({{ $id }})">
            <span>Click here to Decline</span>
        @else accept btn btn-green" wire:click="statusAcceptLesson({{ $id }})">
            <span>Click here to Accept</span>
        @endif
    </button>
    <div class="hidden icon col-1"><i class="fa-solid fa-ellipsis-vertical"></i></div>
</div class="single">
