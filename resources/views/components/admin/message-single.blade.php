@props([
    'info' => [],
    'top_id' => false,
])

@if( $info != null )
    <div class="single-message last:border-0 last:mb-0 last:pb-0 border-b mb-3 pb-3 border-gray-300 {{ App\Livewire\Dashboard\Messages::get_classes( $top_id, $info['message_id'] ) }}" wire:loading.class="updating">
        <div class="intro">
            <div class="from @if( in_array( 'archived', $info['tags'] ) ) collapsed @endif" data-bs-toggle="collapse" data-bs-target="#body-{{ $info['message_id'] }}" aria-expanded="true" aria-controls="body-{{ $info['message_id'] }}" wire:ignore.self>
                <div class="flex items-start justify-between">
                    <div class="w-full md:w-3/5">
                        <div class="sender-name w-full md:w-3/5 mb-1">
                            From: {{ $info['sender_name'] }}
                        </div>
                        <div class="recipients text-neutral-400">To: {{ $info['recipients_names'] }}</div>
                    </div>
                    <div class="flex items-start justify-between md:w-2/5 w-full">
                        <div class="date">{{ $info['date_time'] }}</div>

                        <div class="actions dropdown flex items-start justify-between options -mt-1">
                            <div class="rounded bg-neutral-500 py-1 px-2 text-white transition duration-150 ease-in-out motion-reduce:transition-none
                            hover:bg-blue/70
                            focus:bg-blue focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-blue active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]" type="button" aria-expanded="false"
                            data-te-ripple-init data-te-ripple-color="light"
                            id="actions-toggle-{{ $info['message_id'] }}" data-te-dropdown-toggle-ref
                        >
                                <i class="fa-light w-full fa-ellipsis-vertical"></i>
                            </div>

                            <ul class="py-2 absolute z-[1000] float-left m-0 hidden min-w-max list-none overflow-hidden rounded-lg border-none bg-lightBlue text-left text-base shadow-lg [&[data-te-dropdown-show]]:block" aria-labelledby="actions-toggle-{{ $info['message_id'] }}" data-te-dropdown-menu-ref wire:ignore.self>
                                @if( $info['meta']['last_read_at'] === null) <li class="action px-3 hover:bg-neutral-300 cursor-pointer" wire:click="actions_read({{ $top_id }}, {{ $info['message_id'] }}, true)">Mark as Read</li>

                                @else <li class="action px-3 hover:bg-neutral-300 cursor-pointer" wire:click="actions_read({{ $top_id }}, {{ $info['message_id'] }}, false)">Mark as Unread</li> @endif

                                @if( in_array( 'archived', $info['tags'] ) ) <li class="action px-3 hover:bg-neutral-300 cursor-pointer" wire:click="actions_tags({{ $top_id }}, {{ $info['message_id'] }}, 'unarchive')">Unarchive</li>

                                @else <li class="action px-3 hover:bg-neutral-300 cursor-pointer" wire:click="actions_tags({{ $top_id }}, {{ $info['message_id'] }}, 'archived')">Archive</li> @endif

                                @if( in_array( 'reported', $info['tags'] )) <li class="action px-3 hover:bg-neutral-300 cursor-pointer" wire:click="actions_tags({{ $top_id }}, {{ $info['message_id'] }}, 'unreport')">Cancel Report</li>

                                @else <li class="action px-3 hover:bg-neutral-300 cursor-pointer" wire:click="actions_tags({{ $top_id }}, {{ $info['message_id'] }}, 'reported')">Report</li> @endif

                                @if( $info['deleted'] ) <li class="action px-3 hover:bg-neutral-300 cursor-pointer" wire:click="actions_delete({{ $info['message_id'] }} )">Cancel Deletion</li>

                                @else <li class="action px-3 hover:bg-neutral-300 cursor-pointer" wire:click="actions_delete({{ $info['message_id'] }} )">Delete</li> @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="subject mt-2 mb-4 @if( in_array( 'archived', $info['tags'] ) ) collapsed @endif" data-bs-toggle="collapse" data-bs-target="#body-{{ $info['message_id'] }}" aria-expanded="true" aria-controls="body-{{ $info['message_id']}}" wire:ignore.self>Subject: {{ $info['message']['subject'] }}</div>

        </div>

        <div id="body-{{ $info['message_id'] }}" class="body-wrapper @if( in_array( 'archived', $info['tags'] ) ) hidden @endif" wire:ignore.self><div class="body">{{ $info['message']['body'] }}</div></div>

        <div class="flex items-start justify-between">
            <div class="flex-1">
                @if( in_array( 'reported', $info['tags'] )) <div class="pt-4 text-pink/80 message">You have reported this message<br/>to the admin for review</div> @endif
                @if( $info['deleted'] ) <div class="pt-4 text-black message italic">This message is schedule to be<br/>deleted on {{ $info['deleted'] }}</div> @endif
            </div>

            <button class="mt-4 justify-end w-full italic flex items-center flex-1" role="button" aria-expanded="false"
                data-te-collapse-init data-te-ripple-init data-te-ripple-color="light"
                href="#compose-message-reply" aria-controls="compose-message-reply" wire:click="actions_set_parent( {{ $top_id }} )"
            >
                <i class="fa-light text-[1.5rem] fa-flip-vertical fa-arrow-right-from-bracket"></i>
                <span class="ml-2">Reply to this message</span>
            </button>
        </div>
    </div>
@endif
