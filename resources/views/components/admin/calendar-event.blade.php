<div
    @if($eventClickEnabled)
        wire:click.stop="onEventClick('{{ $event['id']  }}')"
    @endif
    class="{{ App\Livewire\Dashboard\LessonsHelpers::event_class($event)  }}"
    @if($event['updatable']) data-bs-toggle="modal" data-bs-target="#create-lesson" aria-expanded="true" aria-controls="create-lesson" @endif
>

    <p class="fs-sm">
        {{ $event['title'] }}
    </p>
    <p class="mt-2 fs-sm">
        <span class="event-status {{ $event['status_slug'] }}">{{ $event['status'] }}</span>
        {{ $event['description'] ?? 'No description' }}
    </p>
</div>
