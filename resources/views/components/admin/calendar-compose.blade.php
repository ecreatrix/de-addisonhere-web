@props([
    'tutees' => [],
    'tutors' => [],
])

<div class="modal fade compose" id="create-lesson" tabindex="-1" aria-labelledby="createLessonLabel" aria-hidden="true" wire:ignore.self>
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header mb-0 pb-0">
                <h5 class="modal-title" id="createLessonLabel">@if( $this->calendarEventNew ) Create @else Update @endif a lesson</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body mt-0 pt-0">
                <div class="my-3">Status: <span class="event-status status-{{ $this->calendarEventStatus }}">{{ Str::replace( '-', ' ', Str::title( $this->calendarEventStatus ) ) }}</span></div>

                <form wire:submit.prevent="createLesson" class="@if(session()->has('dashboard-message-calendar-create')) hidden @endif needs-validation" wire:loading.class="loading">
                    @csrf
                    <x-form.loading />

                    <input type="hidden" error_object="{{ $errors->__toString() }}" container_classes="col-12 required my-2" id="calendarEventID" label="ID" wire:model.defer="calendarEventID" />
                    <div class="has-validation form-floating required mb-2">
                        @if( $this->user->is_tutee() || $this->user->is_admin() )
                            <select error_object="{{ $errors->__toString() }}" container_classes="col-12 required" id="calendarTutorSelect" name="calendarTutorSelect" label="Tutor" value="{{ $this->calendarTutorSelect }}" wire:model.defer="calendarTutorSelect" :options="$tutors"></select>
                        @elseif( $this->user->is_tutor() || $this->user->is_admin() )
                            <select error_object="{{ $errors->__toString() }}" container_classes="col-12 required" id="calendarTuteeSelect" name="calendarTuteeSelect" label="Tutee" value="{{ $this->calendarTuteeSelect }}" wire:model.defer="calendarTuteeSelect" :options="$tutees"></select>
                        @endif
                    </div>

                    @if($this->user->is_admin())<select error_object="{{ $errors->__toString() }}" container_classes="col-12 required" id="calendarEventComposeStatus" name="calendarEventComposeStatus" label="Status" wire:model.defer="calendarEventComposeStatus" :options="[
                        'request-pending' => 'Request Pending',
                        'booked' => 'Booked',
                        'no-payment' => 'No Payment',
                        'payment-pending' => 'Payment Pending',
                        'payment-received' => 'Payment Received',
                    ]"></select>@endif

                    <input type="number" error_object="{{ $errors->__toString() }}" container_classes="col-12 required my-2" id="calendarEventComposeCost" label="Cost" wire:model.defer="calendarEventComposeCost" />

                    <input type="textarea" error_object="{{ $errors->__toString() }}" container_classes="col-12 required mt-2" id="calendarEventComposeBody" label="Message" wire:model.defer="calendarEventComposeBody" flexs="4" />

                    <div error_object="{{ $errors->__toString() }}" container_classes="col-12 required mt-2" id="calendarEventComposeDatetime" label="When" wire:model.defer="calendarEventComposeDatetime" flexs="4" class="relative mb-3" data-te-datepicker-init data-te-input-wrapper-init>
                        <input type="text" class="peer block min-h-[auto] w-full rounded border-0 bg-transparent px-3 py-[0.32rem] leading-[1.6] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 peer-focus:text-green data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:text-neutral-200 dark:placeholder:text-neutral-200 dark:peer-focus:text-green [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" placeholder="Select a date" />
                        <label for="floatingInput" class="pointer-events-none absolute left-3 top-0 mb-0 max-w-[90%] origin-[0_0] truncate pt-[0.37rem] leading-[1.6] text-neutral-500 transition-all duration-200 ease-out peer-focus:-translate-y-[0.9rem] peer-focus:scale-[0.8] peer-focus:text-green peer-data-[te-input-state-active]:-translate-y-[0.9rem] peer-data-[te-input-state-active]:scale-[0.8] motion-reduce:transition-none dark:text-neutral-200 dark:peer-focus:text-green">Select a date</label>
                    </div>

                    <div class="buttons mt-4">
                        <button type="button" class="w-1/3 btn btn-red" data-bs-dismiss="modal">Cancel</button>
                        <button type="button" class="col-3 btn btn-blue fade @if( !$this->calendarEventNew && $this->calendarEventStatus === 'booked' ) show @endif" data-bs-dismiss="modal">
                            @if( $this->calendarEventApproved )
                                <span wire:click="statusRejectLesson()">Decline</span>
                            @else
                                <span wire:click="statusAcceptLesson()">Accept</span>
                            @endif
                        </button>
                        <button type="submit" class="w-1/3 btn btn-green">@if( $this->calendarEventNew ) Create @else Update @endif Lesson</button>
                    </div>
                </form>

                <x-form.alert tag="h6" alert="dashboard-alert-calendar-create" />
                <x-form.message tag="h6" message="dashboard-message-calendar-create" />
            </div>

        </div>
    </div>
</div>
