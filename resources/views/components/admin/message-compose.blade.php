@props([
    'id' => false,
    'actionable' => [],
])

<form wire:submit.prevent="compose" class="needs-validation @if(session()->has('dashboard-message-compose-messages')) hidden @endif" wire:loading.class="loading">
    @csrf

    <div class="flex justify-between items-center">
        <x-form.select label="Recipients" :errors="$errors" id="composeRecipient-{{ $id }}" wireModel="composeRecipient" wrapperClass="w-full md:w-45/100" :options="$actionable" />

        <x-form.input wrapperClass="md:w-45/100" :errors="$errors" id="composeSubject-{{ $id }}" label="Subject" wireModel="composeSubject"  />
    </div>

    <x-form.textarea wrapperClass="my-3" :errors="$errors" id="composeBody-{{ $id }}" label="Message" wire-model="composeBody" rows="6" style="height:100%;" />

    <div class="mt-4">
        <button type="submit" class="btn btn-green btn-lg mt-0">Send Message</button>
    </div>
</form>

<x-form.alert tag="h6" alert="dashboard-alert-compose-messages" />
<x-form.message tag="h6" message="dashboard-message-compose-messages" />
