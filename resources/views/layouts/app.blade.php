<!DOCTYPE html>
<html class="light" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

    <!-- Scripts -->


    @if ( $__env->yieldContent( 'admin' ) )
        @vite(['resources/styles/dashboard.css', 'resources/scripts/dashboard.js'])

        <link rel="shortcut icon" href="{{ Vite::asset('resources/images/favicon-admin.png') }}">

        <script src="https://kit.fontawesome.com/5ef88b4d32.js" type="text/javascript"></script>

        <!-- Material Icons -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    @else
        @vite(['resources/styles/app.css', 'resources/scripts/app.js'])

        <link rel="shortcut icon" href="{{ Vite::asset('resources/images/favicon.png') }}">
    @endif

    <meta name="csrf_token" value="{{ csrf_token() }}"/>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;900&amp;display=swap" crossorigin="anonymous">

    @livewireStyles
</head>
<body class="font-sans text-black antialiased leading-5 selection:bg-green selection:text-white @if ( ! $__env->yieldContent( 'admin' ) ) mt-[72px] @endif min-h-screen flex flex-col @yield('page-class' ) @yield('slug', \Illuminate\Support\Str::slug( $__env->yieldContent( 'title' ) ) )@if( $__env->yieldContent( 'admin' ) ) admin @endif">
    <div class="md:flex flex-col justify-between bg-center min-h-screen">
        @if ( ! $__env->yieldContent( 'admin' ) )
            @include('layouts.navigation')
        @endif

        <!-- Page Content -->
        <main class="mb-auto mb-auto">
            @if ( $__env->yieldContent( 'entry-block' ) )
                <div class="bg-pink"><div class="container">
                    <h1 class="text-white">
                        @yield('entry-block')
                    </h1>
                </div></div>
            @endif

            @yield('content')
        </main>

        @if ( ! $__env->yieldContent( 'admin' ) )
            @include('layouts.footer')
        @endif
    </div>

    @livewireScripts
</body>
</html>
