<nav class="z-1 fixed top-0 w-full bg-white py-2 text-black md:py-2" data-te-navbar-ref><div class="container my-0 py-0 overflow-visible">
    <div class="relative flex w-full flex-wrap items-center justify-between max-w-7xl mx-auto overflow-visible">
        <div class="h-[40px] bg-red my-2 max-w-2/3">
            <a class="text-xl text-black block h-full max-w-[300px]" href="{{ route('home') }}">
                <x-logos.logo-horizontal-green-black class="block h-9 w-auto fill-current h-full w-full" />
            </a>
        </div>

        <!-- Hamburger button for mobile view -->
        <button id="navbarToggler" class="ml-auto block border-0 bg-transparent px-2 text-black hover:no-underline hover:shadow-none focus:no-underline focus:shadow-none focus:outline-none focus:ring-0 md:hidden" type="button" data-te-collapse-init data-te-target="#mainNavbar" aria-controls="mainNavbar" aria-expanded="false" aria-label="Toggle navigation">
            <!-- Hamburger icon -->
            <span class="[&>svg]:w-7">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="h-7 w-7">
                    <path fill-rule="evenodd" d="M3 6.75A.75.75 0 013.75 6h16.5a.75.75 0 010 1.5H3.75A.75.75 0 013 6.75zM3 12a.75.75 0 01.75-.75h16.5a.75.75 0 010 1.5H3.75A.75.75 0 013 12zm0 5.25a.75.75 0 01.75-.75h16.5a.75.75 0 010 1.5H3.75a.75.75 0 01-.75-.75z" clip-rule="evenodd" />
                </svg>
            </span>
        </button>

        <!-- Collapsible navbar -->
        <div id="mainNavbar" class="order-4 md:order-3 !visible mt-2 hidden basis-[100%] items-center md:mt-0 md:!flex md:basis-auto ml-auto flex flex-wrap text-sm overflow-visible" data-te-collapse-item>

            <!-- Nav links -->
            <div class="order-2 md:order-1 ml-auto items-center basis-[100%] md:basis-auto overflow-visible">
                <!-- Left links -->
                <ul class="main list-style-none mr-auto flex pl-0 md:mt-1 items-center justify-center text-xl md:text-[0.8rem] overflow-visible" data-te-navbar-nav-ref>
                    <x-nav-link :href="route('tutor')" :active="request()->routeIs('tutor')">
                        {{ __('Tutor') }}
                    </x-nav-link>

                    <x-nav-link :href="route('tutee')" :active="request()->routeIs('tutee')">
                        {{ __('Tutee') }}
                    </x-nav-link>

                    @auth
                        <li class="mb-4 pl-2 md:mb-0 md:pl-0 md:pr-1 relative overflow-visible" data-te-nav-item-ref data-te-dropdown-ref>
                            <a class="flex items-center transition duration-200 hover:text-green hover:ease-in-out motion-reduce:transition-none md:px-2 uppercase p-0 transition duration-200 hover:text-green hover:ease-in-out focus:text-green disabled:text-neutral-600/30 motion-reduce:transition-none md:px-2 [&.active]:text-green" href="#" type="button" id="dropdownMenuProfile2" data-te-dropdown-toggle-ref aria-expanded="false">
                                {{ Auth::user()->full_name() }}

                                <svg class="ml-1" fill="currentColor" xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 320 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z"/></svg>
                            </span>

                            <ul class="absolute z-[1000] float-left m-0 hidden min-w-max list-none overflow-hidden rounded-b-lg border-none bg-gray-200 text-left text-base [&[data-te-dropdown-show]]:block w-full top-[48px]" aria-labelledby="dropdownMenuProfile2" data-te-dropdown-menu-ref>
                                <x-nav-sublink :href="route('dashboard')" :active="request()->routeIs('dashboard')">
                                    {{ __('Dashboard') }}
                                </x-nav-sublink>
                                <x-nav-sublink :href="route('logout')" :active="request()->routeIs('logout')">
                                    {{ __('Log Out') }}
                                </x-nav-sublink>
                            </ul>
                        </li>
                    @else
                        <x-nav-link :href="route('login')" :active="request()->routeIs('login')">
                            {{ __('Log In') }}
                        </x-nav-link>

                        <x-nav-link class="text-green" :href="route('register')" :active="request()->routeIs('register')">
                            {{ __('+ SIGN UP') }}
                        </x-nav-link>
                    @endauth
                </ul>
            </div>
        </div>
    </div></div>
</nav>
