<footer class="footer mt-auto bg-green"><div class="container bg-green">
    <div class="flex">
        <div class="brand w-full md:w-1/3">
            <a class="navbar-brand w-3/4 block" href="{{route('home')}}"><x-logos.logo-horizontal-white-black /></a>
        </div>

        <div class="links w-full md:w-3/5">
            <div class="flex flex-col md:flex-row md:mt-0 mt-4 justify-between items-top">
                <div class="tutor md:w-1/4 mt-8 md:mt-0">
                    <h4 class="text-black font-semibold pb-3 md:pb-6 uppercase">Tutor</h4>

                    <div class="list text-white text-xs font-semibold">
                        <a class="block" href="{{route('tutor')}}">Become a Tutor</a>
                        <a class="block" href="{{route('assessment')}}">Assessment</a>
                        <a class="block" href="{{route('earnings')}}">Earnings</a>
                        <a class="block" href="{{route('locations')}}">Location</a>
                        <a class="block" href="{{route('help')}}">Help</a>
                    </div>
                </div>
                <div class="tutee md:w-1/4 my-8 md:my-0">
                    <h4 class="text-black font-semibold pb-3 md:pb-6 uppercase">Tutee</h4>

                    <div class="list text-white text-xs font-semibold">
                        <a class="block" href="{{route('tutee')}}">Find Your Tutor</a>
                        <a class="block" href="{{route('fees')}}">Fees</a>
                        <a class="block" href="{{route('locations')}}">Location</a>
                        <a class="block" href="{{route('help')}}">Help</a>
                    </div>
                </div>
                <div class="general md:w-1/4">
                    <h4 class="text-black font-semibold pb-3 md:pt-0 md:pb-6 uppercase">Addison<span class="text-white">Here</span></h4>
                    <div class="list text-white text-xs font-semibold">
                        <a class="block" href="{{route('careers')}}">Careers</a>
                        <a class="block" href="{{route('about')}}">About Us</a>
                        <a class="block" href="{{route('contact')}}">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="text-xs my-6 md:my-4">
        <span class="copyright">&copy; {{ date('Y') }} {{ str_replace(' ', '', env('APP_NAME', '') ) }}</span>
    </div>
</div></footer>
