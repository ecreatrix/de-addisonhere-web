@extends('layouts.app')

@section('title', 'Forgot Password')
@section('entry-block', 'Forgot your Password?')
@section('page-class', 'bg-gray-100')

@section('content')
    <x-larastrap::media_text classes="circle-media" image="images/woman-on-phone.jpg" size-container=true>
        @livewire('auth.forgot-password')
    </x-larastrap::media_text>
@stop
