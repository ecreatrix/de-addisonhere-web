@extends('layouts.app')

@section('title', 'Reset Password')
@section('entry-block', 'Reset Password')
@section('page-class', 'bg-gray-100')

@section('content')
    <div class="container form-block login-form">
        <x-auth-session-status class="mb-4" :status="session('status')" />
        @livewire('auth.reset-password')
    </div>
@stop
