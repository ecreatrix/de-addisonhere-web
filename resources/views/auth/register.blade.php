@extends('layouts.app')

@section('title', 'Sign Up')
@section('entry-block')
Creating Your<br/>New Account
@stop
@section('page-class', 'bg-gray-100')

@section('content')
    <div class="container form-block login-form">
        <x-auth-session-status class="mb-4" :status="session('status')" />
        @livewire('auth.register')
    </div>
@stop
