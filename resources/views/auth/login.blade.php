@extends('layouts.app')

@section('title', 'Log In')
@section('entry-block', 'Login')
@section('page-class', 'bg-gray-100')

@section('content')
    @livewire('auth.login')
@stop
