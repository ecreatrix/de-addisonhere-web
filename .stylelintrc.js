module.exports = {
  "extends": [
    "stylelint-config-recommended-scss",
    "stylelint-config-standard-scss",
    "stylelint-config-idiomatic-order",
    "stylelint-config-prettier"
  ],
  "ignoreFiles": [
    "node_modules/**",
    "dist/**"
  ],
  "rules": {
    "comment-whitespace-inside": null,
    "comment-empty-line-before": null,
    "scss/comment-no-empty": null,
    "order/properties-order": null,
    "property-no-vendor-prefix": null,
    "custom-property-pattern": null,
    "no-descending-specificity": null,
    "selector-class-pattern": null,
    "no-invalid-position-at-import-rule": null,
    "order/properties-alphabetical-order": true,
    "scss/dollar-variable-empty-line-before": null,
    "scss/double-slash-comment-whitespace-inside": null,
    "scss/no-global-function-names": null,
    "scss/double-slash-comment-empty-line-before": null,
    "no-empty-source": null
  }
}